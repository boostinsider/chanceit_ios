//
//  ShareViewController.h
//  OneYuan
//
//  Created by 顺然 贾 on 16/6/3.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineMyOrderModel.h"

@interface ShareViewController : OneBaseVC <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIImageView *imageViewPic;
@property (nonatomic, strong) UILabel *labelTitle;
@property (nonatomic, strong) UITextView *textViewContent;
@property (nonatomic, strong) UILabel *labelPlaceholder;

@property (nonatomic, strong) UIButton *buttonAddImage;

@property (nonatomic, strong) NSMutableArray *arrayImageView;

@property (nonatomic, strong) MineMyOrderItem *item;
@property (nonatomic, strong) NSMutableSet *stImage;

- (void) deleteImage:(UIImage *)image;

@end
