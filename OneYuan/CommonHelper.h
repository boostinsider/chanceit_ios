//
//  CommonHelper.h
//  OneYuan
//
//  Created by 顺然 贾 on 16/6/1.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonHelper : NSObject

+ (void) removeTableCellSeperator: (UITableViewCell *)cell;
+ (void) showMessage:(NSString *)message Completion:(void(^)())compeletion;
+ (NSString *)getLocalizedText:(NSString *)str;
+ (CGSize) getTextViewSize: (NSString *)content Width:(CGFloat)width Font:(UIFont *)font;
@end
