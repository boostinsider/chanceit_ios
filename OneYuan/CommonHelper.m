//
//  CommonHelper.m
//  OneYuan
//
//  Created by 顺然 贾 on 16/6/1.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "CommonHelper.h"

@implementation CommonHelper
+ (void) removeTableCellSeperator:(UITableViewCell *)cell
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.separatorInset = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = false;
    cell.layoutMargins = UIEdgeInsetsZero;
}

+ (void) showMessage:(NSString *)message Completion:(void (^)())compeletion
{
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    UIView *showView = [UIView new];
    showView.backgroundColor = [UIColor blackColor];
    showView.frame = CGRectMake(1, 1, 1, 1);
    showView.alpha = 1.0;
    showView.layer.cornerRadius = 5.0;
    showView.layer.masksToBounds = true;
    [window addSubview:showView];
    
    
    UILabel *label = [UILabel new];
    label.frame = CGRectMake(10, 5, 240, 50);
    label.numberOfLines = 0;
    [label setText:message];
    //    label.text = message;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    [showView addSubview:label];
    showView.frame = CGRectMake(0, 0, label.frame.size.width + 20, label.frame.size.height + 10);
    [showView setCenter:window.center];
    
    
    [UIView animateWithDuration:1.5 animations:^{
        showView.alpha = 0;
    } completion:^(BOOL finished) {
        [showView removeFromSuperview];
        if (compeletion)
            compeletion();
    }];
}

+ (NSString *)getLocalizedText:(NSString *)str
{
    NSString *res = NSLocalizedStringFromTable(str, nil, nil);
    return res?res:str;
    //    return str;
}

+ (CGSize) getTextViewSize: (NSString *)content Width:(CGFloat)width Font:(UIFont *)font
{
    UITextView *tmp = [[UITextView alloc] init];
    tmp.font = font;
    [tmp setText:content];
    CGSize sz = [tmp sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)];
    return sz;
}

@end
