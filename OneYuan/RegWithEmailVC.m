//
//  RegWithEmailVC.m
//  OneYuan
//
//  Created by NW on 16/5/27.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "RegWithEmailVC.h"
#import "ServerHelper.h"
#import "LoginModel.h"
#import "User.h"

@interface RegWithEmailVC ()<UITextFieldDelegate>
{
    UITextField     *txtEmail;
    __block UITextField     *txtPwd;
    __block UITextField     *txtRePwd;
}
@end

@implementation RegWithEmailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    self.title = [CommonHelper getLocalizedText:@"email_sign_up"];
    __weak typeof (self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"btnback" htlImage:@"btnback" title:@"" action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    
//    UIView* vvv = [[UIView alloc] initWithFrame:CGRectMake(16, 15, mainWidth - 32, 44)];
//    vvv.backgroundColor = [UIColor whiteColor];
//    vvv.layer.borderColor = [UIColor hexFloatColor:@"dedede"].CGColor;
//    vvv.layer.borderWidth = 0.5;
//    vvv.layer.cornerRadius = 3;
//    [self.view addSubview:vvv];
    
    
    
    //new ui
    UIView* vvv = [[UIView alloc] initWithFrame:CGRectMake(16.5, 20.5, mainWidth - 33, 87 + 43)];
    vvv.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:vvv];
    
    UIImageView* imgUser = [[UIImageView alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
    imgUser.image = [UIImage imageNamed:@"login_name"];
    [vvv addSubview:imgUser];
    
    txtEmail = [[UITextField alloc] initWithFrame:CGRectMake(35, 0, vvv.frame.size.width - 35, 44)];
    txtEmail.placeholder = [CommonHelper getLocalizedText:@"signup_name_required"];
    txtEmail.clearButtonMode = UITextFieldViewModeWhileEditing;
    txtEmail.font = [UIFont systemFontOfSize:14];
//    txtEmail.text = [[NSUserDefaults standardUserDefaults] objectForKey:kLoginUsername];
    [vvv addSubview:txtEmail];
    
    UIImageView* imgPwd = [[UIImageView alloc] initWithFrame:CGRectMake(10, 56, 20, 20)];
    imgPwd.image = [UIImage imageNamed:@"login_password"];
    [vvv addSubview:imgPwd];
    
    txtPwd = [[UITextField alloc] initWithFrame:CGRectMake(35, 44, vvv.frame.size.width - 35, 44)];
    txtPwd.placeholder = [CommonHelper getLocalizedText:@"signup_pwd_required"];
    txtPwd.clearButtonMode = UITextFieldViewModeWhileEditing;
    txtPwd.font = [UIFont systemFontOfSize:14];
    txtPwd.secureTextEntry = YES;
    [vvv addSubview:txtPwd];
    
    UIImageView* img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 100, 20, 20)];
    img.image = [UIImage imageNamed:@"login_password"];
    [vvv addSubview:img];
    
    txtRePwd = [[UITextField alloc] initWithFrame:CGRectMake(35, 44 + 43, vvv.frame.size.width - 35, 44)];
    txtRePwd.backgroundColor = [UIColor whiteColor];
    txtRePwd.font = [UIFont systemFontOfSize:14];
    txtRePwd.placeholder = [CommonHelper getLocalizedText:@"signup_repwd_required"];
    txtRePwd.clearButtonMode = UITextFieldViewModeWhileEditing;
    txtRePwd.keyboardType = UIKeyboardAppearanceDefault;
    txtRePwd.delegate = self;
    txtRePwd.secureTextEntry = YES;
    [vvv addSubview:txtRePwd];
    
    UIButton* btnDone = [[UIButton alloc] initWithFrame:CGRectMake(16, 75 + 44 + 44, mainWidth - 32, 44)];
    btnDone.backgroundColor = mainColor;
    [btnDone setTitle:[CommonHelper getLocalizedText:@"signup"] forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.layer.cornerRadius = 5;
    [btnDone addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnDone];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length > 0 && textField.text.length > 19)
    {
        return false;
    }
    return YES;
}

- (void)btnAction
{
    if(txtEmail.text.length == 0)
    {
        [[XBToastManager ShardInstance] showtoast:[CommonHelper getLocalizedText:@"signup_email_format_required"]];
        return;
    }
    if(txtPwd.text.length < 6 || txtPwd.text.length > 20)
    {
        [[XBToastManager ShardInstance] showtoast:[CommonHelper getLocalizedText:@"signup_pwd_required"]];
        return;
    }
    if([txtRePwd.text isEqualToString:txtPwd.text] == NO)
    {
        [[XBToastManager ShardInstance] showtoast:[CommonHelper getLocalizedText:@"signup_repwd_same_required"]];
        return;
    }
    __weak typeof (self) wSelf = self;
    [[XBToastManager ShardInstance] showprogress];
//    [RegModel regPhoneSetPwd:myStr pwd:txtPwd.text success:^(AFHTTPRequestOperation* operation1, NSObject* result1){
//        RegSms* r = [[RegSms alloc] initWithDictionary:(NSDictionary*)result1];
//        if([r.state intValue] != 0)
//        {
//            [[XBToastManager ShardInstance] hideprogress];
//            return ;
//        }
//        [wSelf doLogin];
//    } failure:^(NSError* error){
//        [[XBToastManager ShardInstance] hideprogress];
//    }];
    NSDictionary* params = @{@"username":txtEmail.text,
                             @"password":txtPwd.text,
                             @"repassword":txtRePwd.text};
    
    [ServerHelper signUpWithParameters:params Completion:^(NSDictionary *user) {
        NSLog(@"sign up succeed");
        [[XBToastManager ShardInstance] hideprogress];
        [wSelf doLogin];
    } failure:^{
        NSLog(@"sign up failed");
        [[XBToastManager ShardInstance] hideprogress];
    }];
}

-(void)doLogin
{
    __weak typeof (self) wSelf = self;
    
     [[XBToastManager ShardInstance] showprogress];
    NSDictionary *param = @{@"username":txtEmail.text,@"password":txtPwd.text};
    [ServerHelper loginWithParameters:param Completion:^(NSDictionary *result) {
        NSDictionary* dic = (NSDictionary*)result;
        if ([[dic objectForKey:@"status"]intValue] == 200 && [dic objectForKey:@"user"] != NULL) {
            [[NSUserDefaults standardUserDefaults] setObject:txtEmail.text forKey:kLoginUsername];
            [[NSUserDefaults standardUserDefaults] setObject:txtPwd.text forKey:kLoginPwd];
            [[User getInstance] setUserInfo:[dic objectForKey:@"user"]];
            [[XBToastManager ShardInstance] hideprogress];
            [[NSNotificationCenter defaultCenter] postNotificationName:kDidLoginOk object:nil];
            [wSelf.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            [[XBToastManager ShardInstance] showtoast:[NSString stringWithFormat:@"%@:%@",[CommonHelper getLocalizedText:@"failed"],[dic objectForKey :@"message"]]];
        }
    } failure:^{
        [[XBToastManager ShardInstance] showtoast:[CommonHelper getLocalizedText:@"failed"]];
    }];
}


@end
