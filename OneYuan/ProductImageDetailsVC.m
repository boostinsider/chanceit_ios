//
//  ProductImageDetailsVC.m
//  OneYuan
//
//  Created by NW on 16/6/9.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "ProductImageDetailsVC.h"
#import "ProductModel.h"
#import "ProductDetailTopCell.h"
#import "ProductDetailGetCell.h"
#import "AllProModel.h"
#import "ShowOrderListVC.h"
#import "ProductBuyListVC.h"
#import "ProductDetailOptView.h"
#import "CartModel.h"
#import "CartInstance.h"
#import "ProductLotteryVC.h"
#import "HomeModel.h"

@interface ProductImageDetailsVC ()<ProductDetailOptViewDelegate>
{
    int    _goodsId;
    int    _codeId;
    
    __block UIWebView       *webView;
    __block ProductDetail   *proDetail;
    
    UIImageView     *imgPro;
    __block ProductDetailOptView* optView;
    
    __block ShopOverResult          *_shopOverResult;
    __block ProductDetailResult     *_productDetailResult;
}

@end

@implementation ProductImageDetailsVC
@synthesize periodNum;

- (id)initWithGoodsId:(int)goodsId codeId:(int)codeId
{
    self = [super init];
    if(self)
    {
        _goodsId = goodsId;
        _codeId = codeId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    self.title = [CommonHelper getLocalizedText:@"image_details"];
    __weak typeof (self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"btnback" htlImage:@"btnback" title:@"" action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    CGFloat margin = 0;
    if(![OyTool ShardInstance].bIsForReview)
    {
        margin = 60;
        optView = [[ProductDetailOptView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 107 - 16, mainWidth, 60)];
        optView.delegate = self;
        [self.view addSubview:optView];
    }
    
    //webView
    webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, mainWidth, self.view.bounds.size.height - margin - 64)];
    //    webView.navigationDelegate = self;
    [self.view addSubview:webView];
    [webView setScalesPageToFit:YES];
    webView.hidden = YES;
    
    [self getData];
    [self showLoad];
}

- (void)getData //goodsId 不为0，获取商品详情
{
    __weak typeof (self) wSelf = self;
    NSDictionary* dic = @{@"pid":@(_goodsId)};
    [ServerHelper getProductDetails:dic Completion:^(NSDictionary *result) {
        NSLog(@"getProductDetails succeed!");
        [wSelf hideLoad];
        if ([[result objectForKey:@"status"] intValue] == 200 && [result objectForKey:@"data"]) {
            _productDetailResult = [[ProductDetailResult alloc]initWithDictionary:result];
            //            NSString* html = @"<p style=\"text-align: center;\"><img src=\"http://localhost/Picture/Ueditor/2016-01-03/568880640db45.jpg\"/><img src=\"http://localhost/Picture/Ueditor/2016-01-03/5688806419aaf.jpg\"/><img src=\"http://localhost/Picture/Ueditor/2016-01-03/5688806433cad.jpg\"/><img src=\"http://localhost/Picture/Ueditor/2016-01-03/568880644f234.jpg\"/><img src=\"http://localhost/Picture/Ueditor/2016-01-03/56888064af55a.jpg\"/><img src=\"http://localhost/Picture/Ueditor/2016-01-03/56888064d2bcb.jpg\"/><img src=\"http://localhost/Picture/Ueditor/2016-01-03/56888064ea2d0.jpg\"/><img src=\"http://localhost/Picture/Ueditor/2016-01-03/568880655fff9.jpg\"/><img src=\"http://localhost/Picture/Ueditor/2016-01-03/56888065a844a.jpg\"/><img src=\"http://localhost/Picture/Ueditor/2016-01-03/56888065df727.jpg\"/></p>";
            [webView loadHTMLString:_productDetailResult.data.content baseURL:nil];
            //            [webView loadHTMLString:html baseURL:nil];
            webView.hidden = NO;
        }
        else if ([[result objectForKey:@"status"] intValue] == 401)
        {
            //go to login page
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        
    } failure:^{
        NSLog(@"getAnnouncedProductsWithParameters failed!");
    }];
}

#pragma mark - delegate
- (void)addToCartAction
{
    //    CartItem* item = [[CartItem alloc] init];
    //    item.cPid = [NSNumber numberWithInt:_goodsId];
    //    item.cName = proDetail.Rows2.goodsName;
    //    item.cPeriod = proDetail.Rows2.codePeriod;
    //    item.cBuyNum = [NSNumber numberWithInt:1];
    //    item.cCid = proDetail.Rows2.codeID;
    //    item.cPrice = proDetail.Rows2.codePrice;
    //    item.cSrc = [NSString stringWithFormat:@"%@%@",oyImageBaseUrl,[[proDetail.Rows1 objectAtIndex:0] picName]];
    //    [[CartInstance ShartInstance] addToCart:item imgPro:imgPro type:addCartType_Opt];
    if (_productDetailResult == nil || _goodsId == 0) {
        return;
    }
    CartItem* item = [[CartItem alloc] init];
    item.cPid = [NSNumber numberWithInt:_goodsId];
    item.cName = _productDetailResult.data.name;
    item.cPeriod = self.periodNum;//期号
    item.cBuyNum = [NSNumber numberWithInt:1];
    item.cCid = [NSNumber numberWithInt: _codeId];
    item.cPrice =_productDetailResult.data.price;
    item.cSrc = _productDetailResult.data.pic;
    [[CartInstance ShartInstance] addToCart:item imgPro:imgPro type:addCartType_Opt];
    
    [self performSelector:@selector(setCartNum) withObject:nil afterDelay:1];
}

- (void)setCartNum
{
    [CartModel quertCart:nil value:nil block:^(NSArray* result){
        [optView setCartNum:(int)result.count];
    }];
}

- (void)gotoCartAction
{
    self.tabBarController.selectedIndex = 3;
    [self.navigationController performSelector:@selector(popToRootViewControllerAnimated:) withObject:[NSNumber numberWithBool:NO] afterDelay:0.2];
}

- (void)addGotoCartAction
{
    //    CartItem* item = [[CartItem alloc] init];
    //    item.cPid = [NSNumber numberWithInt:_goodsId];
    //    item.cName = proDetail.Rows2.goodsName;
    //    item.cPeriod = proDetail.Rows2.codePeriod;
    //    item.cBuyNum = [NSNumber numberWithInt:1];
    //    item.cCid = proDetail.Rows2.codeID;
    //    item.cPrice = proDetail.Rows2.codePrice;
    //    item.cSrc = [NSString stringWithFormat:@"%@%@",oyImageBaseUrl,[[proDetail.Rows1 objectAtIndex:0] picName]];
    //    [[CartInstance ShartInstance] addToCart:item imgPro:imgPro type:addCartType_Opt];
    //
    //    [self performSelector:@selector(gotoCartAction) withObject:nil afterDelay:1];
    
    if (_productDetailResult == nil || _goodsId == 0) {
        return;
    }
    CartItem* item = [[CartItem alloc] init];
    item.cPid = [NSNumber numberWithInt:_goodsId];
    item.cName = _productDetailResult.data.name;
    item.cPeriod = self.periodNum;//期号
    item.cBuyNum = [NSNumber numberWithInt:1];
    item.cCid = [NSNumber numberWithInt: _codeId];
    item.cPrice =_productDetailResult.data.price;
    item.cSrc = _productDetailResult.data.pic;
    [[CartInstance ShartInstance] addToCart:item imgPro:imgPro type:addCartType_Opt];
    
    [self performSelector:@selector(gotoCartAction) withObject:nil afterDelay:1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
