//
//  ShareViewController.m
//  OneYuan
//
//  Created by 顺然 贾 on 16/6/3.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "ShareViewController.h"
#import "ImagePreviewViewController.h"


#define HINT @"say something...."
@implementation ShareViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor hexFloatColor:@"ffffff"];
    self.title = [CommonHelper getLocalizedText:@"product_details_nav_title"];
    __weak typeof (self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"btnback" htlImage:@"btnback" title:@"" action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    self.arrayImageView = [NSMutableArray array];
    self.stImage = [NSMutableSet set];
    
    _imageViewPic = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 80, 80)];
    [self.view addSubview:_imageViewPic];

    _labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(100, 20, DESIGN_SCREEN_WIDTH-120, 80)];
    [self.view addSubview:_labelTitle];
    _labelTitle.numberOfLines = 0;
    _labelTitle.lineBreakMode = NSLineBreakByWordWrapping;
    
    self.navigationController.topViewController.title = @"Share";
    UIBarButtonItem *buttonShare = [[UIBarButtonItem alloc] initWithTitle:@"share" style:UIBarButtonItemStyleDone target:self action:@selector(clickButtonShare)];
    self.navigationItem.rightBarButtonItem = buttonShare;
    buttonShare.tintColor = COLOR_WHITE;
    
    UILabel *labelHint = [[UILabel alloc] initWithFrame:CGRectMake(20, 120, DESIGN_SCREEN_WIDTH, 15)];
    labelHint.textColor = COLOR_GRAY;
    labelHint.text = @"[Add comment]";
    [self.view addSubview:labelHint];
    
    
    CGRect rect = _imageViewPic.frame;
    _textViewContent = [[UITextView alloc] initWithFrame:CGRectMake(20, 140, DESIGN_SCREEN_WIDTH-140, 80)];
    _textViewContent.textColor = COLOR_DARK_GRAY;
    _textViewContent.backgroundColor = COLOR_LIGHT_GRAY;
    [self.view addSubview:_textViewContent];
    _textViewContent.font = [UIFont systemFontOfSize:16];
    _textViewContent.delegate = self;
    
    _labelPlaceholder = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, _textViewContent.frame.size.width - 20, 20)];
    _labelPlaceholder.textColor = COLOR_GRAY;
    _labelPlaceholder.enabled = false;
    _labelPlaceholder.text = HINT;
    [_textViewContent addSubview:_labelPlaceholder];
    
    _buttonAddImage = [[UIButton alloc] initWithFrame:CGRectMake(DESIGN_SCREEN_WIDTH-100, 140, 80, 80)];
    [_buttonAddImage setTitle:@"+" forState:UIControlStateNormal];
    _buttonAddImage.titleLabel.font = [UIFont systemFontOfSize:60];
    [_buttonAddImage setTitleColor:COLOR_GRAY forState:UIControlStateNormal];
    _buttonAddImage.backgroundColor = COLOR_LIGHT_GRAY;
    [_buttonAddImage addTarget:self action:@selector(clickButtonAddImage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_buttonAddImage];
    
    
    rect = _textViewContent.frame;
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, rect.origin.y + rect.size.height + 20, DESIGN_SCREEN_WIDTH, 10)];
    line.backgroundColor = COLOR_LIGHT_GRAY;
    [self.view addSubview:line];
    
    rect = line.frame;
    
    rect = CGRectMake(0, rect.origin.y + rect.size.height + 20, 0, 80);
    for (int i = 0; i < 3; i ++)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(rect.origin.x + rect.size.width + 20, rect.origin.y, 80, 80)];
        [self.view addSubview:imageView];
        [_arrayImageView addObject:imageView];
        rect = imageView.frame;
        imageView.hidden = YES;
        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImage:)];
        imageView.userInteractionEnabled = YES;
        [imageView addGestureRecognizer:tap];
    }
    
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)] ;
    tapGesture.delegate=self;
    [tapGesture setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tapGesture];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[UIView class]]) {
        [self hideKeyboard];
        return NO;
    }
    return YES;
}

-(void)hideKeyboard
{
    [_textViewContent resignFirstResponder];
}

- (void) tapImage: (UITapGestureRecognizer *)recognizer
{
    ImagePreviewViewController *vc = [[ImagePreviewViewController alloc] init];
    
    UIImageView *imageView = (UIImageView *)recognizer.view;
    vc.image = imageView.image;
    [self.navigationController pushViewController:vc animated:YES];
    vc.parent = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.imageViewPic setImage_oy:oyImageBaseUrl image:_item.goodsPic];
    self.labelTitle.text = _item.goodsSName;
}
- (void) clickButtonShare
{
    NSArray *arr = [self.stImage allObjects];
    if (arr.count == 0)
    {
        [CommonHelper showMessage:@"at least 1 picture" Completion:nil];
        return;
    }
    
    JGProgressHUD* progressHUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
    progressHUD.textLabel.text = @"sharing";
    [progressHUD showInView:self.view animated:YES];
    
    __block int cnt = 0;
    NSMutableArray *arrayPic = [NSMutableArray array];
    NSMutableArray *arrayThumbpic = [NSMutableArray array];
    for (UIImage *image in arr)
    {
        [ServerHelper addPhoto:image WithParams:nil Completion:^(NSDictionary *info) {
            cnt ++;
            NSString *url = [info objectForKey:@"path"];
            NSString *thumb = [info objectForKey:@"thumbpath"];
            [arrayPic addObject:url];
            [arrayThumbpic addObject:thumb];
            if (cnt == arr.count)
            {
                NSDictionary *params = @{@"content":_textViewContent.text,
                                         @"pic":arrayPic,
                                         @"thumbpic":arrayThumbpic,
                                         @"pid":_item.pid};
                
                [ServerHelper shareWithParams:params Completion:^(id result) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:kDidReloadUser object:nil];
                    [progressHUD dismissAnimated:YES];
                    [CommonHelper showMessage:@"share success, You got 1 coin" Completion:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    
                } failure:^{
                    
                    progressHUD.indicatorView = [JGProgressHUDErrorIndicatorView new];
                    progressHUD.textLabel.text = @"failed";
                    [progressHUD dismissAfterDelay:1.0 animated:YES];
                }];
                NSLog(@"ok");
            }
            
        } failure:^{
            
            progressHUD.indicatorView = [JGProgressHUDErrorIndicatorView new];
            progressHUD.textLabel.text = @"failed";
            [progressHUD dismissAfterDelay:1.0 animated:YES];
        }];
    }
    
}

- (void) clickButtonAddImage
{
    if (_stImage.count >= 3)
    {
        [CommonHelper showMessage:@"cannot greater than 3" Completion:^{
            
        }];
        return;
    }
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.view.backgroundColor = [UIColor orangeColor];
//    UIImagePickerControllerSourceType sourcheType = UIImagePickerControllerSourceTypeCamera;
//    picker.sourceType = sourcheType;
    picker.delegate = self;
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void) deleteImage:(UIImage *)image
{
    [_stImage removeObject:image];
    [self reloadImage];
}

- (void) addImage: (UIImage *)image
{
    [_stImage addObject:image];
    [self reloadImage];
}
- (void) reloadImage
{
    
    NSArray *arr = [_stImage allObjects];
    for (int i = 0; i < 3; i ++)
    {
        UIImageView *imageView = [_arrayImageView objectAtIndex:i];
        if (i < arr.count)
        {
            imageView.hidden = NO;
            UIImage *image = [arr objectAtIndex:i];
            [imageView setImage:image];
        }
        else
        {
            imageView.hidden = YES;
        }
    }
}

#pragma mark - textview delegate
-(void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length == 0) {
        _labelPlaceholder.text = @"say something...";
    }else{
        _labelPlaceholder.text = @"";
    }
}

#pragma mark - image picker

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        [self addImage:image];
    }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
