//
//  imagePreviewViewController.m
//  OneYuan
//
//  Created by 顺然 贾 on 16/6/4.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "ImagePreviewViewController.h"
#import "ShareViewController.h"


@implementation ImagePreviewViewController

@synthesize image;

- (void)viewDidLoad
{
    self.view.backgroundColor = [UIColor blackColor];
    [self setDisplayImage];
}


- (void)setDisplayImage
{
    self.imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0 , self.view.frame.size.width, self.view.frame.size.height)];
    
    if (image) {
        CGFloat width = image.size.width;
        CGFloat height = image.size.height;
        
        CGFloat displayWidth = self.view.frame.size.width;
        CGFloat displayHeight = displayWidth/width * height;
        [self.imageView setFrame:CGRectMake(0, self.view.frame.size.height / 2 - displayHeight / 2, self.view.frame.size.width, displayHeight)];
        [self.imageView setImage:image];
    }
    
    self.navigationController.topViewController.title = @"Preview";
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"delete" style:UIBarButtonItemStyleDone target:self action:@selector(clickButtonDelete)];
    self.navigationItem.rightBarButtonItem = item;
    
    [self.view addSubview:self.imageView];
    
    //    [self.imageView setContentScaleFactor:[[UIScreen mainScreen] scale]];
    //    self.imageView.contentMode =  UIViewContentModeScaleAspectFill;
    //    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    //    self.imageView.clipsToBounds  = YES;
    
    
}

- (void) clickButtonDelete
{
    ShareViewController *parent = (ShareViewController *)self.parent;
    
    [self.navigationController popViewControllerAnimated:YES];
    [parent deleteImage:image];
    
}

@end
