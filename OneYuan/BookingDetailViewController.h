//
//  BookingDetailViewController.h
//  OneYuan
//
//  Created by 顺然 贾 on 16/5/30.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BraintreeCore.h"
#import "BraintreeUI.h"

@interface BookingDetailViewController : OneBaseVC <BTDropInViewControllerDelegate,BTViewControllerPresentingDelegate,BTAppSwitchDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate,
UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

//@property (nonatomic, strong) UITextField *textFieldPrice;
@property (nonatomic, strong) UIButton *buttonConfirm;
@property (strong,nonatomic) UICollectionView *collectionView;
@property (nonatomic, strong) BTAPIClient *braintreeClient;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UILabel *labelPrice;
@property (nonatomic, assign) CGFloat coinCardWith;
@property (nonatomic, assign) int selectIndex;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, strong) UITextField *customPriceEdit;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIView *priceView;

@end
