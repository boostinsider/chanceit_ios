//
//  CalculateFunVC.m
//  OneYuan
//
//  Created by NW on 16/6/30.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "CalculateFunVC.h"

@implementation CalculateFunVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    self.title = [CommonHelper getLocalizedText:@"calculate_fun_nav_title"];
    __weak typeof (self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"btnback" htlImage:@"btnback" title:@"" action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    //
    
    NSString* str1 = [CommonHelper getLocalizedText:@"statement_no_relationship_with_app_inc"];
    
    
    CGFloat height1 = [CommonHelper getTextViewSize:str1 Width:self.view.frame.size.width - 16 * 2 Font:[UIFont systemFontOfSize:16]].height;
    
    UITextView* txtView1 = [[UITextView alloc]initWithFrame:CGRectMake(16, 4, self.view.frame.size.width - 16 * 2, height1)];
    txtView1.font = [UIFont systemFontOfSize:16];
    txtView1.editable = NO;
    txtView1.userInteractionEnabled = NO;
    txtView1.backgroundColor = [UIColor clearColor];
    txtView1.textColor = COLOR_RED;
    txtView1.text = str1;
    [self.view addSubview:txtView1];

    
//    self.view.backgroundColor = [UIColor lightGrayColor];
    
    
    NSString* str = [NSString stringWithFormat:@"%@\n%@%@%@\n\n%@%@%@%d%@%d%@\n\n%@%d + 10000001 = %d",
                     [CommonHelper getLocalizedText:@"result_rule_line_0"],
                     [CommonHelper getLocalizedText:@"result_rule_line_1_0"],@"such as 326829567",[CommonHelper getLocalizedText:@"result_rule_line_1_1"],
                     [CommonHelper getLocalizedText:@"result_rule_line_2_0"],@"such as 326829567",[CommonHelper getLocalizedText:@"result_rule_line_2_1"],24,
                     [CommonHelper getLocalizedText:@"result_rule_line_2_2"],326829567%24,[CommonHelper getLocalizedText:@"result_rule_line_2_3"],
                     [CommonHelper getLocalizedText:@"result_rule_line_3_0"],326829567%24,326829567%24 + 10000001];
    
    //        shop =         {
    //            "kaijang_num" = 10000062;
    //            "kaijiang_count" = 389363821;
    //            "kaijiang_ssc" = 0;
    //            no = 100003;
    //            number = 6480;
    //            state = 2;
    //        };
    
    CGFloat height = [CommonHelper getTextViewSize:str Width:self.view.frame.size.width - 16 * 2 Font:[UIFont systemFontOfSize:14]].height;
    
    UITextView* txtView = [[UITextView alloc]initWithFrame:CGRectMake(16, 4 + height1 + 14, self.view.frame.size.width - 16 * 2, height)];
    txtView.font = [UIFont systemFontOfSize:14];
    txtView.editable = NO;
    txtView.userInteractionEnabled = NO;
    txtView.backgroundColor = [UIColor clearColor];
    txtView.textColor = [UIColor blackColor];
    txtView.text = str;
    [self.view addSubview:txtView];
}
@end
