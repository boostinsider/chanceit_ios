//
//  ProductImageDetailsVC.h
//  OneYuan
//
//  Created by NW on 16/6/9.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductImageDetailsVC : OneBaseVC
- (id)initWithGoodsId:(int)goodsId codeId:(int)codeId;
@property (nonatomic,strong) NSNumber* periodNum;
@end
