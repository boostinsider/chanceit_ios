//
//  ProfileViewController.m
//  OneYuan
//
//  Created by NW on 16/6/13.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "ProfileViewController.h"
#import "NameEditViewController.h"
#import "BioEditViewController.h"
#import "GenderEditViewController.h"
#import "BirthdayEditViewController.h"

@interface ProfileViewController () <UITableViewDataSource,UITableViewDelegate>
{
     __block UITableView     *tbView;
    NSArray *arrTitles;
}
@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLoginOk) name:kDidLoginOk object:nil];
    
    self.title = [CommonHelper getLocalizedText:@"profile"];
    
    __weak typeof (self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"btnback" htlImage:@"btnback" title:@"" action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    arrTitles = @[[CommonHelper getLocalizedText:@"profile_avatar"],
                  [CommonHelper getLocalizedText:@"profile_nickname"],
                  [CommonHelper getLocalizedText:@"profile_sex"],
                  [CommonHelper getLocalizedText:@"profile_birthday"],
                  [CommonHelper getLocalizedText:@"profile_bio"],
                  ];
    
    tbView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tbView.delegate = self;
    tbView.dataSource = self;
    tbView.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    tbView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tbView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:tbView];
    
    [[NSNotificationCenter defaultCenter] addObserver:tbView selector:@selector(reloadData) name:kUserLogout object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:tbView selector:@selector(reloadData) name:kUserInfoUpdate object:nil];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrTitles.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 100;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

//- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    if (section == 0)
//    {
//        //        if ([UserInstance ShardInstnce].userId)
//        //        {
//        //            MineUserView* v = [[MineUserView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 130)];
//        //            v.delegate = self;
//        //            return v;
//        //        }
//        //        else
//        //        {
//        //            MineLoginView* v = [[MineLoginView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 130)];
//        //            v.delegate = self;
//        //            return v;
//        //        }
//        
//        if ([User getInstance].isLogin)
//        {
//            MineUserView* v = [[MineUserView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 130)];
//            v.delegate = self;
//            return v;
//        }
//        else
//        {
//            MineLoginView* v = [[MineLoginView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 130)];
//            v.delegate = self;
//            return v;
//        }
//    }
//    return nil;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell =  nil;//(UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"profile_cell"];
    }
    
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSString* title = nil;
    title = [arrTitles objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@",title];
    cell.separatorInset = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = false;
    cell.layoutMargins = UIEdgeInsetsZero;
    
    if (indexPath.row == 0) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        UIImageView* icon = [[UIImageView alloc]initWithFrame:CGRectMake(DESIGN_SCREEN_WIDTH - 66 - 16, 16, 66, 66)];
//        NSURL *url = [NSURL URLWithString:[User getInstance].photoUrl];
//        [icon sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"add"] options:SDWebImageRetryFailed];
        [icon setImage_oy:oyImageBaseUrl image:[User getInstance].photoUrl];
        
        icon.layer.cornerRadius = 20.0;
        icon.clipsToBounds = true;
        [cell.contentView addSubview :icon];

    }
    else if (indexPath.row == 1)
    {
         cell.detailTextLabel.text = [User getInstance].nickname;
    }
    else if (indexPath.row == 2)
    {
        cell.detailTextLabel.text = [User getInstance].gender;
    }
    else if (indexPath.row == 3)
    {
        cell.detailTextLabel.text = [User getInstance].birthday;
    }
    else if (indexPath.row == 4)
    {
        cell.detailTextLabel.text = [User getInstance].bio;
        
    }
//    cell.detailTextLabel.textColor = COLOR_DARK_GRAY;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([User getInstance].isLogin == NO) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            UIImagePickerController* imagePicker = [[UIImagePickerController alloc]init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            imagePicker.allowsEditing = true;
            imagePicker.navigationBar.tintColor = COLOR_WHITE;
            imagePicker.navigationBar.barTintColor = COLOR_WHITE;
            imagePicker.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
            UIView* topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
            topView.backgroundColor = NAVBAR_COLOR;
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
        else if (indexPath.row == 1)
        {
            NameEditViewController* vc = [[NameEditViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if (indexPath.row == 2)
        {
            GenderEditViewController* vc = [[GenderEditViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if (indexPath.row == 3)
        {
            BirthdayEditViewController* vc = [[BirthdayEditViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if (indexPath.row == 4)
        {
            BioEditViewController* vc = [[BioEditViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}

#pragma imagepicker
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo
{
    JGProgressHUD *progress = [[JGProgressHUD alloc] initWithStyle:JGProgressHUDStyleLight];
    progress.textLabel.text = [CommonHelper getLocalizedText: @"uploading"];
    [progress showInView:self.view];
    [ServerHelper uploadUserImage:image WithParams:nil Completion:^(NSDictionary *info) {
        NSLog(@"succeed");
        
        if ([info objectForKey:@"path"] && ![[info objectForKey:@"path"] isKindOfClass:NSNull.class]) {
            NSString* path = [info objectForKey:@"path"];
            progress.textLabel.text = [CommonHelper getLocalizedText: @"saving"];
            NSDictionary *postData = @{@"headimgurl":path};
            [ServerHelper updateUserInfoWithParameters:postData Completion:^(NSDictionary *user) {
                [progress dismissAnimated:YES];
                [[User getInstance]setPhotoUrl:path];
                [[NSNotificationCenter defaultCenter] postNotificationName:kUserInfoUpdate object:nil];
                
                [picker dismissViewControllerAnimated:YES completion:nil];
                
            } failure:^{
                [progress setIndicatorView:[JGProgressHUDErrorIndicatorView new]];
                progress.textLabel.text = [CommonHelper getLocalizedText: @"failed_to_save_data"];
                [progress dismissAfterDelay:1.0];
                
                [picker dismissViewControllerAnimated:YES completion:nil];
            }];
            
        }
        else
        {
            [progress setIndicatorView:[JGProgressHUDErrorIndicatorView new]];
            progress.textLabel.text = [CommonHelper getLocalizedText: @"failed_to_save_data"];
            [progress dismissAfterDelay:1.0];
            
            [picker dismissViewControllerAnimated:YES completion:nil];
        }
    } failure:^{
        [progress setIndicatorView:[JGProgressHUDErrorIndicatorView new]];
        progress.textLabel.text = [CommonHelper getLocalizedText: @"failed_to_save_data"];
        [progress dismissAfterDelay:1.0];
        
        [picker dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didLoginOk
{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
