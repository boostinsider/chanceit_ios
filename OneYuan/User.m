//
//  User.m
//  OneYuan
//
//  Created by NW on 16/5/25.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "User.h"

@implementation User

+ (instancetype) getInstance
{
    static User *_shareIns = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        _shareIns = [[User alloc] init];
        _shareIns.name = @"";
        _shareIns.coins = 0;
        _shareIns.activation = NO;
        _shareIns.country = @"";
        _shareIns.city = @"";
        _shareIns.province = @"";
        _shareIns.nickname = @"";
        _shareIns.createTime = 0;
        _shareIns.gender = @"";
        _shareIns.redPacket = 0;
        _shareIns.photoUrl = @"";
        _shareIns.idNum = -1;
        _shareIns.login_ip = 0;
        _shareIns.password = @"";
        _shareIns.qqopenid = @"";
        _shareIns.spread = 0;
        _shareIns.status = 0;
        _shareIns.turntable = 0;
        _shareIns.ucid = 0;
        _shareIns.unionid = @"";
        _shareIns.username = @"";
        _shareIns.wid = 0;
        _shareIns.wxlogin_time = 0;
        _shareIns.isLogin = NO;
        _shareIns.uid = 0;
        _shareIns.bio = @"";
        _shareIns.birthday = @"";
    });
    return _shareIns;
}

- (void) setUserInfo:(NSDictionary *)info
{
    id uid = [info objectForKey:@"id"];
    id name = [info objectForKey:@"name"];
    id coins = [info objectForKey:@"black"];
    id country = [info objectForKey:@"country"];
    id city = [info objectForKey:@"city"];
    id province = [info objectForKey:@"province"];
    id gender = [info objectForKey:@"sex"];
    id email = [info objectForKey:@"email"];
    id photoUrl = [info objectForKey:@"headimgurl"];
    
    if (uid)
        self.uid = [uid intValue];
    
    id status = [info objectForKey:@"status"];
    if (status && status != [NSNull null])
        self.status = [status intValue];
    
    id bio = [info objectForKey:@"bio"];
    if (bio && bio != [NSNull null])
        self.bio = bio;
    
    id birthday = [info objectForKey:@"birthday"];
    if (birthday && birthday != [NSNull null])
        self.birthday = birthday;
    
    id nickname = [info objectForKey:@"nickname"];
    if (nickname && nickname != [NSNull null])
        self.nickname = nickname;
    
    id turntable = [info objectForKey:@"turntable"];
    if (turntable && turntable != [NSNull null])
        self.turntable = [turntable intValue];
    
    
    if (name && name != [NSNull null])
        self.name = name;
    if (country && country != [NSNull null])
        self.country = country;
    if (city && city != [NSNull null])
        self.city = city;
    if (gender && gender != [NSNull null])
        self.gender = gender;
    if (email && email != [NSNull null])
        self.username = email;
    if (coins && coins != [NSNull null])
        self.coins = [coins intValue];
    if (photoUrl && photoUrl != [NSNull null])
        self.photoUrl = photoUrl;
    if (province && province != [NSNull null])
        self.province = province;
    
    self.isLogin = YES;
}

- (void) logout
{
    self.name = @"";
    self.country = @"";
    self.city = @"";
    self.gender = 0;
    self.username = @"";
    self.nickname = @"";
    self.turntable = 0;
    self.status = 0;
    self.photoUrl = @"";
    self.isLogin = NO;
}

@end
