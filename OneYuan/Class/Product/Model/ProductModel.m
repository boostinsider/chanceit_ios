//
//  ProductModel.m
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/27.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import "ProductModel.h"

@implementation ProductPics
@synthesize picName,picRemark;
@end

@implementation ProductTopic
@synthesize Topic,Reply;
@end

@implementation ProductInfo
@synthesize  goodsName,goodsAltName,codeID,codePrice,codeQuantity,codeSales,codeState,codePeriod,codeType,seconds,codeRNO,codeRTime,codeRBuyTime,codeRIpAddr,userName,userWeb,userPhoto,codeRUserBuyCount;
@end

@implementation ProductDetail
@synthesize Rows1,Rows2,Rows3;
+(Class)Rows1_class
{
    return [ProductPics class];
}
@end


@implementation ProductLottery
@synthesize codePeriod,codeGoodsID,goodsName,codeGoodsPic,codePrice,codeState,codeRNO,codeRTime,codeType,codeRBuyTime,codeRIpAddr,codeRUserBuyCount,userName,userWeb,userPhoto,codeQuantity,codeSales;
@end

@implementation ProductCodeBuy
@synthesize buyTime,rnoNum;
@end

@implementation ProductLotteryDetail
@synthesize Rows1,Rows2,Rows3,Rows4;
+(Class)Rows1_class
{
    return [ProductPics class];
}
+(Class)Rows4_class
{
    return [ProductCodeBuy class];
}
@end

#pragma custom start

//Processing.m

@implementation CalculateItemKJTime
@synthesize create_date;
@synthesize create_hour;
@synthesize create_int;
@synthesize create_time;
@synthesize name;
@synthesize nickname;
@synthesize no;
@synthesize pid;
@synthesize shop_url;
@synthesize user_id;
@synthesize user_url;
@end

@implementation CalculateItemShop
@synthesize kaijang_num;
@synthesize kaijiang_count;
@synthesize kaijiang_ssc;
@synthesize no;
@synthesize number;
@synthesize state;
@end

@implementation CalculateItem
@synthesize kjtime;
@synthesize shop;
+ (Class)kjtime_class {
    return [CalculateItemKJTime class];
}

+ (Class)shop_class {
    return [CalculateItemShop class];
}
@end


@implementation CalculateData
@synthesize status, data;

+ (Class)data_class {
    return [CalculateItem class];
}
@end

//shop over
@implementation ShopOverUser : OneBaseParser
@synthesize address;
@synthesize id;
@synthesize img;
@synthesize name;
@synthesize user_url;
@end

@implementation ShopOverData : OneBaseParser
@synthesize  attend_no;
@synthesize  buy_price;
@synthesize  buy_url;
@synthesize  category;
@synthesize  cid;
@synthesize  content;
@synthesize  count;
@synthesize  create_time;
@synthesize  ctitle;
@synthesize  description;
@synthesize  edit_price;
@synthesize  end_time;
@synthesize  express_name;
@synthesize  express_no;
@synthesize  hits;
@synthesize  id;
@synthesize  jd;
@synthesize  kaijang_diffe;
@synthesize  kaijang_num;
@synthesize  kaijang_time;
@synthesize  kaijang_timing;
@synthesize  kaijiang_count;
@synthesize  kaijiang_ssc;
@synthesize  keywords;
@synthesize  meta_title;
@synthesize  moreurl;
@synthesize  name;
@synthesize  no,shared;
@synthesize  number;
@synthesize  pic;
@synthesize  pid;
@synthesize  position;
@synthesize  price;
@synthesize  sid;
@synthesize  state;
@synthesize  surplus;
@synthesize  uid;
@synthesize  url;
@synthesize user;

+ (Class)user_class {
    return [ShopOverUser class];
}

@end

@implementation ShopOverResult : OneBaseParser
@synthesize status;
@synthesize data;

+ (Class)data_class {
    return [ShopOverData class];
}
@end

@implementation ProductDetailData
@synthesize buy_price;
@synthesize buy_url;
@synthesize category;
@synthesize content;
@synthesize cover_id;
@synthesize create_time;
@synthesize description;
@synthesize display;
@synthesize edit_price;
@synthesize id;
@synthesize keywords;
@synthesize meta_title;
@synthesize name;
@synthesize pic;
@synthesize position;
@synthesize price;
@synthesize status;
@synthesize update_time;
@end

@implementation ProductDetailResult
@synthesize status;
@synthesize data;
@end


#pragma custom end

@implementation ProductModel

+ (void)getGoodDetail:(int)goodsId success:(void(^)(AFHTTPRequestOperation* operation, NSObject* result))success failure:(void(^)(NSError* error))failure
{
    NSString* url = [NSString stringWithFormat:oyGoodsDetail,goodsId];
    [[XBApi SharedXBApi] requestWithURL:url paras:nil type:XBHttpResponseType_Json success:success failure:failure];
}

+ (void)getGoodLottery:(int)codeId success:(void(^)(AFHTTPRequestOperation* operation, NSObject* result))success failure:(void(^)(NSError* error))failure
{
    NSString* url = [NSString stringWithFormat:oyGoodsLottery,codeId];
    [[XBApi SharedXBApi] requestWithURL:url paras:nil type:XBHttpResponseType_Json success:success failure:failure];
}
@end
