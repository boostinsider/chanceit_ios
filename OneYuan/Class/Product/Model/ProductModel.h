//
//  ProductModel.h
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/27.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductPics : OneBaseParser
@property(nonatomic,copy)NSString* picName;
@property(nonatomic,copy)NSString* picRemark;
@end

@interface ProductInfo : OneBaseParser
@property(nonatomic,copy)NSString* goodsName;
@property(nonatomic,copy)NSString* goodsAltName;
@property(nonatomic,copy)NSNumber* codeID;
@property(nonatomic,copy)NSNumber* codePrice;
@property(nonatomic,copy)NSNumber* codeQuantity;
@property(nonatomic,copy)NSNumber* codeSales;
@property(nonatomic,copy)NSNumber* codeState;
@property(nonatomic,copy)NSNumber* codePeriod;
@property(nonatomic,copy)NSNumber* codeType;
@property(nonatomic,copy)NSNumber* seconds;
@property(nonatomic,copy)NSNumber* codeRNO;
@property(nonatomic,copy)NSString* codeRTime;
@property(nonatomic,copy)NSString* codeRBuyTime;
@property(nonatomic,copy)NSString* codeRIpAddr;
@property(nonatomic,copy)NSString* userName;
@property(nonatomic,copy)NSString* userWeb;
@property(nonatomic,copy)NSString* userPhoto;
@property(nonatomic,copy)NSNumber* codeRUserBuyCount;
@end

@interface ProductTopic : OneBaseParser
@property(nonatomic,copy)NSNumber* Topic;
@property(nonatomic,copy)NSNumber* Reply;
@end

@interface ProductDetail : OneBaseParser
@property(nonatomic,copy)NSArray* Rows1;
@property(nonatomic,strong)ProductInfo* Rows2;
@property(nonatomic,strong)ProductTopic* Rows3;
@end

@interface ProductLottery : OneBaseParser
@property(nonatomic,copy)NSNumber* codePeriod;
@property(nonatomic,copy)NSNumber* codeGoodsID;
@property(nonatomic,copy)NSString* goodsName;
@property(nonatomic,copy)NSString* codeGoodsPic;
@property(nonatomic,copy)NSNumber* codePrice;
@property(nonatomic,copy)NSNumber* codeState;
@property(nonatomic,copy)NSNumber* codeRNO;
@property(nonatomic,copy)NSString* codeRTime;
@property(nonatomic,copy)NSNumber* codeType;
@property(nonatomic,copy)NSString* codeRBuyTime;
@property(nonatomic,copy)NSString* codeRIpAddr;
@property(nonatomic,copy)NSNumber* codeRUserBuyCount;
@property(nonatomic,copy)NSString* userName;
@property(nonatomic,copy)NSString* userWeb;
@property(nonatomic,copy)NSString* userPhoto;
@property(nonatomic,copy)NSNumber* codeQuantity;
@property(nonatomic,copy)NSNumber* codeSales;
@end

@interface ProductCodeBuy : OneBaseParser
@property(nonatomic,copy)NSString* buyTime;
@property(nonatomic,copy)NSString* rnoNum;
@end

@interface ProductLotteryDetail : OneBaseParser
@property(nonatomic,copy)NSArray* Rows1;
@property(nonatomic,strong)ProductLottery* Rows2;
@property(nonatomic,strong)ProductTopic* Rows3;
@property(nonatomic,strong)NSArray* Rows4;
@end

//calculate

@interface CalculateItemKJTime : OneBaseParser
@property (nonatomic, copy) NSString *create_date;
@property (nonatomic, copy) NSString *create_hour;
@property (nonatomic, copy) NSNumber *create_int;
@property (nonatomic, copy) NSNumber *create_time;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSNumber *no;
@property (nonatomic, copy) NSNumber *pid;
@property (nonatomic, copy) NSString *shop_url;
@property (nonatomic, copy) NSNumber *user_id;
@property (nonatomic, copy) NSString *user_url;
@end

@interface CalculateItemShop : OneBaseParser
@property (nonatomic, copy) NSNumber *kaijang_num;
@property (nonatomic, copy) NSNumber *kaijiang_count;
@property (nonatomic, copy) NSNumber *kaijiang_ssc;
@property (nonatomic, copy) NSNumber *no;
@property (nonatomic, copy) NSNumber *number;
@property (nonatomic, copy) NSNumber *state;
@end

@interface CalculateItem : OneBaseParser
@property (nonatomic, retain) NSArray* kjtime;
@property (nonatomic, retain) CalculateItemShop* shop;
@end

@interface CalculateData : OneBaseParser
@property (nonatomic, copy) NSNumber *status;
@property (nonatomic, retain) CalculateItem *data;
@end

//shop over

@interface ShopOverUser : OneBaseParser
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSNumber *id;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *user_url;
@end

@interface ShopOverData : OneBaseParser
@property (nonatomic, copy) NSArray* attend_no;
@property (nonatomic, copy) NSNumber* buy_price;
@property (nonatomic, copy) NSString* buy_url;
@property (nonatomic, copy) NSNumber* category;
@property (nonatomic, copy) NSNumber* cid;
@property (nonatomic, copy) NSString* content;
@property (nonatomic, copy) NSNumber* count;
@property (nonatomic, copy) NSNumber* create_time;
@property (nonatomic, copy) NSString* ctitle;
@property (nonatomic, copy) NSString* description;
@property (nonatomic, copy) NSNumber* edit_price;
@property (nonatomic, copy) NSNumber* end_time;
@property (nonatomic, copy) NSString* express_name;
@property (nonatomic, copy) NSString* express_no;
@property (nonatomic, copy) NSNumber* hits;
@property (nonatomic, copy) NSNumber* id;
@property (nonatomic, copy) NSNumber* jd;
@property (nonatomic, copy) NSString* kaijang_diffe;
@property (nonatomic, copy) NSNumber* kaijang_num;
@property (nonatomic, copy) NSString* kaijang_time;
@property (nonatomic, copy) NSNumber* kaijang_timing;
@property (nonatomic, copy) NSNumber* kaijiang_count;
@property (nonatomic, copy) NSNumber* kaijiang_ssc;
@property (nonatomic, copy) NSString* keywords;
@property (nonatomic, copy) NSString* meta_title;
@property (nonatomic, copy) NSString* moreurl;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* no;
@property (nonatomic, copy) NSNumber* shared;

@property (nonatomic, copy) NSNumber* number;
@property (nonatomic, copy) NSString* pic;
@property (nonatomic, copy) NSNumber* pid;
@property (nonatomic, copy) NSNumber* position;
@property (nonatomic, copy) NSNumber* price;
@property (nonatomic, copy) NSNumber* sid;
@property (nonatomic, copy) NSString* state;
@property (nonatomic, copy) NSNumber* surplus;
@property (nonatomic, copy) NSNumber* uid;
@property (nonatomic, copy) NSString* url;
@property (nonatomic, retain) ShopOverUser* user;

@end

@interface ShopOverResult : OneBaseParser
@property (nonatomic, copy) NSNumber *status;
@property (nonatomic, retain) ShopOverData *data;
@end


//product details

@interface ProductDetailData : OneBaseParser
@property (nonatomic, copy) NSNumber *buy_price;
@property (nonatomic, copy) NSString *buy_url;
@property (nonatomic, copy) NSNumber *category;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSNumber *cover_id;
@property (nonatomic, copy) NSNumber *create_time;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, copy) NSNumber *display;
@property (nonatomic, copy) NSNumber *edit_price;
@property (nonatomic, copy) NSNumber *id;
@property (nonatomic, copy) NSString *keywords;
@property (nonatomic, copy) NSString *meta_title;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, copy) NSNumber *position;
@property (nonatomic, copy) NSNumber *price;
@property (nonatomic, copy) NSNumber *status;
@property (nonatomic, copy) NSNumber *update_time;
@end

@interface ProductDetailResult : OneBaseParser
@property (nonatomic, copy) NSNumber *status;
@property (nonatomic, retain) ProductDetailData *data;
@end

@interface ProductModel : NSObject
+ (void)getGoodDetail:(int)goodsId success:(void(^)(AFHTTPRequestOperation* operation, NSObject* result))success failure:(void(^)(NSError* error))failure;
+ (void)getGoodLottery:(int)codeId success:(void(^)(AFHTTPRequestOperation* operation, NSObject* result))success failure:(void(^)(NSError* error))failure;
@end
