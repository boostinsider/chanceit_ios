//
//  ProductLotteryCodeCell.m
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/28.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import "ProductLotteryCodeCell.h"

@interface ProductLotteryCodeCell ()
{
    UILabel* lblTime;
    UILabel* lblCode;
    
    UILabel* lblCode1;
    UILabel* lblCode2;
    UILabel* lblCode3;
    UILabel* lblCode4;
    
}
@end

@implementation ProductLotteryCodeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if(self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        lblTime = [[UILabel alloc] initWithFrame:CGRectMake(16, 10, mainWidth, 13)];
        lblTime.font = [UIFont systemFontOfSize:13];
        lblTime.textColor = [UIColor grayColor];
        [self addSubview:lblTime];
        
        lblCode = [[UILabel alloc] init];
        lblCode.font = [UIFont systemFontOfSize:13];
        lblCode.textColor = [UIColor lightGrayColor];
        lblCode.numberOfLines = 9999;
        lblCode.lineBreakMode = NSLineBreakByWordWrapping;
        [self addSubview:lblCode];
        
        lblCode1 = [[UILabel alloc] init];
        lblCode1.font = [UIFont systemFontOfSize:13];
        lblCode1.textColor = [UIColor lightGrayColor];
        lblCode1.numberOfLines = 9999;
        lblCode1.lineBreakMode = NSLineBreakByWordWrapping;
        [self addSubview:lblCode1];
        
        lblCode2 = [[UILabel alloc] init];
        lblCode2.font = [UIFont systemFontOfSize:13];
        lblCode2.textColor = [UIColor lightGrayColor];
        lblCode2.numberOfLines = 9999;
        lblCode2.lineBreakMode = NSLineBreakByWordWrapping;
        [self addSubview:lblCode2];
        
        lblCode3 = [[UILabel alloc] init];
        lblCode3.font = [UIFont systemFontOfSize:13];
        lblCode3.textColor = [UIColor lightGrayColor];
        lblCode3.numberOfLines = 9999;
        lblCode3.lineBreakMode = NSLineBreakByWordWrapping;
        [self addSubview:lblCode3];
        
        lblCode4 = [[UILabel alloc] init];
        lblCode4.font = [UIFont systemFontOfSize:13];
        lblCode4.textColor = [UIColor lightGrayColor];
        lblCode4.numberOfLines = 9999;
        lblCode4.lineBreakMode = NSLineBreakByWordWrapping;
        [self addSubview:lblCode4];
    }
    return self;
}

- (void)setCodes:(ProductCodeBuy*)codes
{
    NSArray* codeCount = [[Jxb_Common_Common sharedInstance] getSpiltString:codes.rnoNum split:@","];
    lblTime.text = [NSString stringWithFormat:@"%@   -   %d %@",codes.buyTime,(int)codeCount.count,[CommonHelper getLocalizedText:@"price_number"]];
    
    lblCode.text = codes.rnoNum;
    CGSize s = [lblCode.text textSizeWithFont:lblCode.font constrainedToSize:CGSizeMake(mainWidth - 30, 999) lineBreakMode:NSLineBreakByWordWrapping];
    lblCode.frame = CGRectMake(16, 30, mainWidth - 30, s.height);
}

- (void)setCustomCodes:(ProductCodeBuy*)codes AndData:(ShopOverData*) shopOverData
{
//    NSArray* codeCount = [[Jxb_Common_Common sharedInstance] getSpiltString:codes.rnoNum split:@","];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd HH:mm"];
    NSString *buyTimeStr = [NSString stringWithFormat:@"%@",
                            [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[shopOverData.end_time integerValue]/1000]]];
    NSString* btime = buyTimeStr;
    
    lblTime.text = [NSString stringWithFormat:@"%@   -   %d %@",btime,[shopOverData.count intValue],[CommonHelper getLocalizedText:@"price_number"]];
    NSMutableString* str1 = [NSMutableString stringWithFormat:@""];
    NSMutableString* str2 = [NSMutableString stringWithFormat:@""];
    NSMutableString* str3 = [NSMutableString stringWithFormat:@""];
    NSMutableString* str4 = [NSMutableString stringWithFormat:@""];
    int index = 0;
    for (NSString* item in shopOverData.attend_no) {
        int y = index % 4 ;
        switch (y) {
            case 0:
            {
                [str1 appendFormat:@"%@\n",item];
            }
                break;
            case 1:
            {
                [str2 appendFormat:@"%@\n",item];
            }
                break;
            case 2:
            {
                [str3 appendFormat:@"%@\n",item];
            }
                break;
            case 3:
            {
                [str4 appendFormat:@"%@\n",item];
            }
                break;
                
            default:
                break;
        }
        
        index++;
    }
    CGFloat itemWidth = (mainWidth - 16 * 2) / 4;
    
    lblCode1.text = str1;
    CGSize s1 = [lblCode1.text textSizeWithFont:lblCode1.font constrainedToSize:CGSizeMake(itemWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    lblCode1.frame = CGRectMake(16, 30, itemWidth, s1.height);
    
    lblCode2.text = str2;
    CGSize s2 = [lblCode2.text textSizeWithFont:lblCode2.font constrainedToSize:CGSizeMake(itemWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    lblCode2.frame = CGRectMake(16 + itemWidth, 30, itemWidth, s2.height);
    
    lblCode3.text = str3;
    CGSize s3 = [lblCode3.text textSizeWithFont:lblCode3.font constrainedToSize:CGSizeMake(itemWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    lblCode3.frame = CGRectMake(16 + itemWidth * 2, 30, itemWidth, s3.height);
    
    lblCode4.text = str4;
    CGSize s4 = [lblCode4.text textSizeWithFont:lblCode4.font constrainedToSize:CGSizeMake(itemWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    lblCode4.frame = CGRectMake(16 + itemWidth * 3, 30, itemWidth, s4.height);
}

@end
