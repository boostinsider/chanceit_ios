//
//  ProductDetailGetCell.m
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/27.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import "ProductDetailGetCell.h"

@interface ProductDetailGetCell ()
{
    UIImageView     *imgUser;
    UILabel         *lblName;
    UILabel         *lblRTime;
    UILabel         *lblBTime;
    UILabel         *lblRNo;
    UILabel         *lblArea;
    UILabel         *lblBuyCount;
}
@end

@implementation ProductDetailGetCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if(self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UILabel* lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, mainWidth, 14)];
        lblTitle.text = [CommonHelper getLocalizedText:@"last_winner"];
        lblTitle.textColor = [UIColor grayColor];
        lblTitle.font = [UIFont systemFontOfSize:15];
        [self addSubview:lblTitle];
        
        imgUser = [[UIImageView alloc] initWithFrame:CGRectMake(16, 35, 40, 40)];
        imgUser.image = [UIImage imageNamed:@"noimage"];
        imgUser.layer.cornerRadius = imgUser.frame.size.height / 2;
        imgUser.layer.masksToBounds = YES;
        [self addSubview:imgUser];
        
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(70, 30, mainWidth - 70, 14)];
        lblName.textColor = [UIColor hexFloatColor:@"3385ff"];
        lblName.font = [UIFont systemFontOfSize:12];
        [self addSubview:lblName];
        
        lblRTime = [[UILabel alloc] initWithFrame:CGRectMake(lblName.frame.origin.x, 50, mainWidth - 70, 14)];
        lblRTime.textColor = [UIColor lightGrayColor];
        lblRTime.font = [UIFont systemFontOfSize:12];
        [self addSubview:lblRTime];
        
        lblBTime = [[UILabel alloc] initWithFrame:CGRectMake(lblName.frame.origin.x, 65, mainWidth - 70, 14)];
        lblBTime.textColor = [UIColor lightGrayColor];
        lblBTime.font = [UIFont systemFontOfSize:12];
        [self addSubview:lblBTime];
        
        lblBuyCount = [[UILabel alloc] initWithFrame:CGRectMake(100, 10, 200, 14)];
        lblBuyCount.textColor = mainColor;
        lblBuyCount.font = [UIFont systemFontOfSize:12];
        [self addSubview:lblBuyCount];
        
        
        UILabel* lblno = [[UILabel alloc] initWithFrame:CGRectMake(lblName.frame.origin.x, 80, 100, 14)];
        lblno.text = [NSString stringWithFormat :@"%@:",[CommonHelper getLocalizedText:@"lucky_number"]];;
        lblno.textColor = [UIColor lightGrayColor];
        lblno.font = [UIFont systemFontOfSize:12];
        [self addSubview: lblno];
        
        lblRNo = [[UILabel alloc] initWithFrame:CGRectMake(lblName.frame.origin.x + 100, 80, mainWidth - 70, 14)];
        lblRNo.textColor = mainColor;
        lblRNo.font = [UIFont systemFontOfSize:12];
        [self addSubview:lblRNo];
        
        lblArea = [[UILabel alloc] init];
        lblArea.textColor = [UIColor lightGrayColor];
        lblArea.font = [UIFont systemFontOfSize:12];
        [self addSubview:lblArea];
    }
    return self;
}

- (void)setProDetail:(ProductDetail*)detail
{
    if(!detail)
        return;
    [imgUser setImage_oy:oyHeadBaseUrl image:detail.Rows2.userPhoto];
    
    lblName.text = detail.Rows2.userName;
    NSString* rtime = detail.Rows2.codeRTime;
    lblRTime.text = [NSString stringWithFormat:@"%@：%@",[CommonHelper getLocalizedText:@"announced_time"], rtime];
    NSString* btime = detail.Rows2.codeRBuyTime;
    lblBTime.text = [NSString stringWithFormat:@"%@：%@",[CommonHelper getLocalizedText:@"buy_time"], btime];
    lblRNo.text = [detail.Rows2.codeRNO stringValue];
    
    lblBuyCount.text = [NSString stringWithFormat:@"总云购 %d %@",[detail.Rows2.codeRUserBuyCount intValue],[CommonHelper getLocalizedText:@"price_number"]];
    
    CGSize s = [lblName.text textSizeWithFont:lblName.font constrainedToSize:CGSizeMake(MAXFLOAT, 999) lineBreakMode:NSLineBreakByCharWrapping];
    lblArea.text = [NSString stringWithFormat:@"(%@)",detail.Rows2.codeRIpAddr];
    lblArea.frame = CGRectMake(lblName.frame.origin.x + s.width, lblName.frame.origin.y, 200, 14);
}

- (void)setCustomProDetail:(ShopOverData*)detail
{
    if(!detail)
        return;
    [imgUser setImage_oy:oyHeadBaseUrl image:detail.user.img];
    
    lblName.text = detail.user.name;
    NSString* rtime = detail.kaijang_time;
    lblRTime.text = [NSString stringWithFormat:@"%@：%@",[CommonHelper getLocalizedText:@"announced_time"], rtime];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd HH:mm"];
    NSString *buyTimeStr = [NSString stringWithFormat:@"%@",
                     [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[detail.end_time integerValue]/1000]]];
    NSString* btime = buyTimeStr;
    lblBTime.text = [NSString stringWithFormat:@"%@：%@",[CommonHelper getLocalizedText:@"buy_time"], btime];
    lblRNo.text = detail.kaijang_num;
    
    lblBuyCount.text = [NSString stringWithFormat:@"%@ %d %@",[CommonHelper getLocalizedText:@"total"],[detail.number intValue],[CommonHelper getLocalizedText:@"price_number"]];
    
    CGSize s = [lblName.text textSizeWithFont:lblName.font constrainedToSize:CGSizeMake(MAXFLOAT, 999) lineBreakMode:NSLineBreakByCharWrapping];
    lblArea.text = [NSString stringWithFormat:@"(%@)",detail.user.address];
    lblArea.frame = CGRectMake(lblName.frame.origin.x + s.width, lblName.frame.origin.y, 200, 14);
}

@end
