//
//  ProductDetailVC.h
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/27.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailVC : OneBaseVC <UIActionSheetDelegate,UIActivityItemSource>
- (id)initWithGoodsId:(int)goodsId codeId:(int)codeId;

@property (nonatomic,strong) NSNumber* periodNum;
@property (nonatomic,assign) BOOL hasSetPeriodNum;
@property (nonatomic,assign) int productStatus;
@property (nonatomic,strong) NSString *shortUrlFacebook;
@property (nonatomic, strong) NSString *shortUrlTwtter;
@property (nonatomic, strong) NSString *shortUrlMail;

@end
