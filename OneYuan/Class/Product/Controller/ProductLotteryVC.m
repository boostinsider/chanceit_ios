//
//  ProductLotteryVC.m
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/28.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import "ProductLotteryVC.h"
#import "ProductModel.h"
#import "ProductLotteryTopCell.h"
#import "ProductLotteryCodeCell.h"
#import "ShowOrderListVC.h"
#import "ProductBuyListVC.h"
#import "ProductLotteryOptView.h"
#import "ProductDetailVC.h"
#import "AllProModel.h"
#import "CalculateListVC.h"

@interface ProductLotteryVC ()<UITableViewDataSource,UITableViewDelegate,ProductLotteryOptViewDelegate>
{
    int _goodsId;
    int _codeId;
    
    __block ProductLotteryOptView   *optView;
    __block UITableView             *tbView;
    
    __block ProductLotteryDetail    *_detail;
    __block AllProPeriodList        *_allCodePeriod;
    
    __block CalculateData           *_calculateData;
    __block ShopOverResult          *_shopOverResult;
}
@end

@implementation ProductLotteryVC

- (id)initWithGoods:(int)goodsId codeId:(int)codeId
{
    self = [super init];
    if(self)
    {
        _goodsId = goodsId;
        _codeId = codeId;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = [CommonHelper getLocalizedText:@"announced_result_nav_title"];
    self.view.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    __weak typeof(self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"btnback" htlImage:@"btnback" title:@"" action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    CGFloat margin = 0;
    if(![OyTool ShardInstance].bIsForReview)
    {
        margin = 44;
        optView = [[ProductLotteryOptView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 107, mainWidth, 44)];
        optView.delegate = self;
        [self.view addSubview:optView];
    }
    
    tbView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.view.bounds.size.height - margin) style:UITableViewStyleGrouped];
    tbView.delegate = self;
    tbView.dataSource = self;
    tbView.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    tbView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tbView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:tbView];
    
    [tbView addPullToRefreshWithActionHandler:^{
        [wSelf getData];
    }];
    [tbView.pullToRefreshView setOYStyle];
    
    [self getData];
    
    [self showLoad];
    
    [self getProductAllCodes];
}

- (void)getData// 获取商品开奖信息
{
    __weak typeof(self) wSelf = self;
    [ProductModel getGoodLottery:_codeId success:^(AFHTTPRequestOperation* operation, NSObject* result){
        [[XBToastManager ShardInstance] hideprogress];
        [wSelf hideLoad];
        [tbView.pullToRefreshView stopAnimating];
        _detail = [[ProductLotteryDetail alloc] initWithDictionary:(NSDictionary*)result];
        [tbView reloadData];
    } failure:^(NSError* error){
        [wSelf hideLoad];
        [tbView.pullToRefreshView stopAnimating];
    }];
    
    
    NSDictionary* dic = @{@"pid":@(_codeId)};
    [ServerHelper getAnnouncedDetails:dic Completion:^(NSDictionary *products) {
        NSLog(@"getAnnouncedProductsWithParameters succeed!");
        [[XBToastManager ShardInstance] hideprogress];
//        [wSelf hideLoad];
//        [tbView.pullToRefreshView stopAnimating];
//        
//        _calculateData = [[CalculateData alloc]initWithDictionary:products];
//        [tbView reloadData];
        
        //        if(block!=NULL)
        //            block();
        //        NewestProList* list = [[NewestProList alloc] initWithDictionary:(NSDictionary*)products];
        //        if (!listNew)
        //        {
        //            listNew = [NSMutableArray arrayWithArray:list.Rows];
        //        }
        //        else
        //        {
        //            [listNew addObjectsFromArray:list.Rows];
        //        }
        //        [tbView reloadData];
        //        [tbView setShowsInfiniteScrolling:listNew.count<[list.count integerValue]];
        //set new
        
        //        processingData = [[ProcessingList alloc]initWithDictionary:products];
        //        ProcessingList* list = [[ProcessingList alloc] initWithDictionary:(NSDictionary*)products];
        
        //        if (list.data && [list.data isKindOfClass:NSArray.class]) {
        //            listNew =  [NSMutableArray arrayWithArray:list.data];
        //            [tbView reloadData];
        //        }
    } failure:^{
        NSLog(@"getAnnouncedProductsWithParameters failed!");
        //[[XBToastManager ShardInstance] showtoast:[NSString stringWithFormat:@"获取最新揭晓页面数据异常:%@",error]];
        [wSelf hideLoad];
        [tbView.pullToRefreshView stopAnimating];
    }];
    
    [ServerHelper getShopOverWithParameters:dic Completion:^(NSDictionary *result) {
        NSLog(@"getShopOverWithParameters succeed!");
        [tbView.pullToRefreshView stopAnimating];
        [tbView.infiniteScrollingView stopAnimating];
        
        [wSelf hideLoad];
        
        _shopOverResult = [[ShopOverResult alloc]initWithDictionary:result];
        
        //latest pid
        NSDictionary* dic2 = @{@"sid":_shopOverResult.data.sid};
        [ServerHelper getLatestShopPeriodInfo:dic2 Completion:^(id result) {
            int latestPid = [[result objectForKey:@"id"] intValue];
            
            NSDictionary* dic3 = @{@"pid":@(latestPid)};
            [ServerHelper getShopOverWithParameters:dic3 Completion:^(NSDictionary *result) {
                NSLog(@"get latest period succeed1!");
                
                [tbView.pullToRefreshView stopAnimating];
                [tbView.infiniteScrollingView stopAnimating];
                
                ShopOverResult* latestOver = [[ShopOverResult alloc]initWithDictionary:result];
                [optView setCustomBtnPeriod:latestOver.data];
            } failure:^{
                NSLog(@"get latest period failed!2");
            }];
            NSLog(@"get latest period succeed!");
        } failure:^{
            NSLog(@"get latest period failed!");
        }];
//        [optView setCustomBtnPeriod:_shopOverResult.data];
        [tbView reloadData];
    } failure:^{
        NSLog(@"getShopOverWithParameters failed!");
        //[[XBToastManager ShardInstance] showtoast:[NSString stringWithFormat:@"获取最新揭晓页面数据异常:%@",error]];
    }];
    
    
    
    
//    __weak typeof(self) wSelf = self;
    NSDictionary *params = @{@"pid":@(_codeId),
                             @"page_size":@10,
                             @"page":@(1)};
    [ServerHelper getShopRecordWithParameters:params Completion:^(id result) {
        NSLog(@"123"); 
//        [tbView.pullToRefreshView stopAnimating];
//        [tbView.infiniteScrollingView stopAnimating];
//        if(block!=NULL)
//            block();
//        if (!result || result == [NSNull null]) return ;
//        if (!arrData)
//            arrData = [NSMutableArray array];
//        
//        NSArray *array = [result objectForKey:@"list"];
//        
//        for (id tmp in array )
//        {
//            ProdcutBuyItem * item = [[ProdcutBuyItem alloc] init];
//            item.userPhoto = [tmp objectForKey:@"img"];
//            item.userName = [tmp objectForKey:@"name"];
//            item.buyTime = [tmp objectForKey:@"time"];
//            NSDictionary *number = [tmp objectForKey:@"number"];
//            item.buyNum = [NSString stringWithFormat:@"%@",[number objectForKey:@"count"]];
//            [arrData addObject:item];
//        }
//        
//        
//        [tbView reloadData];
//        if(arrData.count ==0)
//        {
//            [wSelf showEmpty];
//        }
//        
        
    } failure:^{
//        [tbView.pullToRefreshView stopAnimating];
//        [tbView.infiniteScrollingView stopAnimating];
    }];
}

- (void)getProductAllCodes //获取 所有期号
{
    __weak typeof(self) wSelf = self;
    [AllProModel getGoodsPeriodByCodeId:_codeId success:^(AFHTTPRequestOperation* operation, NSObject* result){
        _allCodePeriod = [[AllProPeriodList alloc] initWithDictionary:(NSDictionary*)result];
        AllProPeriod* p = nil;
        int index = -1;
        for (AllProPeriod* period in _allCodePeriod.Rows) {
            index ++;
            if([period.codeState intValue] == 3)
            {
                p = period;
                break;
            }
        }
        [optView setBtnPeriod:[_allCodePeriod.Rows objectAtIndex:0]];
        
        if(index > 0)
        {
            NSMutableArray* tmp = [NSMutableArray arrayWithArray:_allCodePeriod.Rows];
            for(int i =0;i<index;i++)
            {
                [tmp removeObjectAtIndex:0];
            }
            _allCodePeriod.Rows = tmp;
        }
        if([p.codeID intValue] != _codeId)
        {
            _codeId = [p.codeID intValue];
            [wSelf getData];
        }
        else
            [tbView reloadData];
    } failure:^(NSError* error){
        
    }];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    return !_detail ? 0 : 3;
    return _shopOverResult ? 3 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
        return 1;
    if(section == 2)
    {
//        return 1+_detail.Rows4.count;
        
        return 1 + 1 + 1;//假设只有一个号 //test niewen
    }
    
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
        return 100;
    if(indexPath.section == 2 && indexPath.row > 1)
    {
        ProductCodeBuy* codes = [_detail.Rows4 objectAtIndex:indexPath.row - 1];
//        NSString* codeList = [codes rnoNum];
        
//        NSString* codeList = [_shopOverResult.data.attend_no objectAtIndex:0];//test 自己买的所有号的列表 ？ //test niewen
//        CGSize s = [codeList textSizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(mainWidth - 30, 999) lineBreakMode:NSLineBreakByWordWrapping];
        
        NSMutableString* str1 = [NSMutableString stringWithFormat:@""];
        NSMutableString* str2 = [NSMutableString stringWithFormat:@""];
        NSMutableString* str3 = [NSMutableString stringWithFormat:@""];
        NSMutableString* str4 = [NSMutableString stringWithFormat:@""];
        int index = 0;
        for (NSString* item in _shopOverResult.data.attend_no) {
            int y = index % 4 ;
            switch (y) {
                case 0:
                {
                    [str1 appendFormat:@"%@\n",item];
                }
                    break;
                case 1:
                {
                    [str2 appendFormat:@"%@\n",item];
                }
                    break;
                case 2:
                {
                    [str3 appendFormat:@"%@\n",item];
                }
                    break;
                case 3:
                {
                    [str4 appendFormat:@"%@\n",item];
                }
                    break;
                    
                default:
                    break;
            }
            
            index++;
        }
        CGFloat itemWidth = (mainWidth - 16 * 2) / 4;
        CGSize s1 = [str1 textSizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(itemWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
        CGSize s2 = [str2 textSizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(itemWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
        CGSize s3 = [str3 textSizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(itemWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
        CGSize s4 = [str4 textSizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(itemWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
        
        CGFloat max = MAX(MAX(s1.height, s2.height), MAX(s3.height, s4.height)) ;
        
        return max + 40;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 0.1;
    if(section == 2)
        return 50;
    return 10;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 2)
    {
        UIView* vvv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 50)];
     
        if(_allCodePeriod)
        {
            UIButton* btnPrev = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 40, 40)];
            [btnPrev setImage:[UIImage imageNamed:@"prev"] forState:UIControlStateNormal];
            [btnPrev addTarget:self action:@selector(btnPrevAction) forControlEvents:UIControlEventTouchUpInside];
            [vvv addSubview:btnPrev];
            
            if(_allCodePeriod)
            {
                AllProPeriod* percode = [_allCodePeriod.Rows objectAtIndex:0];
                if(_codeId == [percode.codeID intValue])
                {
                    btnPrev.hidden = YES;
                }
                else if(_allCodePeriod.Rows.count >1)
                {
                    percode = [_allCodePeriod.Rows objectAtIndex:1];
                    if(_codeId == [percode.codeID intValue])
                    {
                        btnPrev.hidden = YES;
                    }
                }
            }
            
            UIButton* btnNext = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth - 40 - 5, 5, btnPrev.frame.size.width, btnPrev.frame.size.height)];
            [btnNext setImage:[UIImage imageNamed:@"next"] forState:UIControlStateNormal];
            [btnNext addTarget:self action:@selector(btnNextAction) forControlEvents:UIControlEventTouchUpInside];
            [vvv addSubview:btnNext];
            
            if(_allCodePeriod)
            {
                AllProPeriod* percode = [_allCodePeriod.Rows objectAtIndex:_allCodePeriod.Rows.count - 1];
                if(_codeId == [percode.codeID intValue])
                {
                    btnNext.hidden = YES;
                }
            }
        }
        
        NSString* str = [NSString stringWithFormat:@"(%@%d%@)%@：%d",[CommonHelper getLocalizedText:@"no_pre"],[_shopOverResult.data.no intValue],[CommonHelper getLocalizedText:@"no_tail"],[CommonHelper getLocalizedText:@"lucky_number"],[_shopOverResult.data.kaijang_num intValue]];
        
        CGSize s = [str textSizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(MAXFLOAT, 999) lineBreakMode:NSLineBreakByCharWrapping];
        
        
        UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake((mainWidth - s.width) / 2, 18, 300, 15)];
        lbl.text = [NSString stringWithFormat:@"(%@%d%@)%@：",[CommonHelper getLocalizedText:@"no_pre"],[_shopOverResult.data.no intValue],[CommonHelper getLocalizedText:@"no_tail"],[CommonHelper getLocalizedText:@"lucky_number"]];
        lbl.font = [UIFont systemFontOfSize:15];
        lbl.textColor = [UIColor grayColor];
        [vvv addSubview:lbl];
        
        s = [lbl.text textSizeWithFont:lbl.font constrainedToSize:CGSizeMake(MAXFLOAT, 999) lineBreakMode:NSLineBreakByCharWrapping];
        
        UILabel* lblCode = [[UILabel alloc] initWithFrame:CGRectMake(lbl.frame.origin.x + s.width, lbl.frame.origin.y, 100, 15)];
        lblCode.text = [NSString stringWithFormat:@"%d",[_shopOverResult.data.kaijang_num intValue]];
        lblCode.font = [UIFont systemFontOfSize:15];
        lblCode.textColor = mainColor;
        [vvv addSubview:lblCode];
        
        return vvv;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        static NSString *CellIdentifier = @"lotteryTopCell";
        ProductLotteryTopCell *cell =  (ProductLotteryTopCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[ProductLotteryTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
//        [cell setLottery:_detail];
        [cell setCustomLottery:_shopOverResult.data];
        return cell;
    }
    else if(indexPath.section == 1)
    {
        static NSString *CellIdentifier = @"lotterybotCell";
        UITableViewCell *cell =  (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if(indexPath.row == 0)
        {
            cell.textLabel.text = [CommonHelper getLocalizedText:@"section_all_record"];
        }
        else
        {
            cell.textLabel.text = [CommonHelper getLocalizedText:@"section_show_record"];
        }
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.textColor = [UIColor grayColor];
        return cell;
    }
    else if(indexPath.section == 2)
    {
        if (indexPath.row == 0) {
            static NSString *CellIdentifier = @"lotteryCalculateCell";
            UITableViewCell *cell =  (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            //            cell.textLabel.text = [NSString stringWithFormat:@"商品获得者本期总共云购 %d 人次",[_detail.Rows2.codeRUserBuyCount intValue]];
            cell.textLabel.text = [CommonHelper getLocalizedText:@"show_calculate_details"];
            cell.textLabel.font = [UIFont systemFontOfSize:14];
            cell.textLabel.textColor = [UIColor grayColor];
            return cell;
        }
        if(indexPath.row == 1)
        {
            static NSString *CellIdentifier = @"lotteryCTCell";
            UITableViewCell *cell =  (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSString* tail = @"";
            tail = [_shopOverResult.data.count intValue] > 1 ? [CommonHelper getLocalizedText:@"cart_tip_tail"] : [CommonHelper getLocalizedText:@"cart_tip_tail_single"];
//            cell.textLabel.text = [NSString stringWithFormat:@"商品获得者本期总共云购 %d 人次",[_detail.Rows2.codeRUserBuyCount intValue]];
            cell.textLabel.text = [NSString stringWithFormat:@"%@ %d %@",[CommonHelper getLocalizedText:@"winner_buy_total"],[_shopOverResult.data.count intValue],tail];//test niewen
            cell.textLabel.font = [UIFont systemFontOfSize:14];
            cell.textLabel.textColor = [UIColor grayColor];
            return cell;
        }
        else
        {
            static NSString *CellIdentifier = @"lotteryCodeCell";
            ProductLotteryCodeCell *cell =  (ProductLotteryCodeCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[ProductLotteryCodeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
//            [cell setCodes:[_detail.Rows4 objectAtIndex:indexPath.row - 1]];//test niewen
            
            ProductCodeBuy* testBuy = [[ProductCodeBuy alloc]init];
//            testBuy.buyTime = @"2015-06-01" _shopOverResult.data.create_time;
            testBuy.rnoNum = @"001,002";
            
//            [cell setCodes:testBuy];
            [cell setCustomCodes:testBuy AndData:_shopOverResult.data];
            return cell;
        }
    }
    static NSString *CellIdentifier = @"productTopCell";
    UITableViewCell *cell =  (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1)
    {
        if(indexPath.row == 0)
        {
            ProductBuyListVC* vc = [[ProductBuyListVC alloc] initWithCodeId:_codeId];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            ShowOrderListVC* vc = [[ShowOrderListVC alloc] initWithGoodsId:_goodsId];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    else if (indexPath.section == 2)
    {
        if(indexPath.row == 0)
        {
            CalculateListVC* vc = [[CalculateListVC alloc]initWithCodeId:_codeId];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}

- (void)btnNextAction
{
    if(_allCodePeriod)
    {
        int cur = 0;
        for (int i = 0;i<_allCodePeriod.Rows.count;i++) {
            AllProPeriod* period = [_allCodePeriod.Rows objectAtIndex:i];
            if([period.codeID intValue] == _codeId)
            {
                cur = i;
                break;
            }
        }
        if(cur+1 >= _allCodePeriod.Rows.count)
            return;
        int nextCode = [[[_allCodePeriod.Rows objectAtIndex:cur+1] codeID] intValue];
        if(nextCode > 0)
        {
            [[XBToastManager ShardInstance] showprogress];
            _codeId = nextCode;
            [self getData];
        }
    }
}

- (void)btnPrevAction
{
    if(_allCodePeriod)
    {
        int cur = 0;
        for (int i = 0;i<_allCodePeriod.Rows.count;i++) {
            AllProPeriod* period = [_allCodePeriod.Rows objectAtIndex:i];
            if([period.codeID intValue] == _codeId)
            {
                cur = i;
                break;
            }
        }
        if(cur<=1)
        {
            return;
        }
        else
        {
            int nextCode = [[[_allCodePeriod.Rows objectAtIndex:cur-1] codeID] intValue];
            if(nextCode > 0)
            {
                [[XBToastManager ShardInstance] showprogress];
                _codeId = nextCode;
                [self getData];
            }
        }
    }
}

#pragma mark - delegate
- (void)gotoCartAction
{
    self.tabBarController.selectedIndex = 3;
    [self.navigationController performSelector:@selector(popToRootViewControllerAnimated:) withObject:[NSNumber numberWithBool:NO] afterDelay:0.2];
}

- (void)gotoDetailAction
{
    ProductDetailVC* vc = [[ProductDetailVC alloc] initWithGoodsId:_goodsId codeId:_codeId];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
