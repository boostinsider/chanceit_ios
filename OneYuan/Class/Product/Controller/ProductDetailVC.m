//
//  ProductDetailVC.m
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/27.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import "ProductDetailVC.h"
#import "ProductModel.h"
#import "ProductDetailTopCell.h"
#import "ProductDetailGetCell.h"
#import "AllProModel.h"
#import "ShowOrderListVC.h"
#import "ProductBuyListVC.h"
#import "ProductDetailOptView.h"
#import "CartModel.h"
#import "CartInstance.h"
#import "ProductLotteryVC.h"
#import "HomeModel.h"
#import "ProductImageDetailsVC.h"
#import <Social/Social.h>
#import "CalculateFunVC.h"


@interface ProductDetailVC ()<UITableViewDataSource,UITableViewDelegate,ProductDetailOptViewDelegate>
{
    int    _goodsId;
    int    _codeId;
    
    
    __block UITableView     *tbView;
    __block UIWebView       *webView;
    __block ProductDetail   *proDetail;
    
    UIImageView     *imgPro;
    __block ProductDetailOptView* optView;
    
    __block ShopOverResult          *_shopOverResult;
    __block ShopOverResult          *_LatestShopOverResult;
    __block ProductDetailResult     *_productDetailResult;
}
@end

@implementation ProductDetailVC
@synthesize hasSetPeriodNum,productStatus;

- (id)initWithGoodsId:(int)goodsId codeId:(int)codeId
{
    self = [super init];
    if(self)
    {
        _goodsId = goodsId;
        _codeId = codeId;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    self.title = [CommonHelper getLocalizedText:@"product_details_nav_title"];
    self.navigationController.navigationBar.tintColor = COLOR_WHITE;
    __weak typeof (self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"btnback" htlImage:@"btnback" title:@"" action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    CGFloat margin = 0;
    if(![OyTool ShardInstance].bIsForReview)
    {
        margin = 60;
        optView = [[ProductDetailOptView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 107 - 16, mainWidth, 60)];
        optView.delegate = self;
        [self.view addSubview:optView];
    }
    
    hasSetPeriodNum = NO;
    productStatus = 2;
    
    tbView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, self.view.bounds.size.height - margin ) style:UITableViewStyleGrouped];
    tbView.delegate = self;
    tbView.dataSource = self;
    tbView.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    tbView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tbView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:tbView];
    
    //webView
    webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, mainWidth, self.view.bounds.size.height - margin - 64)];
//    webView.navigationDelegate = self;
    [self.view addSubview:webView];
    [webView setScalesPageToFit:YES];
    webView.hidden = YES;
    
    [tbView addPullToRefreshWithActionHandler:^{
        [wSelf getData];
    }];
    
    if(_goodsId)
    {
        [self getData];
    }
    else
    {
        [self getGoodsId];
    }
    
    [tbView.pullToRefreshView setOYStyle];
    [self showLoad];
    
    UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithTitle:@"share" style:UIBarButtonItemStyleDone target:self action:@selector(clickShare)];
    self.navigationItem.rightBarButtonItem = shareItem;
    
}



- (id)activityViewController:(UIActivityViewController *)activityViewController
         itemForActivityType:(NSString *)activityType
{
    if ([activityType isEqualToString:UIActivityTypePostToFacebook]) {
        return _shortUrlFacebook;
    } else if ([activityType isEqualToString:UIActivityTypePostToTwitter]) {
       return _shortUrlTwtter;
    } else {
        return _shortUrlMail;
    }
}

- (void) clickShare
{
    NSArray *array = @[@"facebook",@"twitter",@"mail"];
    __block int cnt = 0;
    
    for (int i = 0; i < array.count; i ++)
    {
        DeeplinkObj *obj = [DeeplinkObj new];
        NSDictionary *payload = @{@"goods_id":@(_goodsId),
                                  @"code_id":@(_codeId),
                                  @"view_id":@"ProductDetailVC"};
        obj.payload = payload;
        obj.title = @"one pay";
        obj.contentDescription = @"one pay  chance it !";
        obj.channel = [array objectAtIndex:i];
        obj.image = [NSString stringWithFormat:@"%@%@",oyImageBaseUrl,_productDetailResult.data.pic];
        
        [Boostinsider createDeeplink:obj success:^(NSString *link) {
            if ([obj.channel isEqualToString:@"facebook"])
            {
                _shortUrlFacebook = link;
            }
            else if ([obj.channel isEqualToString:@"twitter"])
            {
                _shortUrlTwtter = link;
            }
            else
            {
                _shortUrlMail = link;
            }
            cnt ++;
            if (cnt == array.count)
            {
                UIActivityViewController *avc = [[UIActivityViewController alloc] initWithActivityItems:@[self,@""] applicationActivities:nil];
                
                //不显示哪些分享平台
                avc.excludedActivityTypes = @[UIActivityTypeAirDrop,UIActivityTypeCopyToPasteboard,UIActivityTypeAddToReadingList,@"com.tencent.xin.sharetimeline",@"pinterest.ShareExtension"];
                
                
                [self presentViewController:avc animated:YES completion:nil];
                //分享结果回调方法
                UIActivityViewControllerCompletionHandler myblock = ^(NSString *type,BOOL completed){
                    NSLog(@"%d %@",completed,type);
                };
                avc.completionHandler = myblock;
            }
            
        } failure:^{
            
        }];
    }
}

- (void)getData //goodsId 不为0，获取商品详情
{
    __weak typeof (self) wSelf = self;
    
    NSDictionary* dic = @{@"pid":@(_goodsId)};
    [ServerHelper getProductDetails:dic Completion:^(NSDictionary *result) {
        NSLog(@"getAnnouncedProductsWithParameters succeed!");
//        [tbView.pullToRefreshView stopAnimating];
//        [tbView.infiniteScrollingView stopAnimating];
//        [wSelf hideLoad];
        if ([[result objectForKey:@"status"] intValue] == 200 && [result objectForKey:@"data"]) {
            _productDetailResult = [[ProductDetailResult alloc]initWithDictionary:result];
            webView.hidden = YES;
        }
        else if ([[result objectForKey:@"status"] intValue] == 401)
        {
            //go to login page
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }

    } failure:^{
        NSLog(@"getAnnouncedProductsWithParameters failed!");
    }];
    
    
    NSDictionary* dic2 = @{@"sid":@(_goodsId)};
    
    [ServerHelper getLatestShopPeriodInfo:dic2 Completion:^(id result) {
        int latestPid = [[result objectForKey:@"id"] intValue];
        
        if (latestPid == _codeId) {
            [tbView.pullToRefreshView stopAnimating];
            [tbView.infiniteScrollingView stopAnimating];
            [wSelf hideLoad];
            
            _LatestShopOverResult = [[ShopOverResult alloc]init ];
            _LatestShopOverResult = _shopOverResult;
            
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            self.periodNum = [f numberFromString:_LatestShopOverResult.data.no];
            productStatus = [[f numberFromString:_LatestShopOverResult.data.state]intValue];
            hasSetPeriodNum = YES;
            [tbView reloadData];
            return;
        }
         NSDictionary* dic3 = @{@"pid":@(latestPid)};
        [ServerHelper getShopOverWithParameters:dic3 Completion:^(NSDictionary *result) {
            NSLog(@"get latest period succeed1!");
            
            [tbView.pullToRefreshView stopAnimating];
            [tbView.infiniteScrollingView stopAnimating];
            [wSelf hideLoad];
            
            _LatestShopOverResult = [[ShopOverResult alloc]initWithDictionary:result];
            _shopOverResult = _LatestShopOverResult;
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            self.periodNum = [f numberFromString:_LatestShopOverResult.data.no];
            productStatus = [[f numberFromString:_LatestShopOverResult.data.state]intValue];
            hasSetPeriodNum = YES;
            
            [tbView reloadData];
            
        } failure:^{
             NSLog(@"get latest period failed!2");
        }];
        NSLog(@"get latest period succeed!");
    } failure:^{
        NSLog(@"get latest period failed!");
    }];
}

- (void)getGoodsId //goodsId 为0，获取该期正在开奖的详情
{
//    __weak typeof (self) wSelf = self;
    NSDictionary* dic = @{@"pid":@(_codeId)};
    [ServerHelper getAnnouncedDetails:dic Completion:^(NSDictionary *products) {
        NSLog(@"getAnnouncedDetails succeed!");
//        [tbView.pullToRefreshView stopAnimating];
//        [tbView.infiniteScrollingView stopAnimating];
//        [wSelf hideLoad];
    } failure:^{
        NSLog(@"getAnnouncedDetails failed!");
    }];
    
    [ServerHelper getShopOverWithParameters:dic Completion:^(NSDictionary *result) {
        NSLog(@"getAnnouncedProductsWithParameters succeed!");
//        [tbView.pullToRefreshView stopAnimating];
//        [tbView.infiniteScrollingView stopAnimating];
        
        _shopOverResult = [[ShopOverResult alloc]initWithDictionary:result];
        if ([_shopOverResult.status intValue] == 200) {
             _goodsId = [_shopOverResult.data.sid intValue];
            [self getData];
//             [tbView reloadData];
        }
        else if ([_shopOverResult.status intValue] == 401)
        {
            //go to login page
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
       
        
    } failure:^{
        NSLog(@"getAnnouncedProductsWithParameters failed!");
        //[[XBToastManager ShardInstance] showtoast:[NSString stringWithFormat:@"获取最新揭晓页面数据异常:%@",error]];
    }];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    return !proDetail ? 0 : (proDetail.Rows2.userName ? 3 : 2);
    
    return !_shopOverResult ? 0 : (_shopOverResult.data.user.name ? 5: 4);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 3)
    {
//        int row = proDetail.Rows2.codePeriod.intValue > 1 ? 3 : 2;
        int row = _shopOverResult.data.cid.intValue > 1 ? 3 : 2; // test niewen
        return row;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
        return 180;
    
    if (indexPath.section == 1) {
        UITextView *tmp = [[UITextView alloc] init];
        tmp.font = [UIFont systemFontOfSize:16];;
        [tmp setText:[CommonHelper getLocalizedText:@"statement_no_relationship_with_app_inc"]];
        
        CGSize sz = [tmp sizeThatFits:CGSizeMake(self.view.frame.size.width - 14 * 2, CGFLOAT_MAX)];
        
        return sz.height + 2 * 4;
    }
    if(indexPath.section == 4)
        return 100;
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 0.1;
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        static NSString *CellIdentifier = @"productTopCell";
        ProductDetailTopCell *cell =  (ProductDetailTopCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[ProductDetailTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        UIImageView* img = nil;
//        [cell setProDetail:proDetail img:&img];
        [cell setCustomProDetail:_shopOverResult.data img:&img];
        imgPro = img;
        return cell;
    }
    else if(indexPath.section == 1)
    {
        static NSString *CellIdentifier = @"productStatementCell";
        UITableViewCell *cell =  (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        
        UITextView *tmp = [[UITextView alloc] init];
        tmp.font = [UIFont systemFontOfSize:16];
        [tmp setText:[CommonHelper getLocalizedText:@"statement_no_relationship_with_app_inc"]];
        CGSize sz = [tmp sizeThatFits:CGSizeMake(self.view.frame.size.width - 14 * 2, CGFLOAT_MAX)];
        
        UITextView* statement = [[UITextView alloc]initWithFrame:CGRectMake(14, 4, self.view.frame.size.width - 14 * 2, sz.height)];
        statement.font = [UIFont systemFontOfSize:16];
        [cell.contentView addSubview:statement];
        statement.editable = NO;
        statement.userInteractionEnabled = NO;
        statement.textColor = COLOR_RED;
        statement.text = [CommonHelper getLocalizedText:@"statement_no_relationship_with_app_inc"];
        
        return cell;
        
    }
    else if (indexPath.section == 2) {
        static NSString *CellIdentifier = @"lotteryCalculateCell";
        UITableViewCell *cell =  (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        //            cell.textLabel.text = [NSString stringWithFormat:@"商品获得者本期总共云购 %d 人次",[_detail.Rows2.codeRUserBuyCount intValue]];
        cell.textLabel.text = [CommonHelper getLocalizedText:@"show_calculate_func"];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.textLabel.textColor = [UIColor grayColor];
        return cell;
    }
    else if(indexPath.section == 3)
    {
        static NSString *CellIdentifier = @"proCommonCell";
        UITableViewCell *cell =  (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.textColor = [UIColor grayColor];
        if(indexPath.row == 0)
        {
            cell.textLabel.text = [CommonHelper getLocalizedText:@"section_all_record"];
        }
        else if (indexPath.row == 1)
        {
//            cell.textLabel.text = [NSString stringWithFormat:@"商品晒单(%d)",[proDetail.Rows3.Topic intValue]];
            //test niewen
            
//            cell.textLabel.text = [NSString stringWithFormat:@"%@(%d)",[CommonHelper getLocalizedText:@"section_show_record"],[_shopOverResult.data.shared intValue]];
            cell.textLabel.text = [NSString stringWithFormat:@"%@",[CommonHelper getLocalizedText:@"section_show_record"]];
        }
        else if(indexPath.row == 2)
        {
            cell.textLabel.text = [CommonHelper getLocalizedText:@"image_details"];
        }
        return cell;
    }
    else if (indexPath.section == 4)
    {
        static NSString *CellIdentifier = @"productGetCell";
        ProductDetailGetCell *cell =  (ProductDetailGetCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[ProductDetailGetCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
//        [cell setProDetail:proDetail];
        [cell setCustomProDetail:_shopOverResult.data];
        return cell;
    }
    static NSString *CellIdentifier = @"productCommoneCell";
    UITableViewCell *cell =  (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 3)
    {
        if(indexPath.row == 0)
        {
            ProductBuyListVC* vc = [[ProductBuyListVC alloc] initWithCodeId:[ _shopOverResult.data.pid intValue]];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if(indexPath.row ==1)
        {
            ShowOrderListVC* vc = [[ShowOrderListVC alloc] initWithGoodsId:_goodsId];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if(indexPath.row == 2)
        {
//            ProductLotteryVC* vc = [[ProductLotteryVC alloc] initWithGoods:_goodsId codeId:[proDetail.Rows2.codeID intValue] - 1];
//            vc.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:vc animated:YES];
            if (_goodsId == 0 || _codeId == 0 || hasSetPeriodNum == NO) {
                return;
            }
            ProductImageDetailsVC* vc = [[ProductImageDetailsVC alloc]initWithGoodsId:_goodsId codeId:_codeId];
            vc.periodNum = self.periodNum;
             vc.hidesBottomBarWhenPushed = YES;
             [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
    if (indexPath.section == 2)
    {
        CalculateFunVC* vc = [[CalculateFunVC alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

#pragma mark - delegate
- (void)addToCartAction
{
//    CartItem* item = [[CartItem alloc] init];
//    item.cPid = [NSNumber numberWithInt:_goodsId];
//    item.cName = proDetail.Rows2.goodsName;
//    item.cPeriod = proDetail.Rows2.codePeriod;
//    item.cBuyNum = [NSNumber numberWithInt:1];
//    item.cCid = proDetail.Rows2.codeID;
//    item.cPrice = proDetail.Rows2.codePrice;
//    item.cSrc = [NSString stringWithFormat:@"%@%@",oyImageBaseUrl,[[proDetail.Rows1 objectAtIndex:0] picName]];
//    [[CartInstance ShartInstance] addToCart:item imgPro:imgPro type:addCartType_Opt];
    if (_productDetailResult == nil || _LatestShopOverResult == nil || _goodsId == 0) {
        return;
    }
    CartItem* item = [[CartItem alloc] init];
    item.cPid = [NSNumber numberWithInt:_goodsId];
    item.cName = _productDetailResult.data.name;
    item.cPeriod = self.periodNum;//期号
    item.cBuyNum = [NSNumber numberWithInt:1];
    item.cCid = _LatestShopOverResult.data.pid;
    item.cPrice =_productDetailResult.data.price;
    item.cSrc = _productDetailResult.data.pic;
    [[CartInstance ShartInstance] addToCart:item imgPro:imgPro type:addCartType_Opt];
    
    [self performSelector:@selector(setCartNum) withObject:nil afterDelay:1];
}

- (void)addGotoCartAction
{
    //    CartItem* item = [[CartItem alloc] init];
    //    item.cPid = [NSNumber numberWithInt:_goodsId];
    //    item.cName = proDetail.Rows2.goodsName;
    //    item.cPeriod = proDetail.Rows2.codePeriod;
    //    item.cBuyNum = [NSNumber numberWithInt:1];
    //    item.cCid = proDetail.Rows2.codeID;
    //    item.cPrice = proDetail.Rows2.codePrice;
    //    item.cSrc = [NSString stringWithFormat:@"%@%@",oyImageBaseUrl,[[proDetail.Rows1 objectAtIndex:0] picName]];
    //    [[CartInstance ShartInstance] addToCart:item imgPro:imgPro type:addCartType_Opt];
    //
    //    [self performSelector:@selector(gotoCartAction) withObject:nil afterDelay:1];
    
    if (_productDetailResult == nil || _LatestShopOverResult == nil || _goodsId == 0) {
        return;
    }
    CartItem* item = [[CartItem alloc] init];
    item.cPid = [NSNumber numberWithInt:_goodsId];
    item.cName = _productDetailResult.data.name;
    item.cPeriod = self.periodNum;//期号
    item.cBuyNum = [NSNumber numberWithInt:1];
    item.cCid = _LatestShopOverResult.data.pid;
    item.cPrice =_productDetailResult.data.price;
    item.cSrc = _productDetailResult.data.pic;
    [[CartInstance ShartInstance] addToCart:item imgPro:imgPro type:addCartType_Opt];
    
    [self performSelector:@selector(gotoCartAction) withObject:nil afterDelay:1];
    
}

- (void)setCartNum
{
    if (hasSetPeriodNum) {
        [CartModel quertCart:nil value:nil block:^(NSArray* result){
            [optView setCartNum:(int)result.count];
        }];
    }
    else
    {
        [CommonHelper showMessage:[CommonHelper getLocalizedText:@"no_period_available"] Completion:^{
            ;
        }];
    }
    
}

- (void)gotoCartAction
{
    if (hasSetPeriodNum) {
        self.tabBarController.selectedIndex = 3;
        [self.navigationController performSelector:@selector(popToRootViewControllerAnimated:) withObject:[NSNumber numberWithBool:NO] afterDelay:0.2];
    }
    else
    {
        [CommonHelper showMessage:[CommonHelper getLocalizedText:@"no_period_available"] Completion:^{
            ;
        }];
    }
}

@end
