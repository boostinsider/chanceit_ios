//
//  HomeModel.h
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/19.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol  HomeAd <NSObject>
@end
@interface HomeAd : OneBaseParser
@property (nonatomic,copy)NSNumber* type;
@property (nonatomic,copy)NSString* src;
@property (nonatomic,copy)NSString* url;
@property (nonatomic,copy)NSString* alt;
@property (nonatomic,copy)NSNumber* width;
@property (nonatomic,copy)NSNumber* height;
@end

@interface HomeAdList : OneBaseParser
@property (nonatomic,copy)NSArray* Rows;
@end

@protocol  HomeNewing <NSObject>
@end
@interface HomeNewing : OneBaseParser
@property (nonatomic,copy)NSString* goodsPic;
@property (nonatomic,copy)NSString* goodsSName;
@property (nonatomic,copy)NSString* seconds;
@property (nonatomic,copy)NSNumber* codeID;
@property (nonatomic,copy)NSString* price;
@property (nonatomic,copy)NSNumber* period;
@property (nonatomic,copy)NSNumber* codeQuantity;
@property (nonatomic,copy)NSNumber* codeSales;
@end

@interface HomeNewingList : OneBaseParser
@property (nonatomic,copy)NSNumber* errorCode;
@property (nonatomic,copy)NSNumber* maxSeconds;
@property (nonatomic,copy)NSArray*  listItems;
@end

@protocol  HomeNewed <NSObject>
@end
@interface HomeNewed : OneBaseParser
@property (nonatomic,copy)NSNumber* codeGoodsID;
@property (nonatomic,copy)NSString* codeGoodsPic;
@property (nonatomic,copy)NSString* codeGoodsSName;
@property (nonatomic,copy)NSNumber* codeID;
@property (nonatomic,copy)NSNumber* codePeriod;
@property (nonatomic,copy)NSNumber* codeState;
@property (nonatomic,copy)NSString* userName;
@property (nonatomic,copy)NSString* userWeb;
@end

@protocol  HomeHostest <NSObject>
@end
@interface HomeHostest : OneBaseParser
@property (nonatomic,copy)NSNumber* codePrice;
@property (nonatomic,copy)NSNumber* codeQuantity;
@property (nonatomic,copy)NSNumber* codeSales;
@property (nonatomic,copy)NSNumber* goodsID;
@property (nonatomic,copy)NSString* goodsPic;
@property (nonatomic,copy)NSString* goodsSName;
@end

@protocol  HomeShowOrder <NSObject>
@end
@interface HomeShowOrder : OneBaseParser
@property (nonatomic,copy)NSNumber* postID;
@property (nonatomic,copy)NSString* postImg;
@property (nonatomic,copy)NSString* postTime;
@property (nonatomic,copy)NSString* postTitle;
@end

@interface HomePageList : OneBaseParser
@property (nonatomic,copy)NSArray* Rows1;
@property (nonatomic,copy)NSArray* Rows2;
@property (nonatomic,copy)NSArray* Rows3;
@end

@protocol  HomeSearchAd <NSObject>
@end
@interface HomeSearchAd : OneBaseParser
@property (nonatomic,copy)NSNumber* type;
@property (nonatomic,copy)NSString* src;
@property (nonatomic,copy)NSString* url;
@property (nonatomic,copy)NSString* alt;
@property (nonatomic,copy)NSNumber* width;
@property (nonatomic,copy)NSNumber* height;
@end

@interface HomeSearchAdList : OneBaseParser
@property (nonatomic,copy)NSArray* Rows;
@end

@protocol  HomeOrderShowItem <NSObject>
@end
@interface HomeOrderShowItem : OneBaseParser
@property (nonatomic,copy)NSNumber* postID;
@property (nonatomic,copy)NSString* postAllPic;
@property (nonatomic, strong)NSArray *pics;
@property (nonatomic,copy)NSString* postTitle;
@property (nonatomic,copy)NSString* postContent;
@property (nonatomic,copy)NSString* userPhoto;
@property (nonatomic,copy)NSString* userName;
@property (nonatomic,copy)NSString* userWeb;
@property (nonatomic,copy)NSString* postTime;
@property (nonatomic,copy)NSNumber* postHits;
@property (nonatomic,copy)NSNumber* postReplyCount;
@end

@interface HomeOrderShowList : OneBaseParser
@property (nonatomic,copy)NSNumber* count;
@property (nonatomic,copy)NSArray*  listItems;
@end

#pragma custom start

@interface ProductItem : OneBaseParser
@property (nonatomic, copy) NSNumber* id;
@property (nonatomic, copy) NSNumber* jd;
@property (nonatomic, copy) NSString* moreurl;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSNumber* no;
@property (nonatomic, copy) NSString* number;
@property (nonatomic, copy) NSString* pic;
@property (nonatomic, copy) NSNumber* pid;
@property (nonatomic, copy) NSNumber* position;
@property (nonatomic, copy) NSString* price;
@property (nonatomic, copy) NSString* sid;
@property (nonatomic, copy) NSNumber* surplus;
@property (nonatomic, copy) NSString* url;
@end



@interface ProductList : OneBaseParser
@property (nonatomic, copy) NSNumber *status;
@property (nonatomic, retain) NSArray *data;
@end


//processing

@interface ProcessingListUser : OneBaseParser
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSNumber *count;
@property (nonatomic, copy) NSNumber *id;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *user_url;
@end

@interface ProcessingItem : OneBaseParser
@property (nonatomic, copy) NSNumber* id;
@property (nonatomic, copy) NSNumber* sid;
@property (nonatomic, copy) NSNumber* no;
@property (nonatomic, copy) NSNumber* create_time;
@property (nonatomic, copy) NSString* ctitle;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* description;
@property (nonatomic, copy) NSString* number;
@property (nonatomic, copy) NSNumber* edit_price;
@property (nonatomic, copy) NSString* state;
@property (nonatomic, copy) NSString* moreurl;
@property (nonatomic, copy) NSNumber* position;
@property (nonatomic, copy) NSString* price;
@property (nonatomic, copy) NSString* pic;
@property (nonatomic, copy) NSNumber* buy_price;
@property (nonatomic, copy) NSString* url;
@property (nonatomic, copy) NSString* buy_url;
@property (nonatomic, copy) NSString* category;

@property (nonatomic, copy) NSString* cid;
@property (nonatomic, copy) NSString* pid;
@property (nonatomic, copy) NSString* count;

@property (nonatomic, copy) NSString* end_time;

@property (nonatomic, copy) NSString* hits;
@property (nonatomic, copy) NSString* jd;
@property (nonatomic, retain) ProcessingListUser* user;
@property (nonatomic, copy) NSString* kaijang_diffe;
@property (nonatomic, copy) NSString* kaijang_num;
@property (nonatomic, copy) NSString* kaijang_time;
@property (nonatomic, copy) NSString* kaijang_timing;

@property (nonatomic, copy) NSString* kaijiang_count;
@property (nonatomic, copy) NSString* kaijiang_ssc;

@property (nonatomic, copy) NSString* keywords;
@property (nonatomic, copy) NSString* meta_title;
@end

@interface ProcessingList : OneBaseParser
@property (nonatomic, copy) NSNumber *status;
@property (nonatomic, retain) NSArray *data;
@end

#pragma custom end

@interface HomeModel : UITableViewCell
+ (void)getAds:(void(^)(AFHTTPRequestOperation* operation, NSObject* result))success failure:(void(^)(NSError* error))failure;
+ (void)getNewing:(void(^)(AFHTTPRequestOperation* operation, NSObject* result))success failure:(void(^)(NSError* error))failure;
+ (void)getHomePage:(void(^)(AFHTTPRequestOperation* operation, NSObject* result))success failure:(void(^)(NSError* error))failure;
+ (void)getSearchAd1:(void(^)(AFHTTPRequestOperation* operation, NSObject* result))success failure:(void(^)(NSError* error))failure;
+ (void)getSearchAd2:(void(^)(AFHTTPRequestOperation* operation, NSObject* result))success failure:(void(^)(NSError* error))failure;
+ (void)getOrderShow:(void(^)(AFHTTPRequestOperation* operation, NSObject* result))success failure:(void(^)(NSError* error))failure;
@end
