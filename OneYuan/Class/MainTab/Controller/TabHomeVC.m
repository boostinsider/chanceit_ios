//
//  TabHomeVC.m
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/18.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import "TabHomeVC.h"
#import "HomeModel.h"
#import "HomeAdCell.h"
#import "HomeAdDigitalCell.h"
#import "HomeAdBtnCell.h"
#import "HomeNewCell.h"
#import "HomeHotCell.h"
#import "HomeOrderShowCell.h"
#import "HomeInstance.h"
#import "SearchVC.h"
#import "MineBuylistVC.h"
#import "UserInstance.h"
#import "LoginVC.h"
#import "TabNewestVC.h"
#import "ProductDetailVC.h"
#import "ProductLotteryVC.h"
#import "ShowOrderListVC.h"
#import "ShowOrderDetailVC.h"
#import "ServerHelper.h"
#import "BookingDetailViewController.h"
#import "User.h"
@interface TabHomeVC ()<UITableViewDataSource,UITableViewDelegate,HomeAdCellDelegate,HomeAdBtnCellDelegate,HomeAdDigitalCellDelegate,HomeHotCellDelegate,HomeNewCellDelegate,HomeOrderShowCellDelegate>
{
    NSTimer         *timer;
    UITableView     *tbView;
    __block HomePageList    *listHomepage;
    __block ProductList    *productsData;
    __block ProcessingList *processingData;
}
@end

@implementation TabHomeVC

- (void)dealloc
{
    if(timer)
    {
        [timer invalidate];
        timer = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMyData) name:kDidNotifyFromBack object:nil];
    
    self.title = [CommonHelper getLocalizedText:@"home_page_nav_title"];
    __weak typeof (self) wSelf = self;
    
    if(![OyTool ShardInstance].bIsForReview)
    {
        [self actionCustomRightBtnWithNrlImage:@"search" htlImage:@"search" title:@"" action:^{
            SearchVC* vc = [[SearchVC alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [wSelf.navigationController pushViewController:vc animated:YES];
        }];
    }
    
    tbView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tbView.delegate = self;
    tbView.dataSource = self;
    tbView.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    tbView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [tbView setSeparatorInset:UIEdgeInsetsZero];
    [tbView setLayoutMargins:UIEdgeInsetsZero];
    tbView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:tbView];
    
    [tbView addPullToRefreshWithActionHandler:^{
        [wSelf getNewest];
        [wSelf getOrderShows];
    }];
 
    [tbView.pullToRefreshView setOYStyle];
    
    [tbView triggerPullToRefresh];
    
//    [self getLoadAds];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(refreshMyData) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
    
    [self performSelector:@selector(autoLogin) withObject:nil afterDelay:1.0];
    
//    [self autoLogin];
    
    // add login notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(needLogin) name:kNeedLogin object:nil];
//
    
//    //test register
//    NSDictionary* params = @{@"username":@"2@123.com",
//                             @"password":@"123456",
//                             @"repassword":@"123456"};
//    
//    [ServerHelper signUpWithParameters:params Completion:^(NSDictionary *user) {
//        NSLog(@"sign up succeed");
//        [[XBToastManager ShardInstance] hideprogress];
////        [wSelf doLogin];
//    } failure:^{
//        NSLog(@"sign up failed");
//        [[XBToastManager ShardInstance] hideprogress];
//    }];
    
    
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showDetailView:) name:@"show_deep_view" object:nil];
    
}

- (void) showDetailView: (NSNotification *)notification
{
    NSDictionary *dic = [notification object];
    NSString *viewId = [dic objectForKey:@"view_id"];
    int goodsId = [[dic objectForKey:@"goods_id"] intValue];
    int codeId = [[dic objectForKey:@"code_id"] intValue];
    if ([viewId isEqualToString:@"ProductDetailVC"])
    {
        ProductDetailVC *vc = [[ProductDetailVC alloc] initWithGoodsId:0 codeId:codeId];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma need login
-(void)needLogin
{
    LoginVC* vc = [[LoginVC alloc] init];
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

#pragma auto login
-(void)autoLogin
{
//    return;
    NSString* username = [[NSUserDefaults standardUserDefaults] objectForKey:kLoginUsername];
    NSString* pwd = [[NSUserDefaults standardUserDefaults] objectForKey:kLoginPwd];
    
    if (username && pwd) {
        __weak typeof (self) wSelf = self;
        
        NSDictionary *param = @{@"username":username,@"password":pwd};
        
        JGProgressHUD *progress = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
        progress.textLabel.text = @"auto login";
        [progress showInView:self.view];
        
        [ServerHelper loginWithParameters:param Completion:^(NSDictionary *result) {
            [progress dismissAnimated:YES];
            NSDictionary* dic = (NSDictionary*)result;
            if ([[dic objectForKey:@"status"]intValue] == 200 && [dic objectForKey:@"user"] != NULL) {
                [[NSUserDefaults standardUserDefaults] setObject:username forKey:kLoginUsername];
                [[NSUserDefaults standardUserDefaults] setObject:pwd forKey:kLoginPwd];
                [[User getInstance] setUserInfo:[dic objectForKey:@"user"]];
                [[XBToastManager ShardInstance] hideprogress];
                [[NSNotificationCenter defaultCenter] postNotificationName:kDidLoginOk object:nil];
//                [wSelf setCartNum];
            }
            else
            {
//                [[XBToastManager ShardInstance] showtoast:[NSString stringWithFormat:@"登录失败:%@",[dic objectForKey :@"message"]]];
                NSString* msg = [CommonHelper getLocalizedText:@"login_failed"];
                [[XBToastManager ShardInstance] showtoast:msg];

            }
        } failure:^{
            [progress dismissAnimated:YES];

//            [[XBToastManager ShardInstance] showtoast:[NSString stringWithFormat:@"登录失败"]];
            NSString* msg = [CommonHelper getLocalizedText:@"login_failed"];
            [[XBToastManager ShardInstance] showtoast:msg];

        }];
    }
}

#pragma mark - load data
/**
 *  刷新数据
 */
- (void)refreshMyData
{
    [self getNewest];
    [self getOrderShows];
}

/**
 *  最新揭晓 here NIWEN获取产品
 */
- (void)getNewest
{
    __weak UITableView* wTb = tbView;
//    [HomeModel getNewing:^(AFHTTPRequestOperation* operation, NSObject* result){
//         [HomeInstance ShardInstnce].listNewing = [[HomeNewingList alloc] initWithDictionary:(NSDictionary*)result];
//        [HomeModel getHomePage:^(AFHTTPRequestOperation* operation, NSObject* result){
//            listHomepage = [[HomePageList alloc] initWithDictionary:(NSDictionary*)result];
//            [wTb.pullToRefreshView stopAnimating];
//            [wTb reloadData];
//            [[NSNotificationCenter defaultCenter] postNotificationName:kDidNotifyReloadNewest object:nil];
//        } failure:^(NSError* error){
//            [wTb.pullToRefreshView stopAnimating];
//                //[[XBToastManager ShardInstance] showtoast:[NSString stringWithFormat:@"获取首页商品异常：%@",error]];
//        }];
//    } failure:^(NSError* error){
//        [wTb.pullToRefreshView stopAnimating];
//        //[[XBToastManager ShardInstance] showtoast:[NSString stringWithFormat:@"获取最新揭晓异常：%@",error]];
//    }];
    
    
    [ServerHelper getAnnouncedProductsWithParameters:nil Completion:^(NSDictionary *products) {
         NSLog(@"getAnnouncedProductsWithParameters succeed!");
        //set new
        
        processingData = [[ProcessingList alloc]initWithDictionary:products];
        [wTb.pullToRefreshView stopAnimating];
        [wTb reloadData];
         [[NSNotificationCenter defaultCenter] postNotificationName:kDidNotifyReloadNewest object:nil];//为了给 最新揭晓的页面通知
    } failure:^{
        NSLog(@"getAnnouncedProductsWithParameters failed!");
        [wTb.pullToRefreshView stopAnimating];
    }];
    
    
    NSDictionary *dic = @{@"page":@(1),@"page_size": @(6),
                          @"order":@"shop.hits|desc"};
    [ServerHelper getProductsWithParameters:dic Completion:^(NSDictionary *products) {
        NSLog(@"getProductsWithParameters succeed!");
        productsData = [[ProductList alloc]initWithDictionary:products];
        [wTb.pullToRefreshView stopAnimating];
        [wTb reloadData];
//        [[NSNotificationCenter defaultCenter] postNotificationName:kDidNotifyReloadNewest object:nil];
    } failure:^{
        NSLog(@"getProductsWithParameters failed!");
        [wTb.pullToRefreshView stopAnimating];
    }];
}

/**
 *  1元搜索广告
 */
- (void)getLoadAds
{
    __weak UITableView* wTb = tbView;
    [HomeModel getSearchAd1:^(AFHTTPRequestOperation* operation, NSObject* result){
        [HomeInstance ShardInstnce].listAd1 = [[HomeSearchAdList alloc] initWithDictionary:(NSDictionary*)result];
        [wTb reloadData];
    } failure:^(NSError* error){
        //[[XBToastManager ShardInstance] showtoast:[NSString stringWithFormat:@"获取搜索广告1异常:%@",error]];
    }];

    [HomeModel getSearchAd2:^(AFHTTPRequestOperation* operation, NSObject* result){
        [HomeInstance ShardInstnce].listAd2 = [[HomeSearchAdList alloc] initWithDictionary:(NSDictionary*)result];
        [wTb reloadData];
    } failure:^(NSError* error){
        //[[XBToastManager ShardInstance] showtoast:[NSString stringWithFormat:@"获取搜索广告2异常:%@",error]];
    }];
}

/**
 *  晒单分享
 */
- (void)getOrderShows
{
    __weak typeof (tbView) wTb = tbView;
    
    NSDictionary *params = @{@"page_size":@3};
    [ServerHelper getShareListWithParams:params Completion:^(id result) {
        if (result == nil || [result isKindOfClass:NSNull.class]) {
            return;
        }
        [HomeInstance ShardInstnce].listOrderShows = [[HomeOrderShowList alloc] init];
        NSMutableArray *array = [NSMutableArray array];
        for (id tmp in result)
        {
            HomeOrderShowItem *item = [HomeOrderShowItem new];
            item.pics = [tmp objectForKey:@"pic"];
            item.postTitle = [tmp objectForKey:@"content"];
            item.postID = [tmp objectForKey:@"share_id"];
            [array addObject:item];
        }
        [HomeInstance ShardInstnce].listOrderShows.listItems = array;
        
        
        [wTb reloadData];
        
    } failure:^{
        
    }];
    
//    [HomeModel getOrderShow:^(AFHTTPRequestOperation* operation, NSObject* result){
//        [HomeInstance ShardInstnce].listOrderShows = [[HomeOrderShowList alloc] initWithDictionary:(NSDictionary*)result];
//        [wTb reloadData];
//    } failure:^(NSError* error){
//        //[[XBToastManager ShardInstance] showtoast:[NSString stringWithFormat:@"获取首页晒单分享异常:%@",error]];
//    }];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if(section == 3)
//        return 1;
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)//广告和 按钮
    {
//        return indexPath.row == 0 ? 150:100;
        return indexPath.row == 0 ? 0:100;
    }
    if (indexPath.section == 1 && indexPath.row == 1)
        return 140.5;
    if (indexPath.section == 2 && indexPath.row == 1)
        return DESIGN_SCREEN_WIDTH / 5 * 2 + 80;
    if (indexPath.section == 3)
    {
//        if([OyTool ShardInstance].bIsForReview)
//        {
//            return 44;
//        }
//        return 270;
        
        return 0;
    }
    if (indexPath.section == 4 && indexPath.row == 1)
    {
//        return 250;
        return 250;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0)
        return 0.1;
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSeparatorInset:UIEdgeInsetsZero];
    [cell setLayoutMargins:UIEdgeInsetsZero];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    if (section == 0)
    {
        if(row == 0)
        {
            static NSString *CellIdentifier = @"homeAdCell";
            HomeAdCell *cell = (HomeAdCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[HomeAdCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.delegate = self;
            [cell getAds];
            return cell;
        }
        else if(row == 1)
        {
            static NSString *CellIdentifier = @"homeAdBtnCell";
            HomeAdBtnCell *cell = (HomeAdBtnCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[HomeAdBtnCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.delegate = self;
            return cell;
        }
    }
    if (section == 1)
    {
        if(row == 0)
        {
            static NSString *CellIdentifier = @"newTitleCell";
            UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.textLabel.text = [CommonHelper getLocalizedText:@"section_latest"];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return cell;
        }
        else if(row == 1)
        {
            static NSString *CellIdentifier = @"homeNewCell";
            HomeNewCell *cell = (HomeNewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[HomeNewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            [cell setDelegate:self];
//            [cell setNews:[HomeInstance ShardInstnce].listNewing homepage:listHomepage.Rows1];
            [cell setCustomNews:processingData homepage:processingData.data];
//            processingData
            return cell;
        }
    }
    else if (section == 2)
    {
        if(row == 0)
        {
            static NSString *CellIdentifier = @"hotTitleCell";
            UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.textLabel.text = [CommonHelper getLocalizedText:@"section_hottest"];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return cell;
        }
        else if (row == 1)
        {
            static NSString *CellIdentifier = @"homeHotCell";
            HomeHotCell *cell = (HomeHotCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[HomeHotCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.delegate = self;
//            [cell setHots:listHomepage.Rows2];
            [cell setHots:productsData.data];
            return cell;
        }
    }
    else if(section == 3)
    {
//        if (row == 0)
//        {
//            if ([OyTool ShardInstance].bIsForReview)
//            {
//                static NSString *CellIdentifier = @"homeAdDigReviewCell";
//                UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
//                if(cell == nil)
//                {
//                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//                }
//                cell.textLabel.text = @"一元云购，云友的查询利器";
//                return cell;
//
//            }
//            else
//            {
//                static NSString *CellIdentifier = @"homeAdDigCell";
//                HomeAdDigitalCell *cell = (HomeAdDigitalCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
//                if(cell == nil)
//                {
//                    cell = [[HomeAdDigitalCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//                }
//                [cell setDelegate:self];
//                [cell doLoadAds];
//                return cell;
//            }
//        }

    }
    else if (section == 4)
    {
        if(row == 0)
        {
            static NSString *CellIdentifier = @"showTitleCell";
            UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.textLabel.text = [CommonHelper getLocalizedText:@"section_show_record"];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return cell;
        }
        else if(row==1)
        {
            static NSString *CellIdentifier = @"homeOrderShowCell";
            HomeOrderShowCell *cell = (HomeOrderShowCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil)
            {
                cell = [[HomeOrderShowCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            [cell setDelegate:self];
            [cell setOrderShows];
            return cell;

        }
    }
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] init];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 1 && indexPath.row == 0)
    {
        self.tabBarController.selectedIndex = 2;
    }
    else if(indexPath.section == 2 && indexPath.row == 0)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidShowNewPro object:[NSNumber numberWithInt:1]];
        self.tabBarController.selectedIndex = 1;
    }
    else if(indexPath.section == 4 && indexPath.row == 0)
    {
        ShowOrderListVC* vc = [[ShowOrderListVC alloc] initWithGoodsId:0];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma mark - delegate;
- (void)adClick:(NSString*)key Pid:(NSNumber*) pid PeriodId:(NSNumber*) period;
{
    if([key rangeOfString:@"search,"].length > 0)
    {
        NSString* searchKey = [key stringByReplacingOccurrencesOfString:@"search," withString:@""];
        SearchVC* vc = [[SearchVC alloc] initWithKey:searchKey];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if ([key rangeOfString:@"goodsdetail,"].length > 0)
    {
        NSString* goodsId = [key stringByReplacingOccurrencesOfString:@"goodsdetail," withString:@""];
        ProductDetailVC* vc = [[ProductDetailVC alloc] initWithGoodsId:0 codeId:[pid intValue]];
        vc.periodNum = period;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)btnHomeClick:(int)index
{
    if(index == 0)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidShowNewPro object:[NSNumber numberWithInt:3]];
        self.tabBarController.selectedIndex = 1;
    }
    else if(index == 1)
    {
        ShowOrderListVC* vc = [[ShowOrderListVC alloc] initWithGoodsId:0];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if(index == 2)
    {
//        if(![UserInstance ShardInstnce].userId)
        if(![User getInstance].isLogin)
        {
            LoginVC* vc = [[LoginVC alloc] init];
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [self.navigationController presentViewController:nav animated:YES completion:nil];
        }
        else
        {
            MineBuylistVC* vc = [[MineBuylistVC alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
}

- (void) openBookingVC
{
    BookingDetailViewController *vc = [[BookingDetailViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - newest delegate
- (void)doClickGoods:(int)goodsId codeId:(int)codeId
{
    if(goodsId == 0)
    {
        ProductDetailVC* vc = [[ProductDetailVC alloc] initWithGoodsId:0 codeId:codeId];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        ProductLotteryVC* vc = [[ProductLotteryVC alloc] initWithGoods:0 codeId:codeId];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - show delegate
- (void)doCickShare:(int)postId
{
    ShowOrderDetailVC* vc = [[ShowOrderDetailVC alloc] initWithPostId:postId];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
