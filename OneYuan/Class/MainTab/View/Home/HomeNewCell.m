//
//  HomeNewCell.m
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/19.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import "HomeNewCell.h"
#import "HomeNewIngOrEndView.h"


@interface HomeNewCell ()<HomeNewIngOrEndViewDelegate>
{
    __weak id<HomeNewCellDelegate> delegate;
    HomeNewIngOrEndView *view1;
    HomeNewIngOrEndView *view2;
    HomeNewIngOrEndView *view3;
    HomeNewIngOrEndView *view4;
}

@end

@implementation HomeNewCell
@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
      
        view1 = [[HomeNewIngOrEndView alloc] initWithFrame:CGRectMake(0, 0, mainWidth / 2, 70)];
        view1.delegate = self;
        [self addSubview:view1];
    
        view2 = [[HomeNewIngOrEndView alloc] initWithFrame:CGRectMake(mainWidth / 2, 0, mainWidth / 2, 70)];
        view2.delegate = self;
       [self addSubview:view2];
        
        view3 = [[HomeNewIngOrEndView alloc] initWithFrame:CGRectMake(0, 70, mainWidth / 2, 70)];
        view3.delegate = self;
        [self addSubview:view3];
   
        view4 = [[HomeNewIngOrEndView alloc] initWithFrame:CGRectMake(mainWidth / 2, 70, mainWidth / 2, 70)];
        view4.delegate = self;
        [self addSubview:view4];

    }
    return self;
}

- (void)setNews:(HomeNewingList*)listNewing homepage:(NSArray*)listHomepage
{
    if(!listNewing)
    {
        [view1 setNewLoad];
        [view2 setNewLoad];
        [view3 setNewLoad];
        [view4 setNewLoad];
        return;
    }
    if (listNewing.listItems.count > 0)
    {
        for (int i = 0; i<listNewing.listItems.count;i++) {
            if(i == 0)
            {
                [view1 setNewing:[listNewing.listItems objectAtIndex:i]];
            }
            else if(i == 1)
            {
                [view2 setNewing:[listNewing.listItems objectAtIndex:i]];
            }
            else if(i == 2)
            {
                [view3 setNewing:[listNewing.listItems objectAtIndex:i]];
            }
            else if(i == 3)
            {
                [view4 setNewing:[listNewing.listItems objectAtIndex:i]];
            }
        }
    }
    int newCount = (int)listNewing.listItems.count;
    int leftCount = 4 - newCount;//剩下的 几个
    for(int i =0;i<leftCount;i++)//遍历
    {
        int location = newCount + i;
        if(location == 0)
        {
            [view1 setNewed:[listHomepage objectAtIndex:i]];
        }
        else if(location == 1)
        {
            [view2 setNewed:[listHomepage objectAtIndex:i]];
        }
        else if(location == 2)
        {
            [view3 setNewed:[listHomepage objectAtIndex:i]];
        }
        else if(location == 3)
        {
            [view4 setNewed:[listHomepage objectAtIndex:i]];
        }
    }
}

- (void)setCustomNews:(ProcessingList*)listNewing homepage:(NSArray*)listHomepage
{
    if(!listNewing || !listHomepage)
    {
        [view1 setNewLoad];
        [view2 setNewLoad];
        [view3 setNewLoad];
        [view4 setNewLoad];
        return;
    }
    
    if (listHomepage.count > 0) {
        for (int i = 0; i<listHomepage.count && i < 4;i++) {
            int state = [((ProcessingItem*)[listNewing.data objectAtIndex:i]).state intValue];
            if (i == 0) {
                if (state < 2) {
                    [view1 setCustomNewing:[listNewing.data objectAtIndex:i]];
                }
                else{
                    [view1 setCustomNewed:[listNewing.data objectAtIndex:i]];
                }
            }
            else if (i == 1)
            {
                if (state < 2) {
                    [view2 setCustomNewing:[listNewing.data objectAtIndex:i]];
                }
                else{
                    [view2 setCustomNewed:[listNewing.data objectAtIndex:i]];
                }
            }
            else if (i == 2)
            {
                if (state < 2) {
                    [view3 setCustomNewing:[listNewing.data objectAtIndex:i]];
                }
                else{
                    [view3 setCustomNewed:[listNewing.data objectAtIndex:i]];
                }
            }
            else if (i == 3)
            {
                if (state < 2) {
                    [view4 setCustomNewing:[listNewing.data objectAtIndex:i]];
                }
                else{
                    [view4 setCustomNewed:[listNewing.data objectAtIndex:i]];
                }
            }
        }
    }
    
    
//    if (listNewing.data.count > 0)
//    {
//        for (int i = 0; i<listNewing.data.count;i++) {
//            if(i == 0)
//            {
//                [view1 setCustomNewing:[listNewing.data objectAtIndex:i]];
//            }
//            else if(i == 1)
//            {
//                [view2 setCustomNewing:[listNewing.data objectAtIndex:i]];
//            }
//            else if(i == 2)
//            {
//                [view3 setCustomNewing:[listNewing.data objectAtIndex:i]];
//            }
//            else if(i == 3)
//            {
//                [view4 setCustomNewing:[listNewing.data objectAtIndex:i]];
//            }
//        }
//    }
//    int newCount = (int)listNewing.data.count;
//    int leftCount = 4 - newCount;//剩下的 几个
//    for(int i =0;i<leftCount;i++)//遍历
//    {
//        int location = newCount + i;
//        if(location == 0)
//        {
//            [view1 setCustomNewing:[listHomepage objectAtIndex:i]];
//        }
//        else if(location == 1)
//        {
//            [view2 setCustomNewing:[listHomepage objectAtIndex:i]];
//        }
//        else if(location == 2)
//        {
//            [view3 setCustomNewing:[listHomepage objectAtIndex:i]];
//        }
//        else if(location == 3)
//        {
//            [view4 setCustomNewing:[listHomepage objectAtIndex:i]];
//        }
//    }
    
    
    
    
}

#pragma mark - delegate
- (void)doClickGoods:(int)goodsId codeId:(int)codeId
{
    if(delegate)
    {
        [delegate doClickGoods:goodsId codeId:codeId];
    }
}
@end
