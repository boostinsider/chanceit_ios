//
//  MineUserView.m
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/23.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import "MineUserView.h"
#import "UserInstance.h"
#import "User.h"

@interface MineUserView ()
{
    __weak id<MineUserViewDelegate> delegate;
    
    __block UILabel             *lblMoney2;
}
@end

@implementation MineUserView
@synthesize  delegate;

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self)
//    {
//        UIImageView* lineTop = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 0.5)];
//        lineTop.backgroundColor = [UIColor hexFloatColor:@"dedede"];
//        [self addSubview:lineTop];
//        
//        UIImageView* lineBot = [[UIImageView alloc] initWithFrame:CGRectMake(0, frame.size.height - 10, mainWidth, 0.5)];
//        lineBot.backgroundColor = [UIColor hexFloatColor:@"dedede"];
//        [self addSubview:lineBot];
//        
//        UIView* v = [[UIView alloc] initWithFrame:CGRectMake(0, 0.5, mainWidth, frame.size.height - 10.5)];
//        v.backgroundColor = [UIColor whiteColor];
//        [self addSubview:v];
//        
//        UIImageView* img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 60, 60)];
//        img.layer.masksToBounds = YES;
//        img.layer.cornerRadius = 30;
//        [img setImage_oy:nil image:[UserInstance ShardInstnce].userPhoto];
//        [v addSubview:img];
//        
//        UILabel* lblName = [[UILabel alloc] init];
//        lblName.text = [UserInstance ShardInstnce].userName;
//        lblName.textColor = [UIColor grayColor];
//        lblName.font = [UIFont systemFontOfSize:16];
//        [v addSubview:lblName];
//        CGSize s = [lblName.text textSizeWithFont:lblName.font constrainedToSize:CGSizeMake(MAXFLOAT, 999) lineBreakMode:NSLineBreakByCharWrapping];
//        CGFloat maxw = mainWidth - 190;
//        lblName.frame = CGRectMake(80, 20, s.width > maxw ? maxw : s.width, 15);
//        
//        UILabel* lblPhone = [[UILabel alloc] init];
//        lblPhone.text = [NSString stringWithFormat:@"(%@)",[UserInstance ShardInstnce].userPhone];
//        lblPhone.textColor = [UIColor lightGrayColor];
//        lblPhone.font = [UIFont systemFontOfSize:14];
//        lblPhone.frame = CGRectMake(lblName.frame.size.width + lblName.frame.origin.x + 5, 20, 200, 14) ;
//        [v addSubview:lblPhone];
//        
//        UIImageView* imgLevel = [[UIImageView alloc] initWithFrame:CGRectMake(85, 50, 12, 12)];
//        NSString* level = [NSString stringWithFormat:@"degree%d",[[UserInstance ShardInstnce].userLevel intValue]];
//        imgLevel.image = [UIImage imageNamed:level];
//        [v addSubview:imgLevel];
//        
//        UILabel* lblLevel = [[UILabel alloc] initWithFrame:CGRectMake(103, 50, 200, 12)];
//        lblLevel.textColor = [UIColor lightGrayColor];
//        lblLevel.text = [UserInstance ShardInstnce].userLevelName;
//        lblLevel.font = [UIFont systemFontOfSize:12];
//        [v addSubview:lblLevel];
//        
//        UILabel* lblExp = [[UILabel alloc] initWithFrame:CGRectMake(170, 50, 200, 12)];
//        lblExp.textColor = [UIColor lightGrayColor];
//        lblExp.text = [NSString stringWithFormat:@"经验值:%@",[UserInstance ShardInstnce].userExp];
//        lblExp.font = [UIFont systemFontOfSize:12];
//        [v addSubview:lblExp];
//        
//        UILabel* lblFufen1 = [[UILabel alloc] initWithFrame:CGRectMake(20, 90, 200, 12)];
//        lblFufen1.textColor = [UIColor lightGrayColor];
//        lblFufen1.text = @"可用福分:";
//        lblFufen1.font = [UIFont systemFontOfSize:12];
//        [v addSubview:lblFufen1];
//        
//        UILabel* lblFufen2 = [[UILabel alloc] initWithFrame:CGRectMake(75, 90, 200, 12)];
//        lblFufen2.textColor = mainColor;
//        lblFufen2.text = [UserInstance ShardInstnce].userFuFen;
//        lblFufen2.font = [UIFont systemFontOfSize:16];
//        [v addSubview:lblFufen2];
//        
//        UILabel* lblMoney1 = [[UILabel alloc] initWithFrame:CGRectMake(120, 90, 200, 12)];
//        lblMoney1.textColor = [UIColor lightGrayColor];
//        lblMoney1.text = @"可用余额:";
//        lblMoney1.font = [UIFont systemFontOfSize:12];
//        [v addSubview:lblMoney1];
//        
//        UILabel* lblMoney2 = [[UILabel alloc] initWithFrame:CGRectMake(173, 90, 200, 12)];
//        lblMoney2.textColor = mainColor;
//        lblMoney2.text = [UserInstance ShardInstnce].userMoney;
//        lblMoney2.font = [UIFont systemFontOfSize:16];
//        [v addSubview:lblMoney2];
//        
//        UIButton* btnPay = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth - 60, 80, 50, 30)];
//        [btnPay setTitle:@"充值" forState:UIControlStateNormal];
//        [btnPay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [btnPay setBackgroundColor:mainColor];
//        btnPay.titleLabel.font = [UIFont systemFontOfSize:13];
//        btnPay.layer.cornerRadius = 4;
//        [btnPay addTarget:self action:@selector(btnPayAction) forControlEvents:UIControlEventTouchUpInside];
//        [v  addSubview:btnPay];
//    }
//    return self;
//}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        UIImageView* lineTop = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, mainWidth, 0.5)];
        lineTop.backgroundColor = [UIColor hexFloatColor:@"dedede"];
        [self addSubview:lineTop];
        
        UIImageView* lineBot = [[UIImageView alloc] initWithFrame:CGRectMake(0, frame.size.height - 10, mainWidth, 0.5)];
        lineBot.backgroundColor = [UIColor hexFloatColor:@"dedede"];
        [self addSubview:lineBot];
        
        UIView* v = [[UIView alloc] initWithFrame:CGRectMake(0, 0.5, mainWidth, frame.size.height - 10.5)];
        v.backgroundColor = [UIColor whiteColor];
        [self addSubview:v];
        
        UIImageView* img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 60, 60)];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 30;
        [img setImage_oy:oyImageBaseUrl image:[User getInstance].photoUrl];
        [v addSubview:img];
        
        UILabel* lblName = [[UILabel alloc] init];
        lblName.text = [User getInstance].username;
        lblName.textColor = [UIColor grayColor];
        lblName.font = [UIFont systemFontOfSize:16];
        [v addSubview:lblName];
        CGSize s = [lblName.text textSizeWithFont:lblName.font constrainedToSize:CGSizeMake(MAXFLOAT, 999) lineBreakMode:NSLineBreakByCharWrapping];
        CGFloat maxw = mainWidth - 190;
        lblName.frame = CGRectMake(80, 20, s.width > maxw ? maxw : s.width, 15);
        
        UILabel* lblPhone = [[UILabel alloc] init];
//        lblPhone.text = [NSString stringWithFormat:@"(%@)",[UserInstance ShardInstnce].userPhone];
         lblPhone.text = [NSString stringWithFormat:@"%@",[User getInstance].nickname];
        lblPhone.textColor = [UIColor lightGrayColor];
        lblPhone.font = [UIFont systemFontOfSize:14];
        lblPhone.frame = CGRectMake(lblName.frame.size.width + lblName.frame.origin.x + 5, 20, 200, 14) ;
        [v addSubview:lblPhone];
        
        UILabel* lblBio = [[UILabel alloc] init];
        //        lblPhone.text = [NSString stringWithFormat:@"(%@)",[UserInstance ShardInstnce].userPhone];
        lblBio.text = [NSString stringWithFormat:@"%@",[User getInstance].bio];
        lblBio.textColor = [UIColor lightGrayColor];
        lblBio.font = [UIFont systemFontOfSize:14];
        lblBio.frame = CGRectMake(lblName.frame.size.width + lblName.frame.origin.x + 5, lblPhone.frame.origin.y + lblPhone.frame.size.height + 10, 200, 20) ;
        [v addSubview:lblBio];
        
        UIImageView* imgLevel = [[UIImageView alloc] initWithFrame:CGRectMake(85, 50, 12, 12)];
//        NSString* level = [NSString stringWithFormat:@"degree%d",[[UserInstance ShardInstnce].userLevel intValue]];
        NSString* level = [NSString stringWithFormat:@"degree%d",1];
        imgLevel.image = [UIImage imageNamed:level];
        [v addSubview:imgLevel];
        imgLevel.hidden = YES;
        
        UILabel* lblLevel = [[UILabel alloc] initWithFrame:CGRectMake(103, 50, 200, 12)];
        lblLevel.textColor = [UIColor lightGrayColor];
//        lblLevel.text = [UserInstance ShardInstnce].userLevelName;
        lblLevel.text = @"Fresh Man";
        lblLevel.font = [UIFont systemFontOfSize:12];
        [v addSubview:lblLevel];
        lblLevel.hidden = YES;
        
        UILabel* lblExp = [[UILabel alloc] initWithFrame:CGRectMake(170, 50, 200, 12)];
        lblExp.textColor = [UIColor lightGrayColor];
//        lblExp.text = [NSString stringWithFormat:@"经验值:%@",[UserInstance ShardInstnce].userExp];
        lblExp.text = [NSString stringWithFormat:@"%@:%@",[CommonHelper getLocalizedText:@"exp"],@"0"];
        lblExp.font = [UIFont systemFontOfSize:12];
        [v addSubview:lblExp];
        lblExp.hidden = YES;
        
//        UILabel* lblFufen1 = [[UILabel alloc] initWithFrame:CGRectMake(20, 90, 200, 12)];
//        lblFufen1.textColor = [UIColor lightGrayColor];
//        lblFufen1.text = [NSString stringWithFormat:@"%@:",[CommonHelper getLocalizedText:@"red_packets"]];
//        lblFufen1.font = [UIFont systemFontOfSize:12];
//        [v addSubview:lblFufen1];
//        
//        UILabel* lblFufen2 = [[UILabel alloc] initWithFrame:CGRectMake(75, 90, 200, 12)];
//        lblFufen2.textColor = mainColor;
//        lblFufen2.text = [NSString stringWithFormat:@"%d" , [User getInstance].redPacket];
//        lblFufen2.font = [UIFont systemFontOfSize:16];
//        [v addSubview:lblFufen2];
        
//        UILabel* lblMoney1 = [[UILabel alloc] initWithFrame:CGRectMake(120, 90, 200, 12)];
        UILabel* lblMoney1 = [[UILabel alloc] initWithFrame:CGRectMake(20, 90, 200, 12)];
        lblMoney1.textColor = [UIColor lightGrayColor];
        lblMoney1.text = [NSString stringWithFormat:@"%@:",[CommonHelper getLocalizedText:@"my_coins"]];
        lblMoney1.font = [UIFont systemFontOfSize:12];
        [v addSubview:lblMoney1];
        
//        lblMoney2 = [[UILabel alloc] initWithFrame:CGRectMake(173, 90, 200, 12)];
        lblMoney2 = [[UILabel alloc] initWithFrame:CGRectMake(75, 90, 200, 12)];
        lblMoney2.textColor = mainColor;
        lblMoney2.text = [NSString stringWithFormat:@"%d" , [User getInstance].coins];
        lblMoney2.font = [UIFont systemFontOfSize:16];
        [v addSubview:lblMoney2];
        
        UIButton* btnPay = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth - 80, 80, 70, 30)];
        [btnPay setTitle:[CommonHelper getLocalizedText:@"recharge"] forState:UIControlStateNormal];
        [btnPay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnPay setBackgroundColor:mainColor];
        btnPay.titleLabel.font = [UIFont systemFontOfSize:13];
        btnPay.layer.cornerRadius = 4;
        [btnPay addTarget:self action:@selector(btnPayAction) forControlEvents:UIControlEventTouchUpInside];
        [v  addSubview:btnPay];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetUserInfo) name:kDidReloadUser object:nil];
        
        self.userInteractionEnabled = YES;
        UITapGestureRecognizer* singleTapTag = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selfTap)];
        [self addGestureRecognizer :singleTapTag];
    }
    return self;
}

-(void)resetUserInfo
{
    [ServerHelper getUserInfoWithParameters:nil Completion:^(NSDictionary *user) {
        [[User getInstance] setUserInfo:[user objectForKey:@"data"]];
        lblMoney2.text = [NSString stringWithFormat:@"%d" , [User getInstance].coins];
    } failure:^{
        ;
    }];
}

- (void)btnPayAction
{
    if(delegate)
    {
        [delegate btnPayAction];
    }
}

- (void)selfTap
{
    if(delegate)
    {
        [delegate gotoProfile];
    }
}
@end
