//
//  CartCell.m
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/26.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import "CartCell.h"
#import "ProductModel.h"

@interface CartCell ()
{
    __weak id<CartCellDelegate> delegate;
    
    CartItem    *myItem;
    
    UIImageView *imgPro;
    UILabel     *lblTitle;
    __block UILabel     *lblLeft;
    
    UIButton    *btnDown;
    UIButton    *btnAdd;
    UITextField *txtNum;
    
    int         maxNum;
}
@end

@implementation CartCell
@synthesize delegate,parentTableView,index,parentVC,pCloseKeyboardView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        imgPro = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 80, 80)];
        imgPro.image = [UIImage imageNamed:@"noimage"];
        imgPro.layer.borderWidth = 0.5;
        imgPro.layer.cornerRadius = 10;
        imgPro.layer.borderColor = [[UIColor hexFloatColor:@"dedede"] CGColor];
        imgPro.layer.masksToBounds = YES;
        [self addSubview:imgPro];
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(100, 10, mainWidth - 110, 35)];
        lblTitle.font = [UIFont systemFontOfSize:14];
        lblTitle.textColor = [UIColor grayColor];
        lblTitle.numberOfLines = 2;
        lblTitle.lineBreakMode = NSLineBreakByTruncatingTail;
        [self addSubview:lblTitle];
        
        lblLeft = [[UILabel alloc] initWithFrame:CGRectMake(100, 50, mainWidth - 100, 15)];
        lblLeft.font = [UIFont systemFontOfSize:12];
        lblLeft.textColor = [UIColor lightGrayColor];
        lblLeft.text = [CommonHelper getLocalizedText:@"loading"];
        [self addSubview:lblLeft];
        
        UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(100, 75, mainWidth - 100, 15)];
        lbl.font = [UIFont systemFontOfSize:13];
        lbl.textColor = [UIColor lightGrayColor];
        lbl.text = [CommonHelper getLocalizedText:@"buy_count"];
        [self addSubview:lbl];
        
        btnDown = [[UIButton alloc] initWithFrame:CGRectMake(180, 60, 30, 30)];
        [btnDown setImage:[UIImage imageNamed:@"btndown_normal"] forState:UIControlStateNormal];
        [btnDown addTarget:self action:@selector(downnum) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnDown];
        
        txtNum = [[UITextField alloc] initWithFrame:CGRectMake(220, 60, mainWidth - 280, 30)];
        txtNum.layer.borderWidth = 0.5;
        txtNum.layer.borderColor = [[UIColor hexFloatColor:@"dedede"] CGColor];
        txtNum.layer.cornerRadius = 3;
        txtNum.font = [UIFont systemFontOfSize:13];
        txtNum.returnKeyType = UIReturnKeyDone;
        txtNum.textAlignment = NSTextAlignmentCenter;
        txtNum.enabled = YES;
        txtNum.delegate = self;
        txtNum.keyboardType=UIKeyboardTypeNumberPad;
        [self addSubview:txtNum];
        
        btnAdd = [[UIButton alloc] initWithFrame:CGRectMake(mainWidth - 50, 60, 30, 30)];
        [btnAdd setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
        [btnAdd addTarget:self action:@selector(addnum) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnAdd];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKeyboard) name:kHideKeyboard object:nil];
    }
    return self;
}

-(void)hideKeyboard
{
    if ([txtNum isFirstResponder]) {
        [txtNum resignFirstResponder];
    }
}

-(void)keyboardWillHide :(NSNotification*) notification
{
    if (![txtNum isFirstResponder]) {
        return;
    }
    if (parentTableView) {
        CGFloat offset = parentVC.view.frame.origin.y;
        if (offset != 64)
        {
            [UIView beginAnimations: @"ResizeForKeyboard" context: nil];
            [UIView setAnimationDuration: 0.3];
            
            parentVC.view.frame = CGRectMake(0, 64, parentVC.view.frame.size.width, parentVC.view.frame.size.height);
            
            [UIView commitAnimations];
        }
    }
    pCloseKeyboardView.hidden = YES;
}

#pragma keyboard show and hide

-(void)keyboardWillShow :(NSNotification*) notification
{
    if (![txtNum isFirstResponder]) {
        return;
    }
    
    NSDictionary* info  = notification.userInfo;
    NSValue* value = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect rect = [value CGRectValue];
    
    CGFloat keyBoardHeight = rect.size.height;
    
    CGFloat marginTop = 0;
    
    CGFloat offset = 100.0f * (index + 1)  - parentTableView.contentOffset.y + marginTop - (parentVC.view.frame.size.height - keyBoardHeight);
    
    
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:0.3];
    if (offset > 0) {
        parentVC.view.frame = CGRectMake(0,parentVC.view.frame.origin.y - offset, parentVC.view.frame.size.width, parentVC.view.frame.size.height);
    }
    
    [UIView commitAnimations];
    
    pCloseKeyboardView.hidden = NO;
}


- (void)setCart:(CartItem*)item
{
    myItem = item;
    [imgPro setImage_oy:oyImageBaseUrl image:item.cSrc];
    lblTitle.text = [NSString stringWithFormat:@"(%@%@%@)%@",[CommonHelper getLocalizedText:@"no_pre"],[item.cPeriod stringValue],[CommonHelper getLocalizedText:@"no_tail"],item.cName];
    txtNum.text = [item.cBuyNum stringValue];
    
    maxNum = 0;
    
    //统计剩余人次
     NSDictionary* dic = @{@"pid":item.cCid};
    
    JGProgressHUD *progress = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleExtraLight];

    [progress showInView:self.contentView];
    
    [ServerHelper getShopOverWithParameters:dic Completion:^(NSDictionary *result) {
        [progress dismiss];
        NSLog(@"getShopOverWithParameters succeed!");
        
        ShopOverResult* shopOverResult = [[ShopOverResult alloc]initWithDictionary:result];
        
        if ([shopOverResult.status intValue] == 200) {
            int leftCount = shopOverResult.data.price.intValue - shopOverResult.data.number.intValue;
            lblLeft.text = [NSString stringWithFormat:@"%@%d%@",[CommonHelper getLocalizedText:@"require_left"],leftCount,[CommonHelper getLocalizedText:@"price_number"]];
            maxNum = leftCount;
            [self setDis:[item.cBuyNum intValue]];
        }
        else if ([shopOverResult.status intValue] == 401)
        {
            //go to login page
             [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        
        
    } failure:^{
        [progress dismiss];
        NSLog(@"getAnnouncedProductsWithParameters failed!");
    }];
}

- (void)setDis:(int)num
{
    if(num <=0)
    {
        btnDown.layer.opacity = 0.6;
        btnAdd.layer.opacity = 0.6;
        txtNum.text = @"0";
        myItem.cBuyNum = [NSNumber numberWithInt:0];
        txtNum.text = [myItem.cBuyNum stringValue];
        [CartModel addorUpdateCart:myItem];
        if (delegate)
        {
            [delegate setOpt];
        }
    }
    else
    {
        if(num > 1)
            btnDown.layer.opacity = 1;
        if(num >= maxNum)
        {
            myItem.cBuyNum = [NSNumber numberWithInt:maxNum];
            txtNum.text = [myItem.cBuyNum stringValue];
            [CartModel addorUpdateCart:myItem];
            btnAdd.layer.opacity = 0.6;
        }
        else
        {
            btnAdd.layer.opacity = 1;
        }
        
    }
}

- (void)addnum
{
    if([myItem.cBuyNum intValue] >= maxNum )
    {
        return;
    }
    
    if([myItem.cBuyNum intValue] >= maxNum - 1)
    {
        btnAdd.layer.opacity = 0.6;
    }
    btnDown.layer.opacity = 1;

    
    myItem.cBuyNum = [NSNumber numberWithInt:[myItem.cBuyNum intValue] + 1];
    txtNum.text = [myItem.cBuyNum stringValue];
    
    [CartModel addorUpdateCart:myItem];
    
    if (delegate)
    {
        [delegate setOpt];
    }
    
}

- (void)downnum
{
    int num = [myItem.cBuyNum intValue];
    if(num <= 1)
    {
        return;
    }
    
    if(num <= 2)
    {
        btnDown.layer.opacity = 0.6;
    }
    btnAdd.layer.opacity = 1.0;
    
    myItem.cBuyNum = [NSNumber numberWithInt:num - 1];
    txtNum.text = [myItem.cBuyNum stringValue];
    
    [CartModel addorUpdateCart:myItem];
    
    if (delegate)
    {
        [delegate setOpt];
    }
}

#pragma textfeild delegete begin
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if ([txtNum isFirstResponder]) {
        if (txtNum.text.length == 0) {
            textField.text = @"1";
            btnDown.layer.opacity = 0.6;
            myItem.cBuyNum = [NSNumber numberWithInt:1];
            [CartModel addorUpdateCart:myItem];
            
            if (delegate)
            {
                [delegate setOpt];
            }
        }
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        
        [textField resignFirstResponder];
        
        return NO;
        
    }
    
    if ([txtNum isFirstResponder]) {
        BOOL isValid = string.length == 0 || [self validateNumber:string];
        
        if (isValid == NO) {
            return NO;
        }
        
        if (string.length > 0 && isValid) {
            NSString* tmpStr = [NSString stringWithFormat:@"%@%@",txtNum.text,string];
            int length = (int)tmpStr.length;
            int result = 0;
            for (int i = 0; i<length; i++) {
                int x = [tmpStr characterAtIndex:i] - 48;
                int pos = length - i - 1;
                result += x * pow(10.0, pos);
            }
            
            if (result <= 0 ) {
                result = 1;
            }
            
            if (result >= maxNum) {
                result = maxNum;
                
                btnAdd.layer.opacity = 0.6;
            }
            else
            {
                btnAdd.layer.opacity = 1;
            }
            btnDown.layer.opacity = result > 1 ? 1 : 0.6;
            
            myItem.cBuyNum = [NSNumber numberWithInt:result];
            
            NSString* finalString = [NSString stringWithFormat:@"%d",result];
            textField.text = finalString;
            
            [CartModel addorUpdateCart:myItem];
            
            if (delegate)
            {
                [delegate setOpt];
            }
            
            return NO;
        }
        else
        {
            //is backspace
            if (textField.text.length == 1) {//back to empty
//                textField.text = @"1";
//                btnDown.layer.opacity = 0.6;
//                myItem.cBuyNum = [NSNumber numberWithInt:1];
//                [CartModel addorUpdateCart:myItem];
//                
//                if (delegate)
//                {
//                    [delegate setOpt];
//                }
                return YES;
            }
            else
            {
                myItem.cBuyNum = [NSNumber numberWithInt:[myItem.cBuyNum intValue] / 10];
                btnDown.layer.opacity = [myItem.cBuyNum intValue]  > 1 ? 1.0 : 0.6;
                if ([myItem.cBuyNum intValue] < maxNum) {
                    btnAdd.layer.opacity = 1.0;
                }
                else
                {
                    btnAdd.layer.opacity = 0.6;
                }
                
                [CartModel addorUpdateCart:myItem];
                
                if (delegate)
                {
                    [delegate setOpt];
                }
                return YES;
            }
        }
    }
    
    return YES;
}

- (BOOL)validateNumber:(NSString*)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}


@end
