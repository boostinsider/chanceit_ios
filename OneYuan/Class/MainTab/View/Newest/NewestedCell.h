//
//  NewestedCell.h
//  OneYuan
//
//  Created by Peter Jin (https://github.com/JxbSir) on  15/2/22.
//  Copyright (c) 2015年 PeterJin.   Email:i@jxb.name      All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewestModel.h"
#import "HomeModel.h"

@interface NewestedCell : UITableViewCell
- (void)setNewed:(NewestProItme*)newed;
- (void)setCustomNewed:(ProcessingItem*)newed;
@end
