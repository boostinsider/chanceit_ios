//
//  CalculateListVC.m
//  OneYuan
//
//  Created by NW on 16/6/30.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "CalculateListVC.h"
#import "ProductModel.h"

@interface CalculateListVC ()<UITableViewDataSource,UITableViewDelegate>
{
    __block UITableView     *tbView;
    __block int             curPage;
    __block NSMutableArray  *arrData;
    __block CalculateItem   *calculateItem;
    
    int _codeId;
    int _detailsIndex;
}
@end

@implementation CalculateListVC

- (id)initWithCodeId:(int)codeId
{
    self = [super init];
    if(self)
    {
        _codeId = codeId;
        _detailsIndex = 1;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    self.title = [CommonHelper getLocalizedText:@"calculate_list_nav_title"];
    __weak typeof (self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"btnback" htlImage:@"btnback" title:@"" action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    UIView* pLine = [[UIView alloc]initWithFrame:CGRectMake(0, 49, self.view.frame.size.width, 1)];
    pLine.backgroundColor = COLOR_DARK_GRAY;
    [self.view addSubview:pLine];
    
    tbView = [[UITableView alloc] initWithFrame:self.view.bounds];
    tbView.delegate = self;
    tbView.dataSource = self;
    tbView.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    tbView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tbView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:tbView];
    
    [tbView addPullToRefreshWithActionHandler:^{
        __strong typeof (wSelf) sSelf = wSelf;
        sSelf->curPage = 1;
        [wSelf getData:^{
            sSelf->arrData = nil;
        }];
    }];
    
//    [tbView addInfiniteScrollingWithActionHandler:^{
//        __strong typeof (wSelf) sSelf = wSelf;
//        sSelf->curPage ++;
//        [wSelf getData:nil];
//    }];
    
    [tbView.pullToRefreshView setOYStyle];
    [tbView triggerPullToRefresh];
}

- (void)getData:(void (^)(void))block
{
    __weak typeof (self) wSelf = self;
    NSDictionary* dic = @{@"pid":@(_codeId)};
    [ServerHelper getAnnouncedDetails:dic Completion:^(NSDictionary *products) {
        NSLog(@"getAnnouncedProductsWithParameters succeed!");
        [[XBToastManager ShardInstance] hideprogress];
        [wSelf hideLoad];
        [tbView.pullToRefreshView stopAnimating];
        //
        CalculateData *calculateData = [[CalculateData alloc]initWithDictionary:products];
        calculateItem = calculateData.data;
        arrData = [NSMutableArray arrayWithArray:calculateData.data.kjtime];
        [tbView reloadData];
//        [tbView setShowsInfiniteScrolling:listNew.count<[list.count integerValue]];
        //        if(block!=NULL)
        //            block();
        //        NewestProList* list = [[NewestProList alloc] initWithDictionary:(NSDictionary*)products];
        //        if (!listNew)
        //        {
        //            listNew = [NSMutableArray arrayWithArray:list.Rows];
        //        }
        //        else
        //        {
        //            [listNew addObjectsFromArray:list.Rows];
        //        }
        //        [tbView reloadData];
        //        [tbView setShowsInfiniteScrolling:listNew.count<[list.count integerValue]];
        //set new
        
        //        processingData = [[ProcessingList alloc]initWithDictionary:products];
        //        ProcessingList* list = [[ProcessingList alloc] initWithDictionary:(NSDictionary*)products];
        
        //        if (list.data && [list.data isKindOfClass:NSArray.class]) {
        //            listNew =  [NSMutableArray arrayWithArray:list.data];
        //            [tbView reloadData];
        //        }
    } failure:^{
        NSLog(@"getAnnouncedProductsWithParameters failed!");
        //[[XBToastManager ShardInstance] showtoast:[NSString stringWithFormat:@"获取最新揭晓页面数据异常:%@",error]];
        [wSelf hideLoad];
        [tbView.pullToRefreshView stopAnimating];
    }];
}

#pragma mark - tableview

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 48;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* pBg = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 48)];
    pBg.backgroundColor = [UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    
    for (int i = 0 ; i < 4;  i ++) {
        UILabel* titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(i * self.view.frame.size.width / 4, 0, self.view.frame.size.width / 4, 48)];
        [pBg addSubview:titleLabel];
        titleLabel.font = [UIFont systemFontOfSize:16];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = COLOR_RED;
        titleLabel.text = [CommonHelper getLocalizedText:[NSString stringWithFormat:@"calculate_table_hearder_%d",i]];
    }
    
    return pBg;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrData.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == _detailsIndex) {
        // scroll height
        
        NSString* str = [NSString stringWithFormat:@"%@\n%@%@%@\n\n%@%@%@%d%@%d%@\n\n%@%d + 10000001 = %d",
                         [CommonHelper getLocalizedText:@"result_rule_line_0"],
                         [CommonHelper getLocalizedText:@"result_rule_line_1_0"],calculateItem.shop.kaijiang_count,[CommonHelper getLocalizedText:@"result_rule_line_1_1"],
                         [CommonHelper getLocalizedText:@"result_rule_line_2_0"],calculateItem.shop.kaijiang_count,[CommonHelper getLocalizedText:@"result_rule_line_2_1"],[calculateItem.shop.number intValue],
                         [CommonHelper getLocalizedText:@"result_rule_line_2_2"],[calculateItem.shop.kaijiang_count intValue] %[calculateItem.shop.number intValue],[CommonHelper getLocalizedText:@"result_rule_line_2_3"],
                         [CommonHelper getLocalizedText:@"result_rule_line_3_0"],[calculateItem.shop.kaijiang_count intValue]%[calculateItem.shop.number intValue],[calculateItem.shop.kaijang_num intValue]];
        
        CGFloat height = [CommonHelper getTextViewSize:str Width:self.view.frame.size.width - 16 * 2 Font:[UIFont systemFontOfSize:14]].height + 2 * 4;
        
        return height;
    }
    return 44;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"calculate_list_cell";
    UITableViewCell *cell =[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    for (UIView* view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    if (indexPath.row == _detailsIndex) {
        // scroll height
        NSString* str = [NSString stringWithFormat:@"%@\n%@%@%@\n\n%@%@%@%d%@%d%@\n\n%@%d + 10000001 = %d",
                         [CommonHelper getLocalizedText:@"result_rule_line_0"],
                         [CommonHelper getLocalizedText:@"result_rule_line_1_0"],calculateItem.shop.kaijiang_count,[CommonHelper getLocalizedText:@"result_rule_line_1_1"],
                         [CommonHelper getLocalizedText:@"result_rule_line_2_0"],calculateItem.shop.kaijiang_count,[CommonHelper getLocalizedText:@"result_rule_line_2_1"],[calculateItem.shop.number intValue],
                         [CommonHelper getLocalizedText:@"result_rule_line_2_2"],[calculateItem.shop.kaijiang_count intValue] %[calculateItem.shop.number intValue],[CommonHelper getLocalizedText:@"result_rule_line_2_3"],
                         [CommonHelper getLocalizedText:@"result_rule_line_3_0"],[calculateItem.shop.kaijiang_count intValue]%[calculateItem.shop.number intValue],[calculateItem.shop.kaijang_num intValue]];
        
//        shop =         {
//            "kaijang_num" = 10000062;
//            "kaijiang_count" = 389363821;
//            "kaijiang_ssc" = 0;
//            no = 100003;
//            number = 6480;
//            state = 2;
//        };
        
        CGFloat height = [CommonHelper getTextViewSize:str Width:self.view.frame.size.width - 16 * 2 Font:[UIFont systemFontOfSize:14]].height;
        cell.contentView.backgroundColor = [UIColor colorWithRed:252.0f/255 green:102.0f/255 blue:33.0f/255 alpha:1.0f];
        
        UITextView* txtView = [[UITextView alloc]initWithFrame:CGRectMake(16, 4, self.view.frame.size.width - 16 * 2, height)];
        txtView.font = [UIFont systemFontOfSize:14];
        txtView.editable = NO;
        txtView.userInteractionEnabled = NO;
        txtView.backgroundColor = [UIColor clearColor];
        txtView.textColor = [UIColor colorWithRed:252.0f/255 green:248.0f/255 blue:219.0f/255 alpha:1];
        txtView.text = str;
        
        [cell.contentView addSubview:txtView];
    }
    else
    {
        int arrayIndex = indexPath.row < _detailsIndex ? (int)indexPath.row : (int)indexPath.row - 1;
        CalculateItemKJTime* kjtm = [arrData objectAtIndex:arrayIndex];
        
        for (int i = 0 ; i < 4;  i ++) {
            UILabel* titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(i * self.view.frame.size.width / 4, 0, self.view.frame.size.width / 4, 44)];
            [cell.contentView addSubview:titleLabel];
            titleLabel.font = [UIFont systemFontOfSize:12];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.textColor = COLOR_DARK_GRAY;
            titleLabel.text = [CommonHelper getLocalizedText:[NSString stringWithFormat:@"calculate_table_hearder_%d",i]];
            
            switch (i) {
                case 0:
                    titleLabel.text = kjtm.create_date;
                    break;
                case 1:
                    titleLabel.text = kjtm.create_hour;
                    break;
                case 2:
                    titleLabel.text = [NSString stringWithFormat:@"%d",kjtm.create_int.intValue];
                    break;
                case 3:
                    titleLabel.text = kjtm.nickname;
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    if (indexPath.row < 51 && indexPath.row != _detailsIndex) {
        cell.contentView.backgroundColor = [UIColor colorWithRed:252.0f/255 green:248.0f/255 blue:219.0f/255 alpha:1];
    }
    
    cell.selectionStyle =  UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    
//    ShowOrderDetailVC* vc = [[ShowOrderDetailVC alloc] initWithPostId:[[[arrData objectAtIndex:indexPath.section] postID] intValue]];
//    vc.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:vc animated:YES];
}

@end
