//
//  GenderEditViewController.m
//  OneYuan
//
//  Created by NW on 16/6/13.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "GenderEditViewController.h"

@interface GenderEditViewController ()

@end

@implementation GenderEditViewController
@synthesize tableView;
@synthesize selectIndex;
@synthesize data;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    UIBarButtonItem*  buttonSave = [[UIBarButtonItem alloc]initWithTitle:[CommonHelper getLocalizedText: @"save"] style:UIBarButtonItemStylePlain target:self action:@selector(clickButtonSave)];
    self.navigationItem.rightBarButtonItem = buttonSave;
    self.navigationItem.rightBarButtonItem.tintColor = COLOR_WHITE;
    self.title = [CommonHelper getLocalizedText: @"edit_your_gender"];
    __weak typeof (self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"btnback" htlImage:@"btnback" title:@"" action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    //selection table
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 24, self.view.frame.size.width, self.view.frame.size.height - 24)];
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    [self.view addSubview: self.tableView ];
    
    self.data = [NSArray arrayWithObjects:[CommonHelper getLocalizedText: @"male"],[CommonHelper getLocalizedText: @"female"], [CommonHelper getLocalizedText: @"other"],nil];
    
    self.selectIndex = 0;
    if ([[User getInstance].gender isEqual:[CommonHelper getLocalizedText: @"female"]])
    {
        self.selectIndex = 1;
    }
    else if ([[User getInstance].gender isEqual:[CommonHelper getLocalizedText: @"other"]])
        self.selectIndex = 2;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}


-(UITableViewCell *)tableView:(UITableView *)ptableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString* cellIdentifier = @"GenderCellIdentifier";
    // may be no value, so use optional
    UITableViewCell* cell = [ptableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) { // no value
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (selectIndex == indexPath.row)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    switch(indexPath.row)
    {
        case 0:
            cell.textLabel.text = [CommonHelper getLocalizedText: @"male_capitalize"];
            break;
        case 1:
            cell.textLabel.text = [CommonHelper getLocalizedText: @"female_capitalize"];
            break;
        case 2:
            cell.textLabel.text = [CommonHelper getLocalizedText: @"other_capitalize"];
            break;
        default:
            break;
    }
    
    if (indexPath.row < 2)
    {
        UIView* line = [[UIView alloc] initWithFrame: CGRectMake(0,39,cell.contentView.frame.size.width*3,1)];
        line.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
        [cell.contentView addSubview:line];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectIndex = (int)[indexPath row];
    [self.tableView reloadData];
}

-(void)clickButtonSave
{
    JGProgressHUD *progress = [[JGProgressHUD alloc] initWithStyle:JGProgressHUDStyleLight];
    progress.textLabel.text = [CommonHelper getLocalizedText: @"saving"];
    [progress showInView:self.view];
    
    NSString *gender = [CommonHelper getLocalizedText: @"male"];
    if (self.selectIndex == 0)
        gender = [CommonHelper getLocalizedText: @"male"];
    else if (self.selectIndex == 1)
        gender = [CommonHelper getLocalizedText: @"female"];
    else
        gender = [CommonHelper getLocalizedText: @"other"];
    
    NSDictionary *postData = @{@"sex":gender};
    [ServerHelper updateUserInfoWithParameters:postData Completion:^(NSDictionary *user) {
        [progress dismissAnimated:YES];
        [[User getInstance] setGender:gender];
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserInfoUpdate object:nil];
        [self.navigationController popViewControllerAnimated:YES];
        
    } failure:^{
        [progress setIndicatorView:[JGProgressHUDErrorIndicatorView new]];
        progress.textLabel.text = [CommonHelper getLocalizedText: @"failed_to_save_data"];
        [progress dismissAfterDelay:1.0];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
