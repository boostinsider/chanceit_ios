//
//  BioEditViewController.m
//  OneYuan
//
//  Created by NW on 16/6/13.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "BioEditViewController.h"

@interface BioEditViewController ()

@end

@implementation BioEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];;
    UIBarButtonItem*  buttonSave = [[UIBarButtonItem alloc]initWithTitle:[CommonHelper getLocalizedText: @"save"] style:UIBarButtonItemStylePlain target:self action:@selector(clickButtonSave)];
    __weak typeof (self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"btnback" htlImage:@"btnback" title:@"" action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    self.navigationItem.rightBarButtonItem = buttonSave;
    self.navigationItem.rightBarButtonItem.tintColor = COLOR_WHITE;
    
    self.title = [CommonHelper getLocalizedText: @"edit_your_bio"];
    
    
    self.textFieldName = [[UITextView alloc]initWithFrame:CGRectMake(0, 24, self.view.frame.size.width, 200)];
    self.textFieldName.backgroundColor = [UIColor whiteColor];;
    self.textFieldName.layer.cornerRadius = 0;
    self.textFieldName.delegate = self;
    self.textFieldName.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:self.textFieldName];
//    self.textFieldName.placeholder = [CommonHelper getLocalizedText: @"introduction"];
    self.textFieldName.text = [User getInstance].bio;
    
//    UIView* leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 16, 40)];
//    self.textFieldName.leftViewMode = UITextFieldViewModeAlways;
//    self.textFieldName.leftView = leftView;
}

-(void)clickButtonSave
{
    JGProgressHUD *progress = [[JGProgressHUD alloc] initWithStyle:JGProgressHUDStyleLight];
    progress.textLabel.text = [CommonHelper getLocalizedText: @"saving"];
    [progress showInView:self.view];
    NSString *bio = self.textFieldName.text;
    NSDictionary *postData = @{@"bio":self.textFieldName.text};
    [ServerHelper updateUserInfoWithParameters:postData Completion:^(NSDictionary *user) {
        [progress dismissAnimated:YES];
        [[User getInstance] setBio:bio];
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserInfoUpdate object:nil];
        [self.navigationController popViewControllerAnimated:YES];
        
    } failure:^{
        [progress setIndicatorView:[JGProgressHUDErrorIndicatorView new]];
        progress.textLabel.text = [CommonHelper getLocalizedText: @"failed_to_save_data"];
        [progress dismissAfterDelay:1.0];
    }];
}

#pragma textview
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (text.length > 0 && textView.text.length + text.length > 144) {
        return NO;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
