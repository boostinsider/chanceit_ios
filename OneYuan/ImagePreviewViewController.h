//
//  imagePreviewViewController.h
//  OneYuan
//
//  Created by 顺然 贾 on 16/6/4.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePreviewViewController : UIViewController

@property (nonatomic, strong) UIImageView *imageView;
@property (strong,nonatomic) UIImage* image;

@property (nonatomic, strong) UIViewController *parent;

@end
