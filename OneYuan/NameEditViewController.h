//
//  NameEditViewController.h
//  OneYuan
//
//  Created by NW on 16/6/13.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NameEditViewController : OneBaseVC
@property (strong, nonatomic) UITextField* textFieldName;
@end
