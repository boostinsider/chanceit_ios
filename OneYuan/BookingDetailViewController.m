//
//  BookingDetailViewController.m
//  OneYuan
//
//  Created by 顺然 贾 on 16/5/30.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "BookingDetailViewController.h"
#import "BraintreeCard.h"
#import "User.h"

@implementation BookingDetailViewController
@synthesize collectionView,coinCardWith,customPriceEdit,bottomView,priceView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = [CommonHelper getLocalizedText:@"recharge"];
    __weak typeof (self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"btnback" htlImage:@"btnback" title:@"" action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    self.view.backgroundColor = COLOR_WHITE;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self addPriceCollection];
    
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, DESIGN_SCREEN_HEIGHT - 124, DESIGN_SCREEN_WIDTH, 60)];
    [bottomView setBackgroundColor:COLOR_LIGHT_GRAY];
    [self.view addSubview:bottomView];
    
    
    self.buttonConfirm = [[UIButton alloc] initWithFrame:CGRectMake(8, 10, DESIGN_SCREEN_WIDTH - 8 * 2, 40)];
    self.buttonConfirm.layer.cornerRadius = 4;
    [self.buttonConfirm setTitle:@"Confirm" forState:UIControlStateNormal];
    [self.buttonConfirm setBackgroundColor:NAVBAR_COLOR];
    [bottomView addSubview:self.buttonConfirm];
    [self.buttonConfirm addTarget:self action:@selector(clickButtonConfirm) forControlEvents:UIControlEventTouchUpInside];
    
//    NSURL *clientTokenURL = [NSURL URLWithString:@"https://braintree-sample-merchant.herokuapp.com/client_token"];
//    NSMutableURLRequest *clientTokenRequest = [NSMutableURLRequest requestWithURL:clientTokenURL];
//    [clientTokenRequest setValue:@"text/plain" forHTTPHeaderField:@"Accept"];
    
    JGProgressHUD *progress = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
    progress.textLabel.text = @"get payment info";
    [progress showInView:self.view];
    
    [ServerHelper getClientTokenWithCompletion:^(id token) {
        
        progress.indicatorView = [[JGProgressHUDSuccessIndicatorView alloc] init];
        [progress dismissAfterDelay:1.0];
        
        self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:token];
        
    } failure:^{
        
        [progress setIndicatorView:[JGProgressHUDErrorIndicatorView new]];
        progress.textLabel.text = @"failure";
        [progress dismissAfterDelay:1.0];
        
        [self.navigationController popViewControllerAnimated:true];
        
    }];
    
//    self.textFieldPrice = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, DESIGN_SCREEN_WIDTH, 44)];
//    self.textFieldPrice.placeholder = @"Enter price $";
//    self.textFieldPrice.keyboardType = UIKeyboardTypeNumberPad;
//    self.textFieldPrice.backgroundColor = [UIColor whiteColor];;
//    [self.view addSubview:self.textFieldPrice];
//    UIView* leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 16, 40)];
//    self.textFieldPrice.leftViewMode = UITextFieldViewModeAlways;
//    self.textFieldPrice.leftView = leftView;
    
    
//    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DESIGN_SCREEN_WIDTH,  176)];
//    [self.view addSubview:self.tableView];
//    self.tableView.delegate = self;
//    self.tableView.dataSource = self;
//    self.tableView.bounces = NO;
    
    
    _selectIndex = 0;
    
    priceView = [[UIView alloc] initWithFrame:CGRectMake(0, DESIGN_SCREEN_HEIGHT - 168, DESIGN_SCREEN_WIDTH, 44)];
    [self.view addSubview:priceView];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DESIGN_SCREEN_WIDTH, 1)];
    [line setBackgroundColor:COLOR_LIGHT_GRAY];
    [priceView addSubview:line];
    
    UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0,  100, 44)];
    labelTitle.textColor = COLOR_DARK_GRAY;
    [labelTitle setText:@"total"];
    [priceView addSubview:labelTitle];
    
    self.labelPrice = [[UILabel alloc] initWithFrame:CGRectMake( 110, 0, DESIGN_SCREEN_WIDTH - 120, 44)];
    _labelPrice.textColor = COLOR_DARK_GRAY;
    [priceView addSubview:_labelPrice];
    _labelPrice.text = @"50";
    
    
    UIView *gestureView = [[UIView alloc] initWithFrame:CGRectMake(0, 176, DESIGN_SCREEN_WIDTH, DESIGN_SCREEN_HEIGHT - 168 - 176)];
//    gestureView.backgroundColor = COLOR_WHITE;
    [self.view addSubview:gestureView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    [tap addTarget:self action:@selector(closeKeyboard:)];
    self.view.userInteractionEnabled = YES;
    [gestureView addGestureRecognizer:tap];
    
    _price = @"0";
    
    
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)] ;
    tapGesture.delegate=self;
    [tapGesture setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tapGesture];
}

-(void)hideKeyboard
{
    if(customPriceEdit)
    {
        [customPriceEdit resignFirstResponder];
    }
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[UIView class]]) {
        [self hideKeyboard];
        return NO;
    }
    return YES;
}

-(void)addPriceCollection
{
    UILabel* pLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(14, 0, 160, 48)];
    [self.view addSubview:pLabel1];
    pLabel1.text = [CommonHelper getLocalizedText:@"your_current_coins"];
    
    UILabel* pLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(14 + 160 + 16, 0, 200, 48)];
    [self.view addSubview:pLabel2];
    pLabel2.text = [NSString stringWithFormat:@"%d",[User getInstance].coins];
    pLabel2.textColor = NAVBAR_COLOR;
    CGFloat photosTitleHeight = 48;
    CGFloat padding = 16;
    
    CGFloat imageDivider = 16;
    
    coinCardWith = (self.view.frame.size.width - 2 * imageDivider - 2 * padding) / 3;
    
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setItemSize:CGSizeMake(coinCardWith, 48)];
    flowLayout.minimumLineSpacing = 16;
    flowLayout.minimumInteritemSpacing = 16;
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(padding, photosTitleHeight, self.view.frame.size.width - padding * 2, coinCardWith * 2 + imageDivider * 3) collectionViewLayout:flowLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"UICollectionViewCell"];
    [self.view addSubview:self.collectionView];
    
    
}

- (void) closeKeyboard: (UITapGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    }
}

//-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
//{
//    return NO;
//}


- (void) clickButtonConfirm
{
    BTDropInViewController *dropInViewController = [[BTDropInViewController alloc]
                                                    initWithAPIClient:self.braintreeClient];
    dropInViewController.delegate = self;
    
    // This is where you might want to customize your view controller (see below)
    
    // The way you present your BTDropInViewController instance is up to you.
    // In this example, we wrap it in a new, modally-presented navigation controller:
    UIBarButtonItem *item = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                             target:self
                             action:@selector(userDidCancelPayment)];
    dropInViewController.navigationItem.leftBarButtonItem = item;
    UINavigationController *navigationController = [[UINavigationController alloc]
                                                    initWithRootViewController:dropInViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void) postWithNonce: (NSString *)nonce
{
    NSString *price = self.labelPrice.text;
    
    if ([price  isEqual: @"0"])
    {
        [CommonHelper showMessage:@"amount should greater than 0" Completion:^{
            
        }];
        return;
    }
    NSDictionary *params = @{@"nonce":nonce,@"amount":price};
    
    
    JGProgressHUD *progress = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
    progress.textLabel.text = @"purchase";
    [progress showInView:self.view];
    [ServerHelper payWithParams:params Completion:^(id result) {
        
        int black = [[result objectForKey:@"black"] intValue];
        [[User getInstance] setCoins:black];
        
        [progress dismiss];
        [CommonHelper showMessage:@"Succeed!" Completion:^{
            [self dismissViewControllerAnimated:YES completion:^{
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
        }];
        
    } failure:^(int status) {
        
        [progress setIndicatorView:[JGProgressHUDErrorIndicatorView new]];
        progress.textLabel.text = @"failure";
        [progress dismissAfterDelay:1.0];
        
        [self dismissViewControllerAnimated:YES completion:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }];
}

- (void)userDidCancelPayment {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - bi delegate
- (void)dropInViewController:(BTDropInViewController *)viewController
  didSucceedWithTokenization:(BTPaymentMethodNonce *)paymentMethodNonce {
    // Send payment method nonce to your server for processing
    
    [self postWithNonce:paymentMethodNonce.nonce];
    //    NSLog(@"%@",paymentMethodNonce.nonce);
    
}



- (void)dropInViewControllerDidCancel:(__unused BTDropInViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma  mark - textfield delegate
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    self.selectIndex = 5;
//    [self.collectionView reloadData];
//    return YES;
//}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (![textField.text isEqualToString:@""])
    {
        self.labelPrice.text = textField.text;
        self.price = self.labelPrice.text;
        self.selectIndex = 0;
        [self.collectionView reloadData];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length == 5 && string.length > 0) {
        return NO;
    }
    NSString* tmpStr = [NSString stringWithFormat:@"%@%@",textField.text,string];
    int length = (int)tmpStr.length;
    
    int result = 0;
    for (int i = 0; i<length; i++) {
        int x = [tmpStr characterAtIndex:i] - 48;
        int pos = length - i - 1;
        result += x * pow(10.0, pos);
    }
    
    if (result <= 0 ) {
        result = 1;
    }
    
    if (string.length == 0) {
        result /= 10;
    }
    
    _labelPrice.text = [NSString stringWithFormat:@"%d",result];
    
    
    if (string.length == 0) {
        return YES;
    }
    
    return textField.text.length < 5;
}

#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}
//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(9_0){
    return YES;
}
//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)pcollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"UICollectionViewCell";
    UICollectionViewCell * cell = [pcollectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    for (UIView* view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    cell.contentView.layer.borderWidth = 0;
    BOOL isSelected = _selectIndex == indexPath.row;
    if (indexPath.row == 5) {
        cell.contentView.backgroundColor = [UIColor whiteColor];
        
        customPriceEdit = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height)];
        customPriceEdit.placeholder = @"custom";
        customPriceEdit.keyboardType = UIKeyboardTypeNumberPad;
        [cell.contentView addSubview:customPriceEdit];
        customPriceEdit.delegate = self;
        customPriceEdit.textAlignment = NSTextAlignmentCenter;
        if (![self.price isEqualToString:@"0"])
            customPriceEdit.text = self.price;
        
        if (isSelected) {
            [customPriceEdit becomeFirstResponder];
        }
        else
        {
            customPriceEdit.userInteractionEnabled = NO;
        }
    }
    else
    {
        UILabel* pLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height)];
        [cell.contentView addSubview:pLabel];
        pLabel.textAlignment = NSTextAlignmentCenter;
        NSArray* coins = @[@"50 keys",@"100 keys",@"200 keys",@"500 keys",@"1000 keys"];
        pLabel.text = [coins objectAtIndex:indexPath.row];
        pLabel.textColor = isSelected ? NAVBAR_COLOR : COLOR_DARK_GRAY;
    }
    cell.contentView.layer.borderColor = isSelected ? NAVBAR_COLOR.CGColor:COLOR_GRAY.CGColor;
    
    cell.contentView.layer.borderWidth = 1;
    cell.contentView.layer.cornerRadius = 4;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_selectIndex == indexPath.row && _selectIndex == 5) {
        [customPriceEdit becomeFirstResponder];
    }
}
#pragma mark --UICollectionViewDelegateFlowLayout
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(coinCardWith, 48);
}
//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
#pragma mark --UICollectionViewDelegate
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)pcollectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    UICollectionViewCell * cell = (UICollectionViewCell *)[pcollectionView cellForItemAtIndexPath:indexPath];
    //    cell.backgroundColor = [UIColor whiteColor];
    
    if (indexPath.row == 5) {
//        [self callImageSelectPage];
    }
    else
    {
        NSArray* coins = @[@"50",@"100",@"200",@"500",@"1000"];
        self.labelPrice.text = [coins objectAtIndex:indexPath.row];
    }
    
    _selectIndex = (int)indexPath.row;
    
    [pcollectionView reloadData];
    
    [self.collectionView performBatchUpdates:^{
        [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
//        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        [customPriceEdit becomeFirstResponder];
    }];
    
}
//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (indexPath.row == self.imageArray.count) {
    //        return NO;
    //    }
    return YES;
}

#pragma keyboard

-(void)keyboardWillHide :(NSNotification*) notification
{
    CGFloat offset = self.bottomView.frame.origin.y;
    if (offset != DESIGN_SCREEN_HEIGHT - 124)
    {
        [UIView beginAnimations: @"ResizeForKeyboard" context: nil];
        [UIView setAnimationDuration: 0.3];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        self.bottomView.frame = CGRectMake(0, DESIGN_SCREEN_HEIGHT - 124, DESIGN_SCREEN_WIDTH, 60);
        self.priceView.frame = CGRectMake(0, DESIGN_SCREEN_HEIGHT - 168, DESIGN_SCREEN_WIDTH, 44);
        [UIView commitAnimations];
    }
}

-(void)keyboardWillShow :(NSNotification*) notification
{
    NSDictionary* info  = notification.userInfo;
    NSValue* value = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect rect = [value CGRectValue];
    CGFloat keyBoardHeight = rect.size.height;
    
    CGRect frame = bottomView.frame;
    CGFloat offset = frame.origin.y + frame.size.height - (self.view.frame.size.height - keyBoardHeight);
    
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.3];
    if (offset > 0) {
        self.bottomView.frame = CGRectMake(frame.origin.x,frame.origin.y - offset, frame.size.width, frame.size.height);
        self.priceView.frame = CGRectMake(self.priceView.frame.origin.x, self.priceView.frame.origin.y - offset, self.priceView.frame.size.width, self.priceView.frame.size.height);

    }
    [UIView commitAnimations];
}


@end
