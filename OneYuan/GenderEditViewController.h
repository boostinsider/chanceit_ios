//
//  GenderEditViewController.h
//  OneYuan
//
//  Created by NW on 16/6/13.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GenderEditViewController : OneBaseVC  <UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) UITableView* tableView;
@property (strong, nonatomic) NSArray* data;
@property (assign, nonatomic) int selectIndex;
@end
