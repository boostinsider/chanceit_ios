//
//  User.h
//  OneYuan
//
//  Created by NW on 16/5/25.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, assign) int uid;
@property (nonatomic, strong) NSString *name;
@property (assign,nonatomic) int coins;
@property (assign,nonatomic) BOOL activation;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *province;
@property (nonatomic, assign) int *createTime;
@property (nonatomic, strong) NSString *photoUrl;
@property (nonatomic, assign) int redPacket;
@property (nonatomic, assign) int idNum;
@property (nonatomic, assign) long login_ip;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *qqopenid;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, assign) int spread;
@property (nonatomic, assign) int status;
@property (nonatomic, assign) int turntable;
@property (nonatomic, assign) int ucid;
@property (nonatomic, strong) NSString *unionid;
@property (nonatomic, strong) NSString *bio;
@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, assign) int wid;
@property (nonatomic, assign) int wxlogin_time;
@property (nonatomic, assign) BOOL isLogin;

+ (instancetype) getInstance;
- (void) setUserInfo: (NSDictionary *)info;
- (void) logout;

@end
