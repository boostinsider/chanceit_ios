//
//  BirthdayEditViewController.m
//  OneYuan
//
//  Created by NW on 16/6/13.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "BirthdayEditViewController.h"

@interface BirthdayEditViewController ()

@end

@implementation BirthdayEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor hexFloatColor:@"f8f8f8"];
    UIBarButtonItem*  buttonSave = [[UIBarButtonItem alloc]initWithTitle:[CommonHelper getLocalizedText: @"save"] style:UIBarButtonItemStylePlain target:self action:@selector(clickButtonSave)];
    
    self.navigationItem.rightBarButtonItem = buttonSave;
    self.navigationItem.rightBarButtonItem.tintColor = COLOR_WHITE;
    self.title = [CommonHelper getLocalizedText: @"edit_your_birthday"];
    __weak typeof (self) wSelf = self;
    [self actionCustomLeftBtnWithNrlImage:@"btnback" htlImage:@"btnback" title:@"" action:^{
        [wSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 24, self.view.frame.size.width, 120)];
    [self.datePicker setDatePickerMode:UIDatePickerModeDate];
    //    [self.datePicker setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"zh_Hans_CN"]];
    [self.datePicker setTimeZone:[NSTimeZone defaultTimeZone]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [User getInstance].birthday.length > 0 ? [dateFormatter dateFromString:[User getInstance].birthday] : [[NSDate alloc]init];
    
    [self.datePicker setDate:date];
    
    [self.view addSubview:self.datePicker];

}

-(void)clickButtonSave
{
    JGProgressHUD *progress = [[JGProgressHUD alloc] initWithStyle:JGProgressHUDStyleLight];
    progress.textLabel.text = [CommonHelper getLocalizedText: @"saving"];
    [progress showInView:self.view];
    NSDate *birthday = self.datePicker.date;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSString *strDate = [formatter stringFromDate:birthday];
    
    
    NSDictionary *postData = @{@"birthday":strDate};
    [ServerHelper updateUserInfoWithParameters:postData Completion:^(NSDictionary *user) {
        [progress dismissAnimated:YES];
        [[User getInstance] setBirthday:strDate];
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserInfoUpdate object:nil];
        [self.navigationController popViewControllerAnimated:YES];
        
    } failure:^{
        [progress setIndicatorView:[JGProgressHUDErrorIndicatorView new]];
        progress.textLabel.text = [CommonHelper getLocalizedText: @"failed_to_save_data"];
        [progress dismissAfterDelay:1.0];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
