//
//  ServerHelper.m
//  OneYuan
//
//  Created by NW on 16/5/26.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import "ServerHelper.h"
#import "User.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


@implementation ServerHelper : NSObject

+ (NSString*)getHostAddress
{
//    return @"http://localhost";
    return SERVER_ADDRESS;
}

+(void)userLogout
{
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    NSArray *cookies = [NSArray arrayWithArray:[cookieJar cookies]];
    
    for (NSHTTPCookie *cookie in cookies) {
        [cookieJar deleteCookie:cookie];
    }
}

+ (AFHTTPSessionManager *)sharedSession
{
    static AFHTTPSessionManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        configuration.discretionary = YES;
        sharedInstance = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString: [ServerHelper getHostAddress]] sessionConfiguration:configuration];
//        sharedInstance.requestSerializer = [AFJSONRequestSerializer serializer];
        sharedInstance.responseSerializer = [AFJSONResponseSerializer serializer];
//         AFHTTPRequestSerializer *httpParserSerializer = [[AFHTTPRequestSerializer alloc] init];
//        sharedInstance.requestSerializer = httpParserSerializer;
        
//        sharedInstance.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
//        
//        [sharedInstance.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//        [sharedInstance.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
        sharedInstance.requestSerializer.HTTPShouldHandleCookies = YES;
        
        
//        NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding (kCFStringEncodingGB_18030_2000);
        NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding (kCFStringEncodingUTF8);
        sharedInstance.requestSerializer.stringEncoding = enc;
    });
    
    
    return sharedInstance;
}

+(void)loginWithParameters:(NSDictionary *)parameters Completion:(void (^)(NSDictionary *))success failure:(void (^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/public/app_login"];
    [manager POST:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject objectForKey:@"user"] && [[responseObject objectForKey:@"user"] isKindOfClass:NSNull.class] == NO) {
            NSDictionary* user = [responseObject objectForKey:@"user"];
            [CrashlyticsKit setUserIdentifier: [user objectForKey:@"id"]];
            [CrashlyticsKit setUserEmail:[user objectForKey:@"username"]];
            [CrashlyticsKit setUserName:[user objectForKey:@"username"]];
        }
        
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
        
    }];
}




+(void)signUpWithParameters:(NSDictionary *)parameters Completion:(void (^)(NSDictionary *))success failure:(void (^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    
//    /public/app_reg?username=xxx&password=xxx&repassword=xxx
    
    NSString *url = [NSString stringWithFormat:@"/index.php/public/app_reg?username=%@&password=%@&repassword=%@",[parameters objectForKey:@"username"],[parameters objectForKey:@"password"],[parameters objectForKey:@"repassword"]];
    [manager POST:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+ (void) updateUserInfoWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *user))success failure:(void(^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    
    NSString *url = [NSString stringWithFormat:@"/index.php/user/app_edit_profile"];
    [manager POST:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success(responseObject);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure(400);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+ (void) getUserInfoWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *user))success failure:(void(^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    
    NSString *url = [NSString stringWithFormat:@"/index.php/user/app_get"];
    [manager GET:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success(responseObject);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure(400);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+(void)getProductsWithParameters:(NSDictionary *)parameters Completion:(void (^)(NSDictionary *))success failure:(void (^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/list/app_get"];
    [manager GET:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+(void)getAnnouncedProductsWithParameters:(NSDictionary *)parameters Completion:(void (^)(NSDictionary *))success failure:(void (^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/user/app_announced"];
    [manager GET:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+ (void) getProductDetails:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *result))success failure:(void(^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/shop/app_more"];
    [manager GET:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+ (void) getAnnouncedDetails:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *result))success failure:(void(^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/shop/app_calculate"];
    [manager GET:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+(void)getUserRecordWithParameters:(NSDictionary *)parameters Completion:(void (^)(NSDictionary *))success failure:(void (^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/user/app_records"];
    [manager GET:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success(responseObject);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure(400);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+ (void) getShopOverWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *result))success failure:(void(^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/shop/app_over"];
    [manager GET:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+ (void) getShopRecordWithParameters:(NSDictionary *)parameters Completion:(void (^)(id ))success failure:(void (^)())failure
{
    NSString *url = @"/index.php/shop/app_record";
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    [manager POST:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success([responseObject objectForKey:@"data"]);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure(400);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(400);
    }];
}

+ (void)getLatestShopPeriodInfo:(NSDictionary *)parameters Completion:(void(^)(id result))succees failure:(void(^)())failure
{
    NSString *url = @"/index.php/shop/app_latest_period";
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    [manager POST:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            succees([responseObject objectForKey:@"data"]);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure(400);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(400);
    }];
}

+(void)addAddressWithParameters:(NSDictionary *)parameters Completion:(void (^)(NSDictionary *))success failure:(void (^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/user/app_address_add"];
    [manager POST:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success(responseObject);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure(400);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+(void)updateAddressWithParameters:(NSDictionary *)parameters Completion:(void (^)(NSDictionary *))success failure:(void (^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/user/app_address_edit"];
    [manager POST:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success(responseObject);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure(400);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+(void)getAddressWithCompletion:(void (^)(NSDictionary *))success failure:(void (^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/user/app_address"];
    [manager GET:url parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success(responseObject);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure(400);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+ (void) setAddressAsDefault:(NSDictionary *)parameters Completion: (void(^)(NSDictionary *result))success failure:(void(^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/user/app_address_default"];
    [manager POST:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success(responseObject);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure(400);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

#pragma test

+ (void) test
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/public/test"];
    
    [manager GET:url parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
       
         NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        
        NSArray *cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:[response allHeaderFields] forURL:@"localhost"];
        
        
        NSArray *cookie = [[response allHeaderFields] objectForKey:@"Set_cookie"];
        
        NSLog(@"return ---------  %@",responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"faile!!!");
    }];
}

+ (void) getClientTokenWithCompletion:(void (^)(id))success failure:(void (^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/pay/get_client_token"];
    [manager GET:url parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
//        if ([responseObject objectForKey:@"token"] && [responseObject objectForKey:@"token"] != [NSNull null])
//            success([responseObject objectForKey:@"token"]);
//        else
//            failure();
        
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200 && [responseObject objectForKey:@"token"] && [responseObject objectForKey:@"token"] != [NSNull null])
        {
            success([responseObject objectForKey:@"token"]);
        }
        else if (status == 401)
        {
            // post notification not login
            failure();
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
            
        }
        else
        {
            failure();
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+ (void) payWithParams:(NSDictionary *)params Completion:(void (^)(id))success failure:(void (^)(int))failure
{
    NSString *url = @"/index.php/pay/pay";
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    [manager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
             success([responseObject objectForKey:@"data"]);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure(400);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(400);
    }];
}

+ (void) getRechargeListWithParams:(NSDictionary *)params Completion:(void (^)(id))success failure:(void (^)(int))failure
{
    NSString *url = @"/index.php/user/app_recharge_list";
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    [manager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success([responseObject objectForKey:@"data"]);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure(400);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(400);
    }];
}

#pragma mark - add photo


+(void)getUserLotteryWithParameters:(NSDictionary *)parameters Completion:(void (^)(id ))success failure:(void (^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/user/app_lottery"];
    [manager GET:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success([responseObject objectForKey:@"data"]);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure();
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+ (void)addPhoto:(UIImage *)image WithParams:(NSDictionary *)params Completion:(void (^)(NSDictionary *))success failure:(void (^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/picture/app_upload_picture"];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"file" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"success: %@",responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"error ^%@",error);
        failure();
    }];
}

+ (void) uploadUserImage: (UIImage *)image WithParams:(NSDictionary*) params Completion:(void(^)(NSDictionary *info))success failure:(void(^)())failure
{
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    NSString *url = [NSString stringWithFormat:@"/index.php/picture/app_upload_head_picture"];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"file" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"success: %@",responseObject);
//        success(responseObject);
        
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success(responseObject);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure();
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"error ^%@",error);
        failure();
    }];
}

+ (void) shareWithParams:(NSDictionary *)params Completion:(void (^)(id))success failure:(void (^)())failure
{
    NSString *url = @"/index.php/user/app_shared";
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    [manager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success([responseObject objectForKey:@"data"]);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure(400);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(400);
    }];
}
+ (void)getShareListWithParams:(NSDictionary *)params Completion:(void (^)(id))success failure:(void (^)())failure
{
    NSString *url = @"/index.php/user/app_share_list";
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    [manager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success([responseObject objectForKey:@"data"]);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure();
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+ (void)getShareDetailWithParams:(NSDictionary *)params Completion:(void (^)(id))success failure:(void (^)())failure
{
    NSString *url = @"/index.php/user/app_share_more";
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    [manager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success([responseObject objectForKey:@"data"]);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure();
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}


#pragma buy
+(void)buyProductWithParameters:(NSDictionary *)parameters Completion:(void (^)(NSDictionary *))success failure:(void (^)())failure
{
    NSString *url = @"/index.php/pay/app_buy";
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    [manager POST:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success([responseObject objectForKey:@"data"]);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure();
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}

+ (void) buyProductWithParameters:(NSDictionary *)parameters ProductIndex:(int) index Completion:(void(^)(NSDictionary *result, int index))success failure:(void(^)())failure
{
    NSString *url = @"/index.php/pay/app_buy";
    AFHTTPSessionManager *manager = [ServerHelper sharedSession];
    [manager POST:url parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        int status = [[responseObject objectForKey:@"status"] intValue];
        if (status == 200)
        {
            success([responseObject objectForKey:@"data"],index);
        }
        else if (status == 401)
        {
            // post notification not login
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLogin object:nil];
        }
        else
        {
            failure();
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure();
    }];
}



@end
