//
//  ServerHelper.h
//  OneYuan
//
//  Created by NW on 16/5/26.
//  Copyright © 2016年 Peter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface ServerHelper : NSObject

#pragma share instance
+ (AFHTTPSessionManager *)sharedSession;
+ (NSString*) getHostAddress;
#pragma user
+ (void) loginWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *user))success failure:(void(^)())failure;
+ (void) signUpWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *user))success failure:(void(^)())failure;
+ (void) getUserInfoWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *user))success failure:(void(^)())failure;

+ (void) userLogout;
+ (void) updateUserInfoWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *user))success failure:(void(^)())failure;
+ (void) uploadUserImage: (UIImage *)image WithParams:(NSDictionary*) params Completion:(void(^)(NSDictionary *info))success failure:(void(^)())failure;

#pragma products info
+ (void) getProductsWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *products))success failure:(void(^)())failure;
+ (void) getProductDetails:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *result))success failure:(void(^)())failure;

+ (void) getAnnouncedProductsWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *products))success failure:(void(^)())failure;
+ (void) getAnnouncedDetails:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *result))success failure:(void(^)())failure;
+ (void) getUserRecordWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *records))success failure:(void(^)())failure;

+ (void) getShopOverWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *result))success failure:(void(^)())failure;

+ (void)getShopRecordWithParameters:(NSDictionary *)parameters Completion:(void(^)(id result))success failure:(void(^)())failure;
+ (void)getLatestShopPeriodInfo:(NSDictionary *)parameters Completion:(void(^)(id result))succees failure:(void(^)())failure;


#pragma address

+ (void) addAddressWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *result))success failure:(void(^)())failure;
+ (void) updateAddressWithParameters:(NSDictionary *)parameters Completion:(void (^)(NSDictionary *))success failure:(void (^)())failure;
+ (void) getAddressWithCompletion:(void(^)(NSDictionary *result))success failure:(void(^)())failure;
+ (void) setAddressAsDefault:(NSDictionary *)parameters Completion: (void(^)(NSDictionary *result))success failure:(void(^)())failure;


#pragma mark - test
+ (void) test;

#pragma mark - payment
+ (void) getClientTokenWithCompletion:(void(^)(id token))success failure:(void(^)())failure;

+ (void) payWithParams: (NSDictionary *)params Completion:(void(^)(id result))success failure:(void(^)(int status))failure;

+ (void)getRechargeListWithParams:(NSDictionary *)params Completion:(void(^)(id result))success failure:(void(^)(int status))failure;


#pragma mark - share orders
+ (void) getUserLotteryWithParameters:(NSDictionary *)parameters Completion:(void(^)(id result))success failure:(void(^)())failure;
+ (void) addPhoto: (UIImage *)image WithParams:(NSDictionary*) params Completion:(void(^)(NSDictionary *info))success failure:(void(^)())failure;
+ (void) shareWithParams:(NSDictionary *)params Completion:(void(^)(id result))success failure:(void(^)())failure;
+ (void)getShareListWithParams:(NSDictionary *)params Completion:(void(^)(id result))success failure:(void(^)())failure;
+ (void)getShareDetailWithParams:(NSDictionary *)params Completion:(void(^)(id result))success failure:(void(^)())failure;


#pragma buy
+ (void) buyProductWithParameters:(NSDictionary *)parameters Completion:(void(^)(NSDictionary *result))success failure:(void(^)())failure;
+ (void) buyProductWithParameters:(NSDictionary *)parameters ProductIndex:(int) index Completion:(void(^)(NSDictionary *result, int index))success failure:(void(^)())failure;

@end
